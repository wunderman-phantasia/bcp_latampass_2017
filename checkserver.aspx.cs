﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class checkserver : System.Web.UI.Page
{
    string lstrConnectionString = ConfigurationManager.ConnectionStrings["BD_LANPASSConnectionString"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataSourceEnumerator sqldatasourceenumerator1 = SqlDataSourceEnumerator.Instance;
        DataTable datatable1 = sqldatasourceenumerator1.GetDataSources();
        foreach (DataRow row in datatable1.Rows)
        {
            Console.WriteLine("****************************************");
            Console.WriteLine("Server Name:" + row["ServerName"]);
            Console.WriteLine("Instance Name:" + row["InstanceName"]);
            Console.WriteLine("Is Clustered:" + row["IsClustered"]);
            Console.WriteLine("Version:" + row["Version"]);
            Console.WriteLine("****************************************");
        }
    }
}