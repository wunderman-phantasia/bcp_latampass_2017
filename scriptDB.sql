USE [BCPLANTRIPLICADB]
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_VERANO2013_1]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_VERANO2013_1](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_SUPERMERCADO2_201307]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_SUPERMERCADO2_201307](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_SUPERMERCADO2]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_SUPERMERCADO2](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_SUPERMERCADO04]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_SUPERMERCADO04](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_SUPERMERCADO]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_SUPERMERCADO](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_REST_201305]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_REST_201305](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_PROMO2014_2_AMEX]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_PROMO2014_2_AMEX](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
	[REGV_Departamento] [varchar](250) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_PROMO2014_2]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_PROMO2014_2](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
	[REGV_Departamento] [varchar](250) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_PROMO2014_1]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_PROMO2014_1](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
	[REGV_Departamento] [varchar](250) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_NAVIDAD2013_2]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_NAVIDAD2013_2](
	[REGG_IdRegistroNavidad2013] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
	[REGV_Departamento] [varchar](250) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistroNavidad2013] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_NAVIDAD2013]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_NAVIDAD2013](
	[REGG_IdRegistroNavidad2013] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
	[REGV_Departamento] [varchar](250) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistroNavidad2013] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_NAVIDAD2012_3]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_NAVIDAD2012_3](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_NAVIDAD2012]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_NAVIDAD2012](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_DIA_MADRE]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_DIA_MADRE](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_BASE2014]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_BASE2014](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
	[REGV_Departamento] [varchar](250) NOT NULL,
	[REGV_Capania] [varchar](250) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_2015_SEPTIEMBRE]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_2015_SEPTIEMBRE](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
	[REGV_Departamento] [varchar](250) NOT NULL,
 CONSTRAINT [PK__BLTT_REG__3B3C3A366A30C649] PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO_1]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO_1](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_REGISTRO]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_REGISTRO](
	[REGG_IdRegistro] [uniqueidentifier] NOT NULL,
	[REGV_Nombres] [varchar](100) NOT NULL,
	[REGV_ApellidoPaterno] [varchar](100) NOT NULL,
	[REGV_ApellidoMaterno] [varchar](100) NOT NULL,
	[REGV_CorreoElectronico] [varchar](150) NOT NULL,
	[REGV_TipoDocumento] [varchar](10) NOT NULL,
	[REGV_NumerDocumento] [varchar](20) NOT NULL,
	[REGD_FechaCreacion] [datetime] NOT NULL,
	[REGV_Telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[REGG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BLTT_CLIENTES]    Script Date: 08/20/2015 11:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BLTT_CLIENTES](
	[CLIG_IdRegistro] [uniqueidentifier] NOT NULL,
	[CLIV_Nombres] [varchar](100) NULL,
	[CLIV_ApellidoPaterno] [varchar](100) NULL,
	[CLIV_ApellidoMaterno] [varchar](100) NULL,
	[CLIV_TipoDocumento] [varchar](10) NULL,
	[CLIV_NumerDocumento] [nchar](10) NULL,
	[CLII_Meta] [int] NULL,
	[CLII_Bono] [int] NULL,
	[CLIV_GrupoCampania] [varchar](10) NULL,
 CONSTRAINT [PK_BLTT_CLIENTES] PRIMARY KEY CLUSTERED 
(
	[CLIG_IdRegistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__BLTT_REGI__REGV___656C112C]    Script Date: 08/20/2015 11:49:24 ******/
ALTER TABLE [dbo].[BLTT_REGISTRO] ADD  DEFAULT ('') FOR [REGV_Telefono]
GO
