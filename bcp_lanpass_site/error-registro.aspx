﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="error-registro.aspx.cs" Inherits="error_registro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

	<script type="text/javascript">
	   // Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
	    Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.Content header h1', { fontFamily: 'Flexo-Bold' });
	</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
	<section class="Content"  style="padding-left: 40px; position:relative; height: 380px;" tabindex="7" title="Tu solicitud ha sido enviada. Pronto nos comunicaremos contigo.">
		<header style="height: 34px;">
            <h1>Solicitud enviada</h1>
            <div class="div_regresa"><a class="promobtn" href="Default.aspx" tabindex="8">Regresar</a></div>
        </header>
        <section class="visaconfirmation">
            <%-- <h1 style="font-size: 31px; font-weight: bold; margin-bottom: 24px; color: #474747;">¡Gracias!</h1>--%>
            <!--<h3>
                Pronto nos comunicaremos contigo.
            </h3>            -->
			<%--<p style="font-family:Flexo-Regular;font-size:14px;color:#474747;margin-bottom:20px;">Ya realizaste una solicitud de tarjeta.</p>
            <p style="font-family:Flexo-Regular;font-size:14px;color:#474747;">Pronto nos comunicaremos contigo.</p>--%>
             <p style="font-size: 16px;">
                Ya realizaste una solicitud de tarjeta.
                <br />
                    Pronto nos comunicaremos contigo
                <br />
                <br />
                    En tus planes contigo, BCP.
            </p>
			<div class="imgAbsolute img_confirmacion" style="margin-right: 100px;"></div>
		</section>

		
		
		<div class="clear"></div>
	</section>

    <script type="text/javascript">
        _gaq.push(['_trackPageview', '/WebsiteOficial/formulario/error']);
    </script>

</asp:Content>