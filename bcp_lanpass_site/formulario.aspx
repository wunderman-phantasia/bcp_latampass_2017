﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="formulario.aspx.cs" Inherits="formulario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="prueba/html5tooltips.css" rel="stylesheet" />
    <link href="prueba/html5tooltips.animation.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript">
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
        Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.Content header h1', { fontFamily: 'Flexo-Bold' });
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }; if (!f._fbq) f._fbq = n;
            n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
        }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1493357820896130');
        fbq('track', "PageView");</script>
    <noscript>
        <img height="1" width="1" style="display: none"
            src="https://www.facebook.com/tr?id=1493357820896130&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <section class="Content">
        <header style="height: 34px;">
            <h1 tabindex="7" title="Pide tu Tarjeta BCP LATAM Pass">Pide tu Tarjeta BCP LATAM Pass</h1>
            <div class="div_regresa"><a class="promobtn" href="Default.aspx" tabindex="8">Regresar</a></div>
        </header>
        <div class="tarjetasform" style="height: auto">
            <p tabindex="8" title="Elige la Tarjeta de Crédito que prefieras, ingresa tus datos y nos pondremos en contacto contigo."><span class="lightitalic16light">Elige la </span><span class="mediumitalic16light">Tarjeta de Crédito </span><span class="lightitalic16light">que prefieras, ingresa tus datos y nos pondremos en contacto contigo.</span></p>

            <!-- cambiado action precio: confirmacion.aspx -->
            <form id="frmSolicitud" name="input" action="formulario.aspx" method="post" style="font-family: Flexo-Italic; font-size: 14px; color: #727272;" runat="server">
                <input type="hidden" name="UtmSource" value="<%=UtmSource %>" />
                <input type="hidden" name="UtmMedium" value="<%=UtmMedium %>" />
                <input type="hidden" name="UtmCampaign" value="<%=UtmCampaign %>" />
                <div class="formcardpick">
                    <section class="bbgray">
                        <section>
                            <div style="width: 64px; height: 39px; margin-bottom: 10px;">
                                <img src="images/form/form_visa.png" width="63" height="21" alt="" style="margin-top: 10px; margin-left: 163px;">
                            </div>
                            <input type="radio" id="visa" name="pickcard" value="visa" title="Selecciona Visa">
                        </section>
                        <section>
                            <div style="width: 64px; height: 39px; margin-bottom: 10px;">
                                <img src="images/form/form_amex.png" width="64" height="39" alt="" style="margin-left: 167px;">
                            </div>
                            <input type="radio" id="amex" name="pickcard" value="amex" title="Selecciona Mastercard">
                        </section>
                    </section>
                    <div class="clear"></div>
                    <br />
                </div>
                <div class="clear"></div>
                <span tabindex="10" title="Recuerda seleccionar la tarjeta que quieras solicitar. Aún no lo has hecho.">
                    <label for="pickcard" class="error" style="margin-top: 20px; margin-left: 0px;"></label>
                </span>
                <br />
                <div class="clear"></div>
                <style type="text/css">
                    .form-content {
                        width: 50%;
                        float: left;
                    }

                    .form-element {
                        width: 100%;
                        /*margin-top: 25px;
                        margin-bottom: 25px;*/
                    }
                </style>
                <div>
                    <div class="form-content">
                        <div class="form-element">
                            <fieldset>
                                <input data-id="1" class="forminputtext" type="text" id="firstname" name="firstname" maxlength="30" tabindex="11" title="Ingresa tu nombre." placeholder="Nombre" />
                                <div class="inputerrorimage" data-id="firstname"></div>
                                <div class="inputcheckimage" data-id="firstname"></div>
                                <label for="firstname">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Para registrar y atender tu solicitud." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <div class="clear"></div>
                                <span tabindex="120" title="Recuerda ingresar tu nombre. Aún no lo has hecho.">
                                    <label for="firstname" class="error"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element">
                            <fieldset>
                                <input data-id="2" class="forminputtext" type="text" name="lastnamefather" maxlength="30" tabindex="12" title="Ingresa tu apellido paterno." placeholder="Apellido paterno" />
                                <div class="inputerrorimage" data-id="lastnamefather"></div>
                                <div class="inputcheckimage" data-id="lastnamefather"></div>
                                <label for="lastnamefather">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Para registrar y atender tu solicitud." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <div class="clear"></div>
                                <span tabindex="140" title="Recuerda ingresar tu apellido paterno. Aún no lo has hecho.">
                                    <label for="lastnamefather" class="error"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element">
                            <fieldset>
                                <input data-id="3" class="forminputtext" type="text" name="lastnamemother" maxlength="30" tabindex="13" title="Ingresa tu apellido materno." placeholder="Apellido materno" />
                                <div class="inputerrorimage" data-id="lastnamemother"></div>
                                <div class="inputcheckimage" data-id="lastnamemother"></div>
                                <label for="lastnamemother">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Para registrar y atender tu solicitud." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <div class="clear"></div>
                                <span tabindex="160" title="Recuerda ingresar tu apellido materno. Aún no lo has hecho.">
                                    <label for="lastnamemother" class="error"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element">
                            <fieldset>
                                <input data-id="4" class="forminputtext" type="text" name="phone" maxlength="9" tabindex="14" title="Ingresa tu teléfono o celular." placeholder="Celular o teléfono" />
                                <div class="inputerrorimage" data-id="phone"></div>
                                <div class="inputcheckimage" data-id="phone"></div>
                                <label for="phone">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Para contactarnos contigo y responder tu solicitud" data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <div class="clear"></div>
                                <span tabindex="180" title="Recuerda ingresar tu teléfono con mínimo 6 y máximo 9 números. Aún no lo has hecho o no has ingresado la cantidad correcta de números.">
                                    <label for="phone" class="error"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element">
                            <fieldset>
                                <input data-id="6" class="forminputtext" type="text" name="email" maxlength="64" tabindex="15" title="Ingresa tu correo electrónico. " placeholder="Correo electrónico">
                                <div class="inputerrorimage" data-id="email"></div>
                                <div class="inputcheckimage" data-id="email"></div>
                                <label for="email">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Para contactarnos contigo y responder tu solicitud." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <div class="clear"></div>
                                <span tabindex="200" title="Recuerda ingresar tu correo electrónico. Debe tener @ y una extensión válida. Aún no lo has hecho o no lo has ingresado de la forma correcta.">
                                    <label for="email" class="error"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element">
                            <fieldset>
                                <label for="doctype">
                                    <div style="float: left; padding-top: 12px;">
                                        <span class="italic14light">Selecciona tu tipo de documento </span>
                                    </div>
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Para validar tus datos y la información de tu solicitud." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                    <div class="clear"></div>
                                </label>
                                <input data-id="161" type="radio" name="doctype" value="yes" style="margin-left: 0px; margin-top: 11px;" class="left" tabindex="16" title="Selecciona DNI." checked><span class="left" style="margin-top: 11px; margin-left: 0px;">DNI</span>
                                <input data-id="162" type="radio" name="doctype" value="no" style="margin-left: 10px; margin-top: 11px;" class="left" tabindex="17" title="Selecciona Carné de Extranjería."><span class="left" style="margin-top: 11px; margin-left: 0px;">Carné de extranjería</span>
                                <div class="clear"></div>
                                <span tabindex="250" title="Recuerda indicar si tienes Cuenta Sueldo BCP. Aún no lo has hecho.">
                                    <label for="doctype" class="error" style="height: 15px; line-height: 25px;"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element">
                            <fieldset>
                                <input data-id="5" class="forminputtext" type="text" name="dni" maxlength="8" tabindex="18" title="Ingresa tu número de documento." placeholder="Nro de doc">
                                <div class="inputerrorimage" data-id="dni"></div>
                                <div class="inputcheckimage" data-id="dni"></div>
                                <label for="dni">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Para validar tus datos y la información de tu solicitud." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <div class="clear"></div>
                                <span data-id="5" tabindex="230" title="Recuerda ingresar tu número de DNI. Debe tener 8 dígitos. Aún no lo has hecho o no has ingresado la cantidad correcta de números.">
                                    <label for="dni" class="error"></label>
                                </span>
                            </fieldset>
                        </div>
                    </div>

                    <div class="form-content">
                        <div class="form-element" style="margin-top: -13px">
                            <fieldset>
                                <label for="hasaccount">
                                    <div style="float: left; padding-top: 12px;">
                                        <span class="italic14light">¿Tienes Cuenta Sueldo BCP? </span>
                                    </div>
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Es importante para nosotros saber si ya eres nuestro cliente." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                    <div class="clear"></div>
                                </label>
                                <input data-id="7" type="radio" name="hasaccount" value="yes" style="margin-left: 0px; margin-top: 11px;" class="left" tabindex="19" title="Indica si tienes Cuenta Sueldo BCP. Selecciona Sí."><span class="left" style="margin-top: 11px; margin-left: 0px;">Si</span>
                                <input data-id="7" type="radio" name="hasaccount" value="no" style="margin-left: 11px; margin-top: 10px;" class="left" title="Indica si tienes Cuenta Sueldo BCP. Selecciona No."><span class="left" style="margin-top: 11px; margin-left: 0px;">No</span>
                                <div class="clear"></div>
                                <span tabindex="250" title="Recuerda indicar si tienes Cuenta Sueldo BCP. Aún no lo has hecho.">
                                    <label for="hasaccount" class="error" style="height: 15px; line-height: 25px; margin-top: 4px"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element" style="margin-top: 16px;">
                            <fieldset>
                                <input data-id="8" class="forminputtext" type="text" name="income" maxlength="6" tabindex="20" title="Indica tu ingreso mensual soles." placeholder="Ingreso mensual soles">
                                <div class="inputerrorimage" data-id="income"></div>
                                <div class="inputcheckimage" data-id="income"></div>
                                <label for="income">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Dato necesario para validar la categoría de tarjeta a la que estás aplicando." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <div class="clear"></div>
                                <span tabindex="270" title="Recuerda ingresar tu ingreso mensual. Éste debe ser igual o mayor al ingreso mínimo mensual que corresponde a la tarjeta que quieres solicitar. Aún no lo has ingresado o no has ingresado un monto válido.">
                                    <label for="income" class="error"></label>
                                </span>
                            </fieldset>
                        </div>
                        <div class="form-element">
                            <fieldset>
                                <asp:DropDownList ID="typecard" name="typecard" runat="server" CssClass="forminputtext">
                                </asp:DropDownList>
                                <label for="typecard">
                                    <div style="float: left; padding-top: 11px; padding-left: 10px;" data-tooltip="Dato necesario para validar el diseño de la tarjeta." data-tooltip-more="" data-tooltip-stickto="right" data-tooltip-maxwidth="250" data-tooltip-animate-function="foldin"><span data-id="4" class="ttfirstname regular11redcursor">( ? )</span></div>
                                </label>
                                <%--<select data-id="8" class="forminputtext" name="typecards"  tabindex="20" title="Indica tu ingreso mensual soles." />--%>
                                <%--  <div class="inputerrorimage" data-id="typecard"></div>
                                <div class="inputcheckimage" data-id="typecard"></div>
                                <div class="clear"></div>
                                <span tabindex="270" title="Recuerda ingresar tu ingreso mensual. Éste debe ser igual o mayor al ingreso mínimo mensual que corresponde a la tarjeta que quieres solicitar. Aún no lo has ingresado o no has ingresado un monto válido.">
                                    <label for="ctl00$MainContent$typecard" class="error"></label>
                                </span>--%>
                                <div style="margin-left: 15px;">
                                    <img src="" id="imgTarjeta" class="img-responsive" width="335" style="margin-top: 15px;" />
                                </div>
                                <div class="clear"></div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="form-content" style="width: 100%; margin-top: 10px;">
                        <fieldset>
                            <input type="submit" value="¡Pídela Aquí!" class="btnsubmit" tabindex="22" onclick="_gaq.push(['_trackEvent', 'bcp_lanpass', 'enviar', 'solicitud_lanpass']);" title="Haz click aquí para enviar tus datos y espera la confirmación del envío.">
                        </fieldset>
                    </div>
                    <section class="bbgray"></section>
                    <div class="form-content" style="width: 100%; margin-top: 20px">
                        <span class="italic12light">- La aprobación está sujeta a evaluación crediticia.</span><br />
                        <span class="italic12light">- Las tarjetas con diseño solo aplican para Visa clásica y oro.</span><br />
                        <span class="italic12light">- El costo por emisión de tarjetas con diseño es de S/ 20.00 que serán cargados en la primera facturación.</span>
                    </div>
                    <asp:HiddenField ID="hdfIdDiseno" runat="server" />
                </div>
            </form>
        </div>
        <div class="clear"></div>
    </section>
    <script type="text/javascript">
        $(function () {
            var input = document.createElement("input");
            if (('placeholder' in input) == false) {
                $('[placeholder]').focus(function () {
                    var i = $(this);
                    if (i.val() == i.attr('placeholder')) {
                        i.val('').removeClass('placeholder');
                        if (i.hasClass('password')) {
                            i.removeClass('password');
                            this.type = 'password';
                        }
                    }
                }).blur(function () {
                    var i = $(this);
                    if (i.val() == '' || i.val() == i.attr('placeholder')) {
                        if (this.type == 'password') {
                            i.addClass('password');
                            this.type = 'text';
                        }
                        i.addClass('placeholder').val(i.attr('placeholder'));
                    }
                }).blur().parents('form').submit(function () {
                    $(this).find('[placeholder]').each(function () {
                        var i = $(this);
                        if (i.val() == i.attr('placeholder'))
                            i.val('');
                    })
                });
            }
        });
    </script>
    <%--<script src="js/app.js" type="text/javascript"></script>--%>
    <script src="js/JScript_formulario.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).on("ready", function () {
            String.Format = function (b) {
                var a = arguments;
                return b.replace(/(\{\{\d\}\}|\{\d\})/g, function (b) {
                    if (b.substring(0, 2) == "{{") return b;
                    var c = parseInt(b.match(/\d/)[0]);
                    return a[c + 1];
                });
            };
            window.CargarDisenoTarjeta = function (idDiseno) {
                var ruta = String.Format("https://www.eligetutarjetabcp.com/Handlers/FilesHandler.ashx?idTipoAccion={0}&idTipoMantenimiento={1}&idDisenoTarjeta={2}", 2, 2, idDiseno),
                    image = new Image(),
                    cntImage = $("#imgTarjeta");

                cntImage.fadeOut("fast");

                cntImage.prop("src", ruta + "&" + new Date().getTime());

                image.onload = function () {
                    cntImage.fadeIn("fast");
                }
                image.onerror = function () {
                    console.log("Cannot load image");
                }
                image.src = ruta;
            }

            $("#<%= typecard.ClientID %>").val($("#ContentPlaceHolder1_hdfIdDiseno").val());
            window.CargarDisenoTarjeta($("#ContentPlaceHolder1_hdfIdDiseno").val());
            $("#<%= typecard.ClientID %>").on("change", function () {
                var idDiseno = $("#<%= typecard.ClientID %>").val();
                window.CargarDisenoTarjeta(idDiseno);
            });

        });
    </script>
    <script>
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        try {
            var prodId = getParameterByName('pickcard');
            document.getElementById(prodId).checked = true;
        } catch (err) {
            //console.log("no hay parametro pickcard")
        }
    </script>

    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Tarjetas").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/formulario']);
    </script>

    <script src="prueba/functions.js"></script>
    <script src="prueba/html5tooltips.js"></script>

</asp:Content>

