﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        Cufon.replace('.aviBox h1', { fontFamily: 'Flexo-Light' }); // Requires a selector engine for IE 6-7, see above
        Cufon.replace('.aviBox h3', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.pull-left', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.pull-right', { fontFamily: 'Flexo-Light' });
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.slider .bannerslide[data-id="1"] h1', { fontFamily: 'Flexo-Bold' })
        //Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
        //Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <form id="frmMain" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <section id="roll" class="Content home">
            <div class="contentSlided">

                <div id="bannerSlider" class="slider">

                    <div data-id="10" class="bannerslide banneramex260514">
                        <div class="cont_banner">
                            <div class="msg">
                                <h2 class="title-index" title="Triplica tus KMS. LATAM Pass Comprando lo que quieras">Triplica tus KMS. LATAM Pass<br />
                                    Comprando lo que quieras</h2>
                                <p class="lineas">----------------------------------------------------</p>
                                <p class="text-title-home" title="En TODOS los establecimientos del 1 al 30 junio con tu tarjeta de Crédito Amercian Express® BCP LANPASS">
                                    En TODOS los establecimientos del 1 al 30 junio con tu tarjeta<br />
                                    De Crédito Amercian Express® BCP LATAM Pass
                                </p>
                                <div class="cont_tarjetas"></div>
                                <a href="promoamex.aspx" class="btn-inscribir" title="Triplica tus KMS. LATAM Pass Comprando lo que quieras"></a>
                            </div>
                        </div>
                    </div>

                    <div data-id="1" class="bannerslide bannercarrera10k" style="position: relative;">
                        <a href="http://www.latam.com/latampass/lanpass10kperu/" target="_blank" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0; right: 0; bottom: 0;"></a>
                    </div>                    

                    <div data-id="3" class="bannerslide">
                        <div class="cont_banner">
                            <div class="bnr_left">
                                <h2>¿Ya eres socio LATAM Pass?</h2>
                                <p>
                                    Actualiza tus datos y entérate
                                    <br>
                                    de todas nuestras promociones
                                    <br>
                                    de manera rápida y fácil.
                                </p>
                                <a href="https://ssl.lan.com/cgi-bin/site_login.cgi?site=personas;page=http%3A%2F%2Fssl.lan.com%2Fcgi-bin%2Fprofile%2Fusuario.cgi%3Faction%3Dask_edit_full%253Fotid%253D894423%3Bs_cid%3Donsite2014_PE_20140421_home_bcplatampass_actdatos%3Ftoken%3Dp_VD1z3dyRmhxc9s4PQCFg%3B;msg_word=" target="_blank">Actualiza aquí</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="pagSlide" class="pagSlide"></div>

                <script>
                    $(document).ready(function () {
                        $("#bannerSlider").carouFredSel({
                            auto: true, items: 1,
                            scroll: { fx: "fade", items: 1, pauseOnHover: true, duration: 1000 },
                            pagination: "#pagSlide"
                        });
                    })
                </script>


                <div style="margin-top: 18px;">
                    <div class="avion" tabindex="11" title="Información de kilómetros para los principales destinos: Iquitos 14mil kilómetros, Tarapoto 10mil kilómetros, Trujillo 10mil kilómetros, Cuzco 14mil kilómetros, Arequipa 14mil kilómetros, Santiago 26mil kilómetros, Bogotá 28mil kilómetros, Miami 48mil kilómetros.">
                        <div class="aviBox">
                            <img src="images/flightboard/flightboard_icon.jpg" width="54" height="54" alt="">
                            <h1>¡Viaja a estos destinos!</h1>
                            <h3>Comprando con tus Tarjetas BCP LATAM Pass estarás cada vez más cerca de ellos.</h3>
                            <div class="clear"></div>
                        </div>
                        <div class="aviBox pull-left">
                            Destinos Recomendados
                        </div>
                        <div class="aviBox pull-right">
                            Viaja desde:
                        </div>
                        <div class="clear"></div>

                        <div class="Mainflights">
                            <div class="flightboardline">
                                <div data-id="1" class="basicBoard fliptext"></div>
                                <div data-id="1" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="2" class="basicBoard fliptext"></div>
                                <div data-id="2" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="3" class="basicBoard fliptext"></div>
                                <div data-id="3" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="4" class="basicBoard fliptext"></div>
                                <div data-id="4" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="5" class="basicBoard fliptext"></div>
                                <div data-id="5" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="6" class="basicBoard fliptext"></div>
                                <div data-id="6" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="vIE">
                            <div class="flightboardline">
                                <div data-id="7" class="basicBoard fliptext"></div>
                                <div data-id="7" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="8" class="basicBoard fliptext"></div>
                                <div data-id="8" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="9" class="basicBoard fliptext"></div>
                                <div data-id="9" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="10" class="basicBoard fliptext"></div>
                                <div data-id="10" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="11" class="basicBoard fliptext"></div>
                                <div data-id="11" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="12" class="basicBoard fliptext"></div>
                                <div data-id="12" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS.LATAM Pass</span></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="Pagflights">
                            <a href="javascript:void(0);" rel="01" class="activo">1</a>
                            <a href="javascript:void(0);" rel="02">2</a>
                        </div>

                        <a href="javascript:void(0);" class="arrow-prev" style="display: none;"></a>
                        <a href="javascript:void(0);" class="arrow-next"></a>

                    </div>

                    <div class="banners">
                        <div class="banner1"><a class="btnbanner1" href="formulario.aspx" tabindex="12" title="Pide tu Tarjeta BCP LATAM Pass online.">¡Pídela aquí!</a></div>
                        <div class="banner2"><a class="btnbanner2" href="faustino.aspx" tabindex="13" title="¡Conoce a Faustino!">¡Conoce a Faustino!</a></div>
                        <div class="banner3">
                            <div class="Doc One">
                                <ul id="selectDocType">
                                    <li class="lightitalic11dark" data-id="1"><span style="float: left;">DNI</span><div class="comboselect"></div>
                                    </li>
                                    <li class="lightitalic11dark unselected" data-id="1">DNI</li>
                                    <li class="lightitalic11dark unselected" data-id="2">Carné de extranjería</li>
                                </ul>
                                <input id="inputDoc" data-id="1" type="text" maxlength="8" placeholder="Ingresa tu DNI" class="inputDoc" tabindex="14" title="Ingresa tu número de documento y presiona ENTER">
                                <p class="lightitalic11orange inputDocerror" tabindex="15" title="Tu número de documento debe tener 8 números."></p>
                                <a class="btnbanner3" href="" tabindex="16" title="Consulta">Consulta</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>

            </div>
        </section>
    </form>
    <div class="modal2"></div>
    <div data-id="1" class="modal">
        <div message-id="1" class="message">
            <div class="popupbox">
                <span class="ModalClose bolditalic30orange">&times;</span>
                <p class="bolditalic23orange popuptitle" id="data-nombre-1">&ltNombre&gt</p>
                <p class="lightitalic23orange popupttext1" id="data-titulo-1">¡Traslada tu Deuda de otros bancos al BCP y gana un Bono de Kilómetros*!</p>
                <p class="lightitalic14dark popupttext2" id="data-descrip-1">*Conoce más de esta oferta acercándote a una Agencia BCP o llamando a nuestra Banca por Teléfono al 311-9898 y disfruta de los beneficios de las Tarjetas de Crédito BCP LANPASS!</p>
                <p class="lightitalic11dark popupttext3" id="data-subdescrip-1">Válido hasta el &lt;fecha de vencimiento&gt; sujeto a evaluación crediticia.</p>
            </div>
        </div>
    </div>

    <div data-id="5" class="modal">
        <div class="message messagesmall">
            <div class="popupbox">
                <span class="ModalClose bolditalic30orange">&times;</span>
                <p class="lightitalic23orange popupttext1" style="padding-top: 55px;">Por el momento, no tenemos una oferta activa para ti.</p>
                <p class="lightitalic14dark popupttext2">¡Te mantendremos informado!</p>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="pull-left" style="width: 100px; margin-top: 25px;">
            Compartir en: 
        </div>
        <div class="pull-left" style="width: 50px; margin-top: 20px;">
            <a class="fbbuttonclick" href="">
                <img src="images/facebook.png" width="22" height="22" alt=""></a>
        </div>
        <div class="pull-left" style="margin-top: 20px;">
            <div class="fb-like" data-href="http://www.bcplatampass.com/" data-width="450" data-layout="button_count" data-action="like" data-show-faces="false" data-send="false"></div>
        </div>
    </footer>
    <script type="text/javascript" src="js/jquery.flightboard.js"></script>
    <script src="js/jquery.history.js"></script>
    <script src="js/JScript_Inicio.js" type="text/javascript"></script>
    <script type="text/javascript">
        _gaq.push(['_trackPageview', '/WebsiteOficial/home']);
        //$(".active").removeClass("active");
        $("#Inicio").css({ "display": "none" });
        $('.avion').css('cursor', 'pointer').on('click', function () {
            window.open("http://www.latam.com/latampass/es_pe/sitio_personas/lanpass/promociones/desde-ahora-conoce-el-peru-por-menos-kms/desde-lima/");
        });
    </script>
</asp:Content>

