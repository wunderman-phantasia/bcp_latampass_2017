﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="no-hay-tarjeta.aspx.cs" Inherits="registro_duplicado" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles/reset.css" type="text/css" />
    <link rel="stylesheet" href="Styles/fonts.css" type="text/css" />
    <style type="text/css">
        .message {
            border: 3px;
            width: 450px;
            height: auto;
            padding-top: 50px;
            padding-bottom: 50px;
            padding-left: 30px;
            padding-right: 30px;
            background: #DADADA;
            font-family: 'Flexo-Italic';
            color: #FF4F00;
            border-style: solid;
            border-color: #ffffff;
        }

            .message div {
                font-size: 19px;
            }

        .btn-aceptar {
            display: block;
            width: 112px;
            height: 25px;
            background: url('../images/promo2015septiembre/message-bt-regresar.png') no-repeat;
            left: 35px;
            top: 336px;
            z-index: 60;
            margin-left: 152px;
            margin-top: 32px;
        }


    </style>
</head>
<body>
    <div class="message">
        <div>
            Si no activaste tu Tarjeta de Crédito BCP LATAM Pass<br /> 
            hasta el <b>31/07</b> no podrás participar de la promoción.
        </div>
        <a href="javascript:void(0)" class="btn-aceptar" onclick="parent.closeMessage();"></a>
    </div>
</body>
</html>
