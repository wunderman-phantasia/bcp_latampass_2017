﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="CntDefault" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        Cufon.replace('.aviBox h1', { fontFamily: 'Flexo-Light' }); // Requires a selector engine for IE 6-7, see above
        Cufon.replace('.aviBox h3', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.pull-left', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.pull-right', { fontFamily: 'Flexo-Light' });
        //Cufon.replace('.bannerslide', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.slider .bannerslide[data-id="1"] h1', { fontFamily: 'Flexo-Bold' })
        //Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
        //Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
    </script>



    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }; if (!f._fbq) f._fbq = n;
            n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
        }(window,
        document, 'script', '//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1493357820896130');
        fbq('track', "PageView");</script>
    <noscript>
        <img height="1" width="1" style="display: none"
            src="https://www.facebook.com/tr?id=1493357820896130&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '159570524230911',
                xfbml: true,
                version: 'v2.7'
            });

            var page_like_or_unlike_callback = function (url, html_element) {
                dataLayer.push({
                    'event': 'socialEvent',
                    'network': 'Facebook',
                    'action': 'Like',
                    'target': 'http://www.bcplatampass.com/'
                });
                console.log("Tracking Like!");
            }

            FB.Event.subscribe('edge.create', page_like_or_unlike_callback);
            //FB.Event.subscribe('edge.remove', page_like_or_unlike_callback)
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));



    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <form id="frmMain" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <section id="roll" class="Content home">
            <div class="contentSlided">

                <div id="bannerSlider" class="slider">
                    <%--
                    <div class="bannerslide cont-banner-lanpass-ae" data-id="18" >
                        <div class="cont-int-lanpass-ae">
                            <h2>Viaja por Sudamerica desde <br>18,000 KMS LANPASS</h2>
                            <h3>Canjea del 12 al 17 de Abril en lan.com</h3>
                            <a class="btn-descubre" href="http://www.latam.com/latampass/es_pe/sitio_personas/lanpass/promociones/kms-que-vuelan-vuelve/vuelos-por-sudamerica/"></a>
                        </div> 
                        <div class="text-legal"></div>
                    </div>
                    --%>

                    <%-- <div data-id="12" class="bannerslide" style="position: relative;cursor:pointer;background: url('../images/slider/slider_andina.JPG') no-repeat;" onclick="window.location='http://www.latam.com/latampass/es_pe/sitio_personas/promociones/outlet-casa-andina-bcp-latam-pass/vuelos-desde-lima'" >
                    </div> --%>

                    <%--  <div data-id="11" class="bannerslide" style="position: relative;cursor:pointer;background: url('../images/slider/slider_bcp_amedias.JPG') no-repeat;" onclick="window.location='http://www.latam.com/latampass/es_pe/sitio_personas/promociones/venta-kms-bcp-junio-2016/terminos-condiciones/'" >
                    </div> --%>

                    <%-- <div data-id="12" class="bannerslide" style="position: relative;cursor:pointer;background: url('../images/slider/slider_vf.JPG') no-repeat;" onclick="window.location='http://www.latam.com/latampass/es_pe/sitio_personas/promociones/latam-pass-viaja-por-sudamerica/vuelos-desde-lima/'" >
                    </div> --%>

                    <%--<a data-id="13" class="bannerslide" style="position: relative;cursor:pointer;background: url('../images/slider/sorteokms.jpg') no-repeat;" href="sorteokms.pdf" target="_blank" >
                    </a> --%>

                    <div data-id="14" class="bannerslide" style="position: relative; cursor: pointer; background: url('../images/slider/latam_pass.png') no-repeat;" onclick="window.location='formulario.aspx'">
                    </div>

                    <%--<div data-id="14" class="bannerslide" style="position: relative;cursor:pointer;background: url('../images/slider/banner-para-bcplatampass-53-nuevotexto1.JPG') no-repeat;" onclick="window.location='https://www.bcplatampass.com/formulario.aspx '" >
                    </div> --%>

                    <%--
					<div data-id="13" class="bannerslide" style="position: relative;cursor:pointer;background: url('../images/slider/Slider_BCP_AMEX2016.JPG') no-repeat;" onclick="window.location='https://www.bcplatampass.com/amex2016/'" >
                    </div>
                    --%>



                    <%--                    <div data-id="20" class="bannerslide venta50" style="position: relative;cursor:default !important;" onclick="javascript:void(0);">
                    </div>
                    --%>
                    <%--<div class="bannerslide bannerpromoamexx5" style="position: relative;cursor:pointer;" onclick="window.location='http://www.bcplatampass.com/promoamexx5'">
                    </div> 
                    --%>


                    <%--<div class="bannerslide bannerultimallamada cont-banner-lanpass-ae">
                        <div class="cont-int-lanpass-ae">
                          <h2>Este es el momento que estabas esperando</h2>
                          <a href="http://www.latam.com/latampass/catalogolanpassperu/" target="_blank" class="btn-descubre"></a>
                        </div>             
                    </div>
                    --%>
                    <%--
                    <div class="bannerslide bannernavidad2014" data-id="17">
                        <div class="cont-int-tpd">
                            
                            <h2>Adelantar el regalo de<br>
                                quien quieres, te lleva<br>
                                a donde quieres.</h2>
                            
                            <h3 style="padding-bottom: 0">Comprando en:</h3>
                            
                            <ul class="bullet" style="margin-top: 0; padding-left: 20px;">
                                <li><span>Tiendas por departamento.</span></li>
                                <li><span>Jugueterías.</span></li>
                                <li><span>Librerías.</span></li>
                            </ul>

                            <a href="promo-navidad.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>  
                    --%>
                    <%--
                    <div class="bannerslide bannercanjea cont-banner-lanpass-ae" data-id="18">
                        <div class="cont-int-lanpass-ae">
                          <h2>&iexcl;Volvi&oacute; Kms. que vuelan!</h2>
                          <h3>Viaja por el Perú  desde 7,000 KMS. LANPASS o por Sudamérica desde 18,000 KMS. LANPASS.  </h3>
                          <a href="http://www.latam.com/latampass/es_pe/sitio_personas/lanpass/promociones/kms-que-vuelan-bcp/vuelos-desde-lima/" target="_blank" class="btn-descubre"></a>
                        </div> 
                        <div class="text-legal">*Tarifa desde 7,000 kms. aplica a destinos nacionales, tarifa desde 18,000 kms. solo aplica a La Paz y Santa Cruz. Ver t&eacute;rminos y condiciones.</div>
                    </div>               
                    --%>
                    <%--
                    <div class="bannerslide bannerusala" data-id="16">
                        <div class="cont-int-tpd">
                            <h2>En octubre,<br />
                                usa <span style="font-family: 'Flexo-Bold';">todos los días tu</span><br />
                                <span style="font-family: 'Flexo-Bold';">Tarjeta de Crédito BCP LANPASS</span><br />
                                y gana hasta <span style="font-family: 'Flexo-Bold';">50,000 kms.</span></h2>
                            <a href="usala-todos-los-dias.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>
                    --%>
                    <%--
                    <div class="bannerslide banneramex" data-id="15">
                        <div class="cont-int-lanpass-ae">
                            <h2>Tus compras te acercan<br />
                                a tu próximo viaje</h2>
                            <a href="promo-amex.aspx" class="btn-descubre"></a>
                        </div>
                    </div>
                    <div data-id="14" class="bannerslide bannersupermercadosx5">
                        <div class="cont-int-tpd">
                            <h2>Compra en supermercados con tus<br />
                                <span style="font-family: 'Flexo-Italic'; font-size: 19px;">Tarjetas de Crédito BCP LANPASS</span><br />
                                y acércate a tu próximo viaje</h2>
                            <a href="promo-supermercados.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>
                    <div data-id="14" class="bannerslide bannersupermercadosx5b">
                        <div class="cont-int-tpd">
                            <h2>Compra en supermercados con tus<br />
                                <span style="font-family: 'Flexo-Italic'; font-size: 19px;">Tarjetas de Crédito BCP LANPASS</span><br />
                                y acércate a tu próximo viaje</h2>
                            <a href="promo-supermercados.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>
                    --%>
                </div>
                <div id="pagSlide" class="pagSlide"></div>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#bannerSlider").carouFredSel({
                            auto: { play: true, timeoutDuration: 8000 },
                            items: 1,
                            scroll: { fx: "fade", items: 1, pauseOnHover: true, duration: 800 },
                            pagination: "#pagSlide"
                        });
                    })
                </script>


                <div style="margin-top: 18px; ">
                    <div style="margin: 0 auto; padding-left: 17%;">
                        <div class="avion" tabindex="11" title="Información de kilómetros para los principales destinos: Iquitos 14mil kilómetros, Tarapoto 10mil kilómetros, Trujillo 10mil kilómetros, Cuzco 14mil kilómetros, Arequipa 14mil kilómetros, Santiago 26mil kilómetros, Bogotá 28mil kilómetros, Miami 48mil kilómetros.">
                            <div class="aviBox">
                                <img src="images/flightboard/flightboard_icon.jpg" width="54" height="54" alt="">
                                <h1>¡Viaja a estos destinos!</h1>
                                <h3>Comprando con tus <span>Tarjetas BCP LATAM Pass</span> estarás cada vez más cerca de ellos.</h3>
                                <div class="clear"></div>
                            </div>
                            <div class="aviBox pull-left">
                                Destinos Recomendados
                            </div>
                            <div class="aviBox pull-right">
                                Viaja desde:
                            </div>
                            <div class="clear"></div>

                            <div class="Mainflights">
                                <div class="flightboardline">
                                    <div data-id="1" class="basicBoard fliptext"></div>
                                    <div data-id="1" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="2" class="basicBoard fliptext"></div>
                                    <div data-id="2" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="3" class="basicBoard fliptext"></div>
                                    <div data-id="3" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="4" class="basicBoard fliptext"></div>
                                    <div data-id="4" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="5" class="basicBoard fliptext"></div>
                                    <div data-id="5" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="6" class="basicBoard fliptext"></div>
                                    <div data-id="6" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="vIE">
                                <div class="flightboardline">
                                    <div data-id="7" class="basicBoard fliptext"></div>
                                    <div data-id="7" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="8" class="basicBoard fliptext"></div>
                                    <div data-id="8" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="9" class="basicBoard fliptext"></div>
                                    <div data-id="9" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="10" class="basicBoard fliptext"></div>
                                    <div data-id="10" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="11" class="basicBoard fliptext"></div>
                                    <div data-id="11" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                                <div class="flightboardline">
                                    <div data-id="12" class="basicBoard fliptext"></div>
                                    <div data-id="12" class="basicBoard2 fliptext"></div>
                                    <div class="letterlong"><span class="letterpadding">KMS. LATAM Pass </span></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="Pagflights">
                                <a href="javascript:void(0);" rel="01" class="activo">1</a>
                                <a href="javascript:void(0);" rel="02">2</a>
                            </div>

                            <a href="javascript:void(0);" class="arrow-prev" style="display: none;"></a>
                            <a href="javascript:void(0);" class="arrow-next"></a>

                        </div>

                        <%--<div class="banners">
                        <div class="banner1"><a class="btnbanner1" href="formulario.aspx" tabindex="12" title="Pide tu Tarjeta BCP LATAM Pass online.">¡Pídela aquí!</a></div>
                        
                        <!--<div class="banner2"><a class="btnbanner2" href="faustino.aspx" tabindex="13" title="¡Conoce a Faustino!">¡Conoce a Faustino!</a></div>-->

                        <!--<div class="banner4"><a href="http://www.latam.com/latampass/catalogolanpassperu/" tabindex="13" title="Catálogo LANPASS" target="_blank"></a></div>-->

                        <!--
						<div class="banner3">
                            <div class="Doc One">
                                <ul id="selectDocType">
                                    <li class="lightitalic11dark" data-id="1"><span style="float: left;">DNI</span><div class="comboselect"></div>
                                    </li>
                                    <li class="lightitalic11dark unselected" data-id="1">DNI</li>
                                    <li class="lightitalic11dark unselected" data-id="2">Carné de extranjería</li>
                                </ul>
                                <input id="inputDoc" data-id="1" type="text" maxlength="8" placeholder="Ingresa tu DNI" class="inputDoc" tabindex="14" title="Ingresa tu número de documento y presiona ENTER">
                                <p class="lightitalic11orange inputDocerror" tabindex="15" title="Tu número de documento debe tener 8 números."></p>
                                <a class="btnbanner3" href="" tabindex="16" title="Consulta">Consulta</a>
                            </div>
                        </div>
						-->
                    </div>--%>
                    </div>
            </div>
            <div class="clear"></div>

            </div>
        </section>
    </form>
    <div class="modal2"></div>
    <div data-id="1" class="modal">
        <div message-id="1" class="message">
            <div class="popupbox">
                <span class="ModalClose bolditalic30orange">&times;</span>
                <p class="bolditalic23orange popuptitle" id="data-nombre-1">&ltNombre&gt</p>
                <p class="lightitalic23orange popupttext1" id="data-titulo-1">¡Traslada tu Deuda de otros bancos al BCP y gana un Bono de Kilómetros*!</p>
                <p class="lightitalic14dark popupttext2" id="data-descrip-1">*Conoce más de esta oferta acercándote a una Agencia BCP o llamando a nuestra Banca por Teléfono al 311-9898 y disfruta de los beneficios de las Tarjetas de Crédito BCP LATAM Pass!</p>
                <p class="lightitalic11dark popupttext3" id="data-subdescrip-1">Válido hasta el &lt;fecha de vencimiento&gt; sujeto a evaluación crediticia.</p>
            </div>
        </div>
    </div>

    <div data-id="5" class="modal">
        <div class="message messagesmall">
            <div class="popupbox">
                <span class="ModalClose bolditalic30orange">&times;</span>
                <p class="lightitalic23orange popupttext1" style="padding-top: 55px;">Por el momento, no tenemos una oferta activa para ti.</p>
                <p class="lightitalic14dark popupttext2">¡Te mantendremos informado!</p>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="pull-left" style="width: 100px; margin-top: 25px;">
            Compartir en: 
        </div>
        <div class="pull-left" style="width: 50px; margin-top: 20px;">
            <a class="fbbuttonclick" href="http://www.bcplatampass.com/">
                <img src="images/facebook.png" width="22" height="22" alt=""></a>
        </div>
        <div class="pull-left" style="margin-top: 20px;">
            <div class="fb-like" data-href="http://www.bcplatampass.com/" data-width="450" data-layout="button_count" data-action="like" data-show-faces="false" data-send="false"></div>
        </div>
    </footer>
    <script type="text/javascript" src="js/jquery.flightboard.js"></script>
    <script src="js/jquery.history.js"></script>
    <script src="js/JScript_Inicio.js" type="text/javascript"></script>
    <script type="text/javascript">
        _gaq.push(['_trackPageview', '/WebsiteOficial/home']);
        //$(".active").removeClass("active");
        $("#Inicio").css({ "display": "none" });
        $('.avion').css('cursor', 'pointer').on('click', function () {
            window.open("https://www.latam.com/es_pe/latam-pass/");
        });
    </script>

    <script>        /*(function() {
              var _fbq = window._fbq || (window._fbq = []);
              if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
              }
              _fbq.push(['addPixelId', '1493357820896130']);
            })();
            window._fbq = window._fbq || [];
            window._fbq.push(['track', 'PixelInitialized', {}]);*/
    </script>
    <!--<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1493357820896130&amp;ev=PixelInitialized" /></noscript>-->

    <!-- Facebook Conversion Code for BCP Lan Pass - AON - Noviembre 2014 -->
    <script>(function () {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
    }
})();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6017646669790', { 'value': '0.00', 'currency': 'USD' }]);
    </script>
    <noscript>
        <img height="1" width="1" alt="" style="display: none" src="https://www.facebook.com/tr?ev=6017646669790&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
    <script>
        function postToFeed(elem) {
            var url = $(elem).attr('href'),
				title = $(elem).attr('data-fctitle'),
				fcdescription = $(elem).attr('data-fcdescription'),
				marcacion = $(elem).attr('data-marcacion'),
				dataImagen = $(elem).attr('data-fcimage');

            var obj = {
                method: 'feed',
                link: url,
                picture: dataImagen,
                name: title,
                caption: 'http://www.bcplatampass.com/',
                description: fcdescription
            };

            function callback(response) {
                if (response) {
                    console.log("Tracking Compartir Exitosamente!.")
                    dataLayer.push({
                        'event': 'socialEvent',
                        'network': 'Facebook',
                        'action': 'Compartir',
                        'target': 'http://www.bcplatampass.com/'
                    });
                }
            }
            FB.ui(obj, callback);
        }
        $(document).on("click", ".fbbuttonclick", function (event) {
            event.preventDefault();
            postToFeed(this);
        });
    </script>
</asp:Content>

