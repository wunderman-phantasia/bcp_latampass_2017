﻿using System;
using System.Web;

public class MyHtttpRemovalHandler : IHttpModule
{
    public void Init(HttpApplication context)
    {
        context.EndRequest += new EventHandler(this.handle_EndRequest);
    }

    private void handle_EndRequest(object sender, EventArgs e)
    {
        var response = HttpContext.Current.Response;
        if (response != null)
        {
            var responseHeaders = response.Headers;
            responseHeaders.Remove("X-AspNet-Version");
            responseHeaders.Remove("X-Powered-By");
            responseHeaders.Remove("Server");
        }
    }

    public void Dispose()
    {
    }
}