using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using System.Web.Mail;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

/// <summary>
/// Summary description for MailHelper
/// </summary>
public class BLMailHelper
{
    private String SMTP_SERVER = ConfigurationManager.AppSettings["SMTPSERVER"].ToString();
    public bool EnviarMail(String destinatario, string subject, string body, bool html = true)
    {
        MailMessage mail = new MailMessage();
        if (ConfigurationManager.AppSettings["CORREO-PRUEBA-ACTIVO"].ToString() == "1")
        {
            string[] dest = ConfigurationManager.AppSettings["CORREO-PRUEBA"].ToString().Split(';');
            foreach (var item in dest)
            {
                mail.To.Add(item);
            }
        }
        else
        {
            string[] dest = destinatario.Split(';');
            foreach (var item in dest)
            {
                mail.To.Add(item);
            }
        }
        mail.From = new MailAddress(ConfigurationManager.AppSettings["SMTPFROM"].ToString());
        NetworkCredential basicCredential = new NetworkCredential(mail.From.Address, ConfigurationManager.AppSettings["SMTPPASS"].ToString());

        mail.Subject = subject;
        mail.Body = body;
        if(html) mail.IsBodyHtml = true;
        mail.Priority = MailPriority.High;

        SmtpClient mSmtpClient = new SmtpClient();
        mSmtpClient.Host = SMTP_SERVER;
        mSmtpClient.UseDefaultCredentials = false;
        mSmtpClient.Credentials = basicCredential;

        mSmtpClient.Host = SMTP_SERVER;
        try { mSmtpClient.Send(mail); return true; }
        catch (Exception ex) { new BLLogHelper().RegistrarLog("Error", "MailHelper.EnviarMail(" + destinatario + " ,asunto: " + subject + " ,mensaje: " + body + ")", ex); return false; }
    }
}

