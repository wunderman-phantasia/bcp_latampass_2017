﻿using System.Web;
using DA;

/// <summary>
/// Summary description for BLOferta
/// </summary>
public class BLOferta
{
	public string[] Consultar_Ofertas(string Numdoc, string TipoDoc, HttpRequest Request)
    {
        return new DAOferta().Consultar_Ofertas(Numdoc, TipoDoc, new BLCliente().Obtener_IP(Request));
    }
    public int Grabar_Oferta(int id, string codigo,string titulo, string descripcion, string anexo, bool estado)
    {
        return new DAOferta().Grabar_Oferta(id, codigo, titulo, descripcion, anexo, estado);
    }
    public tbl_lan_oferta Datos_Oferta(int id)
    {
        return new DAOferta().Datos_Oferta(id);
    }
}