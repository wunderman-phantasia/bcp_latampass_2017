﻿using System;
using System.Text.RegularExpressions;
using DA;

public class BLPromocion
{
    public bool HayCuposLibres()
    {
        return new DaoPromocion().HayCuposLibres();
    }

    public MetaBono BuscarMetaYBono(string lstrTipoDocumento, string lstrNumeroDocumento)
    {
        var dataAccess = new DA.DaoPromocion();
        lstrTipoDocumento = getTipoDocumento(lstrTipoDocumento);
        var clienteYaRegistrado = dataAccess.ClienteYaRegistrado(lstrTipoDocumento, lstrNumeroDocumento);
        if (clienteYaRegistrado)
            return new MetaBono
            {
                Resultado = 0,
                Meta = 0,
                Bono = 0
            };

        var noHayCuposLibres = !HayCuposLibres();
        if (noHayCuposLibres)
            return new MetaBono
            {
                Resultado = 1,
                Meta = 0,
                Bono = 0
            };

        return dataAccess.ObtenerMetaYBono(lstrTipoDocumento, lstrNumeroDocumento);
    }

    private static string getTipoDocumento(string lstrTipoDocumento)
    {
        return lstrTipoDocumento.ToLower() == "dni"
            ? "1"
            : lstrTipoDocumento.ToLower() == "pa"
                ? "4"
                : "3";
    }

    private static string getDescripcionDocumento(string lstrTipoDocumento)
    {
        return lstrTipoDocumento.ToLower() == "1"
            ? "DNI"
            : lstrTipoDocumento.ToLower() == "4"
                ? "Nro de pasaporte"
                : "Carné de extranjería";
    }


    public string RegistrarCliente(Guid lgIdRegistro, string lstrNombre, string lstrPaterno, string lstrMaterno, string lstrCorreoElctronico, string lstrTipoDocumento, string lstrNumeroDocumento, string lstrtelefono, string lstrDpto)
    {
        var result = ValidarEmail(lstrCorreoElctronico);
        if (!String.IsNullOrWhiteSpace(result))
            return result;

        result = ValidarTelefono(lstrtelefono);
        if (!String.IsNullOrWhiteSpace(result))
            return result;

        var dataAccess = new DaoPromocion();
        if (dataAccess.EmailYaRegistrado(lstrCorreoElctronico))
            return "Este correo electronico ya está registrado. Por favor, ingresa uno nuevo.";

        var clienteYaRegistrado = dataAccess.ClienteYaRegistrado(lstrTipoDocumento, lstrNumeroDocumento);
        if (clienteYaRegistrado)
            return "Este cliente ya está registrado.";

        lstrTipoDocumento = getTipoDocumento(lstrTipoDocumento);
        return dataAccess.RegistrarCliente(
            lgIdRegistro,
            lstrNombre,
            lstrPaterno,
            lstrMaterno,
            lstrCorreoElctronico,
            lstrTipoDocumento,
            lstrNumeroDocumento,
            lstrtelefono,
            lstrDpto);
    }

    private string ValidarTelefono(string lstrCorreoElctronico)
    {
        bool isEmail = !String.IsNullOrWhiteSpace(lstrCorreoElctronico) && lstrCorreoElctronico.Length > 5 && Regex.IsMatch(lstrCorreoElctronico, @"^[0-9]*$", RegexOptions.IgnoreCase);
        if (isEmail) return "";
        return "Tu teléfono o celular debe tener mínimo 6 números.";
    }

    private string ValidarEmail(string lstrCorreoElctronico)
    {
        bool isEmail = !String.IsNullOrWhiteSpace(lstrCorreoElctronico) && Regex.IsMatch(lstrCorreoElctronico, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        if (isEmail) return "";
        return "Tu e-mail debe tener @ y una extensión válida (.pe, .com, .net, etc.).";
    }


    public Cliente BuscarCliente(string idCliente)
    {
        var dataAccess = new DA.DaoPromocion();
        var result = dataAccess.BuscarCliente(idCliente);
        result.TipoDocumento = getDescripcionDocumento(result.TipoDocumento);
        return result;
    }
}