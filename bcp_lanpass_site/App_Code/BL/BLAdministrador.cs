﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DA;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;

/// <summary>
/// Summary description for BLRegistrarUsuario
/// </summary>
public class BLAdministrador
{
    public PA_LANPASS_CMS_VALIDAR_ADMINISTRADORResult ValidarAdministrador_x_Usuario(string usuario)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                return db.PA_LANPASS_CMS_VALIDAR_ADMINISTRADOR(usuario).SingleOrDefault();
            }
        }
        catch (Exception ex) { new BLLogHelper().RegistrarLog("Error", "ValidarAdministrador_x_Correo(" + usuario + ")", ex); return null; }
    }
    public BEUsuario ValidarAdministrador(string usuario, string clave)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                PA_LANPASS_CMS_VALIDAR_ADMINISTRADORResult sp = ValidarAdministrador_x_Usuario(usuario);
                if (sp != null)
                {
                    if (sp.Clave == clave) return new BEUsuario(sp.ID, sp.Nombres);
                }
                return new BEUsuario(0, "");
            }
        }
        catch (Exception ex) { new BLLogHelper().RegistrarLog("Error", "ValidarAdministrador(" + usuario + ")", ex); return null; }
    }
    public int Grabar_Administrador(int id, string usuario, string clave, bool estado)
    {
        bool validar=true;
        if (id != 0) validar = false;
        if (validar)
        {
            if (ValidarAdministrador_x_Usuario(usuario) != null) return -1; // Ya existe usuario, no se puede registrar uno nuevo
        }
        return new DAAdministrador().Grabar_Administrador(id, usuario, clave, estado);
    }
    public tbl_lan_administrador Datos_Administrador(int id)
    {
        return new DAAdministrador().Datos_Administrador(id);
    }
}