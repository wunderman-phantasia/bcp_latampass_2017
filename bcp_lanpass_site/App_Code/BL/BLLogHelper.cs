using System;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for LogHelper
/// </summary>
/// 
public class BLLogHelper
{
    public void RegistrarLog(string tipo, string funcion, Exception error)
    {
        try
        {
            StreamWriter swWriter;
            string ruta = ConfigurationManager.AppSettings["RUTA_LOG"].ToString();
            if (!Directory.Exists(ruta))
                Directory.CreateDirectory(ruta);
            swWriter = File.AppendText(ruta + "LanPassBCP-" + tipo + "_" + DateTime.Today.Year.ToString() + "-" + DateTime.Today.Month.ToString() + "-" + DateTime.Today.Day.ToString() + ".txt");
            string separador = "------------------------------------------------------------------";
            swWriter.WriteLine(separador);
            swWriter.WriteLine("Funcion: " + funcion);
            if (error != null)
            {
                swWriter.WriteLine("Fuente: " + error.Source);
                swWriter.WriteLine("Mensaje: " + error.Message);
                swWriter.WriteLine("Tipo: " + error.GetType());
                swWriter.WriteLine("Trace: " + error.StackTrace);
                swWriter.WriteLine("Control: " + error.TargetSite);
            }
            swWriter.WriteLine("Usuario CPU: " + Environment.UserName);
            swWriter.WriteLine("CPU: " + Environment.MachineName);
            swWriter.WriteLine("Fecha: " + DateTime.Now);
            swWriter.WriteLine(separador);
            swWriter.Close();
        }
        catch { }
    }
}