﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DA;

/* @001 LAG 07/02/2014 Se agregó tipo de documento en la inserción de datos del cliente */
/* @002 LAG 07/02/2014 Se modificó el método Validar_Cliente */
public class BLCliente
{
    /*I@002*/
    public bool Validar_Cliente(string documento, string tipo_doc)
    {
        using (DataClsLanPass db = new DataClsLanPass())
        {
            if (db.PA_LANPASS_VALIDAR_CLIENTE(documento,tipo_doc).Single().EXISTENCIA == 1) return true;
            else return false;
        }
    }
    /*F@002*/
    public bool? Registrar_Cliente(string tarjeta, string nombre, string apepaterno, string apematerno, string telefono, string tipo_doc, string nro_doc, string email, bool ctasueldo, int sueldo, string moneda, string ip, string utm_source, string utm_medium, string utm_campaign, int disenotarjetaId)
	{
        if (!Validar_Cliente(nro_doc, tipo_doc))
        {
            tbl_lan_cliente o = new tbl_lan_cliente();
            o.cli_str_tarjeta = tarjeta;
            o.cli_str_nombre = nombre;
            o.cli_str_apepaterno = apepaterno;
            o.cli_str_apematerno = apematerno;
            o.cli_str_telefono = telefono;
            /* I@001*/
            o.cli_str_tipodocumento = tipo_doc;
            /* F@001*/
            o.cli_str_dni = nro_doc;
            o.cli_str_email = email;
            o.cli_bit_ctasueldo = ctasueldo;
            o.cli_int_sueldomensual = sueldo;
            o.cli_str_moneda = moneda;
            o.cli_dat_registro = DateTime.Now;
            o.cli_str_ip = ip;
            o.cli_int_cantconsultas = 0;

            o.cli_str_utm_source = utm_source;
            o.cli_str_utm_medium = utm_medium;
            o.cli_str_utm_campaign = utm_campaign;
            o.cli_int_disenotarjetaId = disenotarjetaId; // se agrego la nueva columna para tarjeta de diseño de elije tu tarjeta

            return new DACliente().Registrar_Cliente(o);
        }
        return null;
	}
    public bool Grabar_OfertasxCliente(List<tbl_lan_clienteoferta> lista)
    {
        return new DACliente().Grabar_OfertasxCliente(lista);
    }
    public string Obtener_IP(HttpRequest Request)
    {
        // Recuperamos la IP de la máquina del cliente
        // Primero comprobamos si se accede desde un proxy
        string ipAddress1 = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        // Acceso desde una máquina particular
        string ipAddress2 = Request.ServerVariables["REMOTE_ADDR"];

        string ipAddress = string.IsNullOrEmpty(ipAddress1) ? ipAddress2 : ipAddress1;

        // Devolvemos la ip
        return ipAddress;
    }
}