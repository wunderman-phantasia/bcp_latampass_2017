﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DA;

/// <summary>
/// Summary description for DACliente
/// </summary>
public class DACliente
{
    public bool Registrar_Cliente(tbl_lan_cliente o)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                db.tbl_lan_cliente.InsertOnSubmit(o);
                db.SubmitChanges();
                return true;

            }
        }
        catch (Exception ex)
        {
            new BLLogHelper().RegistrarLog("Error", "DACliente.Registrar_Cliente", ex); return false;
        }
    }
    public bool Grabar_OfertasxCliente(List<tbl_lan_clienteoferta> lista)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                db.ExecuteCommand("TRUNCATE TABLE tbl_lan_clienteoferta");
                db.SubmitChanges();
                //db.tbl_lan_clienteofertas.DeleteAllOnSubmit(db.tbl_lan_clienteofertas.ToList());
                db.tbl_lan_clienteofertas.InsertAllOnSubmit(lista);
                db.SubmitChanges();
                return true;
            }
        }
        catch (Exception ex)
        {
            new BLLogHelper().RegistrarLog("Error", "DACliente.Grabar_OfertasxCliente", ex); return false;
        }
    }
}