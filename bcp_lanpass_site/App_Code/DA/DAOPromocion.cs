﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace DA
{

    public class MetaBono
    {
        public string IdCliente { get; set; }
        public int Resultado { get; set; }
        public long Meta { get; set; }
        public long Bono { get; set; }
    }

    public class Cliente
    {
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Documento { get; set; }
        public string TipoDocumento { get; set; }
        public long Meta { get; set; }
        public long Bono { get; set; }
    }

    public class DaoPromocion
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["DB"].ToString();

        public bool HayCuposLibres()
        {
            const string queryString = "SELECT COUNT(*) FROM BLTT_REGISTRO_2015_SEPTIEMBRE";
            using (var connection =
                new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                var reader = command.ExecuteScalar();
                return (reader as int?) < 20000;
            }
        }

        public bool ClienteYaRegistrado(string lstrTipoDocumento, string lstrNumeroDocumento)
        {
            var queryString =
                String.Format(
                    "SELECT COUNT(*) FROM BLTT_REGISTRO_2015_SEPTIEMBRE WHERE REGV_TipoDocumento = '{0}' AND REGV_NumerDocumento = '{1}'",
                    lstrTipoDocumento, lstrNumeroDocumento);

            using (var connection =
                new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                var reader = command.ExecuteScalar();
                return (reader as int?) > 0;
            }
        }

        public bool EmailYaRegistrado(string lstrEmail)
        {
            var queryString =
                String.Format(
                    "SELECT COUNT(*) FROM BLTT_REGISTRO_2015_SEPTIEMBRE WHERE REGV_CorreoElectronico = '{0}'",
                    lstrEmail);

            using (var connection =
                new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                var reader = command.ExecuteScalar();
                return (reader as int?) > 0;
            }
        }

        public MetaBono ObtenerMetaYBono(string lstrTipoDocumento, string lstrNumeroDocumento)
        {
            var queryString =
                String.Format(
                    "SELECT CLIG_IdRegistro, CLII_Meta, CLII_BONO FROM BLTT_CLIENTES WHERE CLIV_TipoDocumento = '{0}' AND CLIV_NumerDocumento = '{1}'",
                    lstrTipoDocumento, lstrNumeroDocumento);

            using (var connection =
                new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                var reader = command.ExecuteReader();

                try
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new MetaBono
                        {
                            Resultado = 3,
                            IdCliente = reader[0].ToString(),
                            Meta = long.Parse(reader[1].ToString()),
                            Bono = long.Parse(reader[2].ToString())
                        };
                    }
                    return new MetaBono
                    {
                        Resultado = 2,
                        Meta = 0,
                        Bono = 0
                    };
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        public string RegistrarCliente(
            Guid lgIdRegistro,
            string lstrNombre,
            string lstrPaterno,
            string lstrMaterno,
            string lstrCorreoElctronico,
            string lstrTipoDocumento,
            string lstrNumeroDocumento,
            string lstrtelefono,
            string lstrDpto)
        {
            if (ClienteYaRegistrado(lstrTipoDocumento, lstrNumeroDocumento))
                return "Este cliente ya se encuentra registrado.";

            var insertQuery =
                String.Format(
                    "INSERT INTO [BCPLANTRIPLICADB].[dbo].[BLTT_REGISTRO_2015_SEPTIEMBRE] " +
                    " ([REGG_IdRegistro] " +
                    " ,[REGV_Nombres] " +
                    " ,[REGV_ApellidoPaterno] " +
                    " ,[REGV_ApellidoMaterno] " +
                    " ,[REGV_CorreoElectronico] " +
                    " ,[REGV_TipoDocumento] " +
                    " ,[REGV_NumerDocumento] " +
                    " ,[REGV_Departamento] " +
                    " ,[REGD_FechaCreacion] " +
                    " ,[REGV_Telefono]) " +
                    " VALUES " +
                    " (NEWID() " +
                    " ,'{0}'" +
                    " ,'{1}'" +
                    " ,'{2}'" +
                    " ,'{3}'" +
                    " ,'{4}'" +
                    " ,'{5}'" +
                    " ,'{6}'" +
                    " ,GETDATE()" +
                    " ,'{7}')",
                    lstrNombre,
                    lstrPaterno,
                    lstrMaterno,
                    lstrCorreoElctronico,
                    lstrTipoDocumento,
                    lstrNumeroDocumento,
                    lstrDpto,
                    lstrtelefono);

            using (var connection =
                new SqlConnection(_connectionString))
            {
                connection.Open();

                var insertCommand = new SqlCommand(insertQuery, connection);
                insertCommand.ExecuteNonQuery();

                return "";
            }
        }

        public Cliente BuscarCliente(string idCliente)
        {
            var queryString =
                String.Format(
                    "SELECT [CLIG_IdRegistro] ,[CLIV_Nombres] ,[CLIV_ApellidoPaterno] ,[CLIV_ApellidoMaterno] ,[CLIV_TipoDocumento] ,[CLIV_NumerDocumento] ,[CLII_Meta] ,[CLII_Bono] ,[CLIV_GrupoCampania] FROM BLTT_CLIENTES WHERE CLIG_IdRegistro = '{0}'",
                    idCliente);

            using (var connection =
                new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                var reader = command.ExecuteReader();

                try
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new Cliente
                        {
                            Nombres = reader[1].ToString().Trim(),
                            ApellidoPaterno = reader[2].ToString().Trim(),
                            ApellidoMaterno = reader[3].ToString().Trim(),
                            TipoDocumento = reader[4].ToString().Trim(),
                            Documento = reader[5].ToString().Trim(),
                            Meta = long.Parse(reader[6].ToString().Trim()),
                            Bono = long.Parse(reader[7].ToString().Trim())
                        };
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

            return new Cliente();
        }
    }
}