﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DA;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for DAAdministrador
/// </summary>
/* @001 LAG 24/01/2014 Se agregó validación en la clave */
public class DAAdministrador
{
    public int Grabar_Administrador(int id, string usuario, string clave, bool estado)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                string hash = BitConverter.ToString(new SHA512Managed().ComputeHash(new ASCIIEncoding().GetBytes(clave))).Replace("-", "");
                tbl_lan_administrador adm = new tbl_lan_administrador();
                if (id != 0)
                {
                    adm = db.tbl_lan_administradors.Where(x => x.adm_int_id == id).Single();
                }
                adm.adm_bit_activo = estado;
                adm.adm_str_nombre = usuario;
                /* @001 */
                if (clave.Trim() != "xxxxxxxxxx")
                {
                    adm.adm_str_clave = hash;
                }
                /* @001*/
                if (id == 0)
                {
                    adm.adm_dat_registro = DateTime.Now;
                    db.tbl_lan_administradors.InsertOnSubmit(adm);
                }
                db.SubmitChanges();
                return adm.adm_int_id;
            }
        }
        catch (Exception ex) { new BLLogHelper().RegistrarLog("Error", "Grabar_Administrador(" + usuario + ")", ex); return 0; }
    }
    public tbl_lan_administrador Datos_Administrador(int id)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                return db.tbl_lan_administradors.Where(x => x.adm_int_id == id).Single();
            }
        }
        catch (Exception ex)
        {
            new BLLogHelper().RegistrarLog("Error", "Datos_Administrador(" + id + ")", ex);
            return null;
        }
    }
}