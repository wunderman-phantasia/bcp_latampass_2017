﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DA;

/* @001 LAG 07/02/2014 Se agregó el método Validar_Cliente y se agregó el parámetro de tipo de documento*/
public class DAOferta
{
    public string[] Consultar_Ofertas(string Numdoc, string TipoDoc, string Ip)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                PA_LANPASS_CONSULTAR_OFERTAResult o = db.PA_LANPASS_CONSULTAR_OFERTA(Numdoc, TipoDoc).FirstOrDefault();
                if (o != null)
                {
                    string[] r = new string[5];
                    r[0] = o.ofe_str_codigo;
                    r[1] = o.cli_str_nombre;
                    r[2] = o.ofe_str_titulo;
                    r[3] = o.ofe_str_descripcion;
                    r[4] = o.ofe_str_subdescripcion;
                    return r;
                }
                else
                {
                    /* I@001 */
                    //if (!new BLCliente().Validar_Cliente(Numdoc, TipoDoc))
                    //{
                        if (db.tbl_lan_clientenoregistrados.Where(x => x.cli_str_documento == Numdoc && x.cli_str_tipodocumento == TipoDoc).Count() == 0)
                        {
                            tbl_lan_clientenoregistrado obj = new tbl_lan_clientenoregistrado();
                            obj.cli_dat_consulta = DateTime.Now;
                            obj.cli_int_cantconsultas = 1;
                            obj.cli_str_documento = Numdoc;
                            obj.cli_str_tipodocumento = TipoDoc;
                            obj.cli_str_ip = Ip;
                            db.tbl_lan_clientenoregistrados.InsertOnSubmit(obj);
                        }
                        else
                        {
                            tbl_lan_clientenoregistrado obj = db.tbl_lan_clientenoregistrados.Where(x => x.cli_str_documento == Numdoc && x.cli_str_tipodocumento == TipoDoc).Single();
                            obj.cli_str_ip = Ip;
                            obj.cli_int_cantconsultas = obj.cli_int_cantconsultas + 1;
                        }
                        db.SubmitChanges();
                    //}
                    /* F@001 */
                    return null;
                }
            }
        }
        catch (Exception ex)
        {
            new BLLogHelper().RegistrarLog("Error", "DAOferta.Consultar_Ofertas(" + Numdoc + "," + TipoDoc + ")", ex); return null;
        }
    }
    public int Grabar_Oferta(int id, string codigo, string titulo, string descripcion, string anexo, bool estado)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                tbl_lan_oferta e = new tbl_lan_oferta();
                if (id != 0)
                {
                    e = db.tbl_lan_ofertas.Where(x => x.ofe_int_id == id).Single();
                    e.ofe_dat_modificacion = DateTime.Now;
                }
                e.ofe_str_codigo = codigo;
                e.ofe_str_titulo = titulo;
                e.ofe_str_descripcion = descripcion;
                e.ofe_str_subdescripcion = anexo;
                e.ofe_bit_activo = estado;

                if (id == 0)
                {
                    e.ofe_dat_registro = DateTime.Now;
                    db.tbl_lan_ofertas.InsertOnSubmit(e);
                }
                db.SubmitChanges();
                return e.ofe_int_id;
            }
        }
        catch (Exception ex)
        {
            new BLLogHelper().RegistrarLog("Error", "Grabar_Oferta(" + id + "," + titulo + "," + descripcion + "," + anexo + ")", ex);
            return 0;
        }
    }
    public tbl_lan_oferta Datos_Oferta(int id)
    {
        try
        {
            using (DataClsLanPass db = new DataClsLanPass())
            {
                return db.tbl_lan_ofertas.Where(x => x.ofe_int_id == id).Single();
            }
        }
        catch (Exception ex)
        {
            new BLLogHelper().RegistrarLog("Error", "Actualizar_Oferta(" + id + ")", ex);
            return null;
        }
    }
}