﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de BEDisenoTarjeta
/// </summary>
public class BEDisenoTarjeta
{
    public int DisenoTarjetaId { get; set; }
    public string Titulo { get; set; }
    public bool FlagPublicar { get; set; }
    public int? Orden { get; set; }
    public int NroMeGustas { get; set; }
    public string RutaPlantilla { get; set; }

    //public string RutaFacebokk
    //{
    //    get
    //    {
    //        return String.Format("{0}/Plantillas/{1}", Utilitarios.Settings.Get<string>("SiteUrl"), this.RutaPlantilla);
    //    }
    //}
    public bool FlagDebito { get; set; }

    public BEDisenoTarjeta(int disenoTarjetaId, string titulo, bool flagPublicar, int orden, int nroMeGustas, string rutaPlantilla)
    {
        DisenoTarjetaId = disenoTarjetaId;
        Titulo = titulo;
        FlagPublicar = flagPublicar;
        Orden = orden;
        NroMeGustas = nroMeGustas;
        RutaPlantilla = rutaPlantilla;
    }
}