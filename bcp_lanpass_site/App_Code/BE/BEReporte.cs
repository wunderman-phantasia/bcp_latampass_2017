﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BEReporte
/// </summary>
public class BEReporte
{
    public string nombre { get; set; }
    public string html { get; set; }

    public BEReporte(string Nombre, string Html)
    {
        nombre = Nombre;
        html = Html;
    }
}