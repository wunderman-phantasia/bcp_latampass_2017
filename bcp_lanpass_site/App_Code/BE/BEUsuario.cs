﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BEUsuario
/// </summary>
public class BEUsuario
{
    public int id { get; set; }
    public string nombres { get; set; }

    public BEUsuario(int Id, string Nombres)
    {
        id = Id;
        nombres = Nombres;
    }
}