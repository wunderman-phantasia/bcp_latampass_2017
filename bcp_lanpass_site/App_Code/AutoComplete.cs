using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using BCPLANPASSDAL;


/// <summary>
/// Summary description for AutoComplete
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AutoComplete : System.Web.Services.WebService
{
    static string lstrConfig = ConfigurationManager.ConnectionStrings["BCPLPConnectionString"].ToString().Trim();
    public AutoComplete()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //----------------------------------Lista Palabras-----------------------
    [WebMethod]
    public string[] GetCompletionList(string prefixText, int count)
    {
        CPalabraDAL lobjDAL = new CPalabraDAL(lstrConfig);
        DataTable ldtResult = lobjDAL.mListarPalabra(prefixText);
        ldtResult.TableName = "Tabla";
        
        List<string> txtItems = new List<string>();
        String dbValues;

        foreach (DataRow row in ldtResult.Rows)
        {
            //String From DataBase(dbValues)
            dbValues = row["PALV_PALABRA"].ToString(); //Palabra
            //dbValues = row[3].ToString(); //Nombre video
            dbValues = dbValues.ToLower();
            txtItems.Add(dbValues);
        }

        return txtItems.ToArray();
        
        
    }
    //----------------------------------End Lista Palabras-----------------------

}

