﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="usala-todos-los-dias.aspx.cs" Inherits="amex" %>

<asp:Content ID="ContentX" ContentPlaceHolderID="HeadContent" runat="Server">
    <meta property="og:title" content="BCP - LANPASS">
    <meta property="og:site_name" content="BCP - LANPASS">
    <meta property="og:description" content="¡Del 1 al 31 de octubre, compra con tu Tarjeta de Crédito BCP LANPASS todos los días y llévate hasta 50,000 KMS. LANPASS sin sorteos! Inscríbete aquí.">
    <meta property="og:type" content="company">
    <meta property="og:url" content="http://www.bcplatampass.com/usala-todos-los-dias.aspx">
    <meta property="og:image" content="http://www.bcplatampass.com/images/fb_share.png?v=723897129387">
</asp:Content>

<asp:Content ID="PromoAmex" ContentPlaceHolderID="MainContent" runat="Server">

    <section class="ContentPromo">
        <div class="contentSlided">
            <div class="promo-usala">
                <div class="intern-tpd">
                    <a href="Default.aspx" class="btn-return"></a>
                    <div class="note">
                        <h1 style="font-family: 'Flexo-Italic'; font-size: 17px;">
                            <br />
                            En octubre<br />
                            usa <span style="font-family: 'flexobold_italic';">todos los días tu Tarjeta de Crédito</span><br />
                            <span style="font-family: 'flexobold_italic';">BCP LANPASS</span> y gana hasta 50,000 kms.<br />
                            <br />
                            <div style="margin-top: -14px;"><span style="font-family: 'Flexo-Light-Italic'; font-size: 15px; word-spacing: -2px;">Si olvidaste comprar un día, sigue consumiendo,<br />
                                aún tenemos bonos de kms. para ti.</span></div>
                        </h1>
                        <img src="images/cuadro.jpg" style="margin-left: -25px; margin-top: -17px;" />
                        <a href="javascript:void(0)" class="btn-suscribete sprite fancybox"></a>
                        <div style="font-family: 'Flexo-Italic'; font-size: 11px; margin-top: 35px;">*Sin sorteos y sin consumo mínimo.</div>
                    </div>
                </div>
                <img src="images/bg-landing-x5_1.jpg" width="982" height="149" />
                <div class="legal">
                    <p>Vigencia y plazo de inscripción: del 01/10/14 al 31/10/14. Aplican restricciones. Participan sólo clientes con Tarjeta de Crédito BCP LANPASS inscritos hasta el 31/10/2014 a través de www.viabcp.com, www.bcplatampass.com, Banca por Teléfono al 311 9898 opción 9-9 o Banca Exclusiva al 311 9797 y Facebook del BCP. Los consumos realizados con tarjetas adicionales también serán considerados pero el bono se entregará únicamente al cliente titular. Aplica para todos los establecimientos que acepten Tarjetas de Crédito como medio de pago. El bono de Kms. LANPASS será acreditado en cada cuenta de socio LANPASS ganador a más tardar el 31/12/14 y corresponderá a los KMS. LANPASS ganados de acuerdo a la tabla de bonos y los días consumidos, hasta por un monto máximo de 50,000 KMS. LANPASS. Monto máximo de abono por tarjeta habiente: 50,000 KMS. LANPASS. Para consumos menores a 15 días no habrá bono. Stock mínimo: 500,000 KMS. LANPASS. Para fines de abono, la tarjeta del cliente debe encontrarse activa y sin bloqueo. No acumulan Kms. las disposiciones de efectivo, compras de deuda, efectivo preferente, consumos en casinos, pago de impuestos, consumos en servicios legales y abogados y pagos de servicios que se realizan por www.viabcp.com ni cargos por débito automático. Los temas relacionados con el servicio de transporte de LAN, así como la operatividad del programa LANPASS, son de responsabilidad de LAN. Esta campaña está a cargo del BCP. La acumulación, canje, uso y demás condiciones aplicables a los KMS. LANPASS se rigen bajo el reglamento de LANPASS publicado en www.latam.com/es_pe/latam-pass/. Esta campaña está a cargo del BCP. Para más información sobre la campaña,  restricciones, costos y tributos aplicables a las Tarjetas de Crédito BCP LANPASS llama a nuestra Banca por Teléfono al 311-9898 o ingresa a viabcp.com.</p>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="pull-left" style="width: 100px; margin-top: 25px;">
            Compartir en: 
        </div>
        <div class="pull-left" style="width: 50px; margin-top: 20px;">
            <a class="fbbuttonclick" href="">
                <img src="images/facebook.png" width="22" height="22" alt=""></a>
        </div>
        <div class="pull-left" style="margin-top: 20px;">
            <div class="fb-like" data-href="http://www.bcplatampass.com/" data-width="450" data-layout="button_count" data-action="like" data-show-faces="false" data-send="false"></div>
        </div>
    </footer>

    <div id="fb-root"></div>
    <script type="text/javascript">
        var android, iphone, ipad, uagent, vwidth;
        uagent = navigator.userAgent.toLowerCase();
        android = uagent.search(/android/);
        iphone = uagent.search(/iphone/);
        ipad = uagent.search(/ipad/);
        vwidth = $(document).width();

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=171557349613774";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $(document).on('ready', function () {
            _gaq.push(['_trackPageview', '/WebsiteOficial/campana-usala-todos-los-dias']);

            $(".fbbuttonclick").on("click", function (event) {
                event.preventDefault();
                _gaq.push(['_trackEvent', 'WebProducto', 'Footer', 'Btn_Facebook']);
                window.open("https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.bcplatampass.com%2Fusala-todos-los-dias.aspx", '', 'left=20,top=20,width=500,height=550,toolbar=1,resizable=0');
            });
            $(".active").removeClass("active");

            $('.btn-suscribete').on('click', function () {
                $('body').removeClass('promolanpass');
                $.colorbox({
                    fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
                    innerHeight: true,
                    innerWidth: true,
                    width: 510,
                    height: 700,
                    onOpen: function () {
                        $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=0.99, maximum-scale=0.99');
                        $('#cboxClose').hide();
                        if ($.browser.msie) {
                            if (parseInt($.browser.version) == 6) {
                                $('#cboxOverlay').css({
                                    'position': 'absolute',
                                    'width': $(window).width(),
                                    'height': $(document).height()
                                });
                            }
                        }
                        if (android > 1) {
                            //Si es un dispositivo en android
                            $('#cboxOverlay').css({
                                'position': 'absolute',
                                'width': '285%',
                                'height': $(document).height(),
                                'overflow': 'hidden'

                            });
                            //var w_modal = $("#colorbox").width();
                            //var new_left = Math.ceil(w_modal / 2) * (-1);
                            nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                            $('#colorbox').css("left", nwidth + "px");
                        }
                        if (ipad > 1) {
                            //alert("Ipad");
                        }
                        if (iphone > 1) {
                            nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                            $('#colorbox').css("left", nwidth + "px");
                        }

                    },
                    onCleanup: function () {
                        $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
                    },
                    onClosed: function () { $('#cboxClose').hide(); },
                    onLoad: function () { $('#cboxClose').hide(); },
                    onComplete: function () {
                        iframe();
                        $('#cboxClose').show();
                        //                    _gaq.push(['_trackPageview', '/WebsiteOficial/campana-usala-todos-los-dias/formulario']);
                    },
                    close: '',
                    href: 'promoamex-usala/popformprom.aspx'
                });
            });
        });
        function CerrarVentana() {
            _gaq.push(['_trackEvent', 'WebsiteOficial', 'campana-usala-todos-los-dias', 'btn_enviar-datos']);
            $('body').addClass('promolanpass');
            $.colorbox({
                fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
                width: 600, height: 385,
                onOpen: function () { $('#cboxClose').hide(); },
                onClosed: function () { $('#cboxClose').hide(); },
                onLoad: function () { $('#cboxClose').hide(); },
                onComplete: function () {
                    iframe(); $('#cboxClose').show();
                    _gaq.push(['_trackPageview', '/WebsiteOficial/campana-usala-todos-los-dias/formulario-registrosatisfactorio']);
                },
                close: '',
                href: 'registro-correcto.aspx',
                onCleanup: function () {
                    $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
                }
            });
        }
        function RegistroDuplicado() {
            _gaq.push(['_trackEvent', 'WebsiteOficial', 'campana-usala-todos-los-dias', 'btn_enviar-datos']);
            $('body').addClass('promolanpass');
            $.colorbox({
                fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
                width: 473, height: 250,
                onOpen: function () { $('#cboxClose').hide(); },
                onClosed: function () { $('#cboxClose').hide(); },
                onLoad: function () { $('#cboxClose').hide(); },
                onComplete: function () { iframe(); $('#cboxClose').show(); },
                close: '',
                href: 'registro-duplicado.aspx',
                onCleanup: function () {
                    $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
                }
            });
        }
        function iframe() {
            $('iframe').load(function () { $(this).fadeIn('fast'); });
        }

        $(document).ready(function () {
            $(window).resize(function () {
                /*if (android > 1) {
                    $('#cboxOverlay').width({
                        'width': '2024',
                        'height': $(document).height()
                    });
                }*/

            });


        });
    </script>
    <!-- Google Code for BCP - &uacute;sala todos los d&iacute;as - Setiembre2014 -->
    <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1004399224;
        var google_conversion_label = "y3GpCIuttVYQ-NT33gM";
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004399224/?value=1.00&amp;currency_code=USD&amp;label=y3GpCIuttVYQ-NT33gM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
</asp:Content>

