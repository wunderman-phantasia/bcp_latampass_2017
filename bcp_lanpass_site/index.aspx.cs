﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCPPLANTRIPLICA_FE_DAL;
using System.Configuration;
using System.Data;

public partial class index : System.Web.UI.Page
{
    string lstrConnectionString = ConfigurationManager.ConnectionStrings["DB"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        btnGrabar.Attributes.Add("onclick", "return checkFields();");
        cboTipDoc.Attributes.Add("onchange", "setLength(this);");
    }
    private void mGrabarRegistro()
    {
        try
        {
            int lintResult = 0;
            Guid lgIdRegistro = Guid.NewGuid();
            string lstrNombre = txtNombre.Text.Trim();
            string lstrPaterno = txtApePat.Text.Trim();
            string lstrMaterno = txtApeMat.Text.Trim();
            string lstrCorreoElctronico = txtEmail.Text.Trim();
            string lstrTipoDocumento = cboTipDoc.SelectedValue.Trim();
            string lstrNumeroDocumento = txtNumDoc.Text.Trim();
            string lstrtelefono = txtCel.Text.Trim();
            CRegistroDAL lobjDAL = new CRegistroDAL(lstrConnectionString);

            DataTable ldt = lobjDAL.mListarRegistroSupermercado04(lstrTipoDocumento, lstrNumeroDocumento);
            if (ldt.Rows.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(
                this.Page,
                this.GetType(),
                "YaExiste",
                "showError();",
                true);
            }
            else
            {
                lintResult = lobjDAL.mGrabarRegistroSupermercado04(
                        lgIdRegistro,
                        lstrNombre,
                        lstrPaterno,
                        lstrMaterno,
                        lstrCorreoElctronico,
                        lstrTipoDocumento,
                        lstrNumeroDocumento,
                        lstrtelefono

                    );
                ScriptManager.RegisterClientScriptBlock(
                this.Page,
                this.GetType(),
                "ajaxBindings",
                "enviarForm();",
                true);
            }


        }
        catch (Exception lExcErr)
        {
            throw new Exception(lExcErr.Message);
        }
    }

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        mGrabarRegistro();
    }
}