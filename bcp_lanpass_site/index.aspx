<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html lang="es" class="no-js"> <![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>MULTIPLICA KMS. LATAM Pass x4</title>
	<meta name="title" content="">
	<meta name="description" content="">
	<meta name="keywords" content="">
	
	<!-- Opciones para compartir en Facebook -->
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:type" content="">
	<meta property="og:url" content="">
	<meta property="og:image" content="">
	<meta property="og:site_name" content="">
	
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico?v=1">
	<link rel="stylesheet" href="css/main.css?v=3">
    <link rel="stylesheet" href="css/jquery.fancybox.css?v=1" />
	
	<script>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35160300-11']);
		_gaq.push(['_trackPageview', '/campanacuadriplica-home']);
		
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<script src="js/vendor/modernizr-2.6.1.min.js"></script>
</head>
<body>
	
	<section id="container">
		<header></header>
		<section id="wrapper">
            <div class="logo-lanpass"><img src="images/logo-lanpass.png" alt="" /></div>

            <div class="box promoverano">
                <div class="titu">Campa�a Verano</div>
                <h1>Tu pr�ximo viaje,<br />4 veces m�s cerca.</h1>
                <p><strong>Del 01 de enero al 28 de febrero</strong> todas las compras que hagas en el <strong>Boulevard de Asia o en grifos de Lima,</strong> con tus <strong>Tarjetas de Cr�dito BCP LATAM Pass</strong>, multiplican tus <strong>KMS. LATAM Pass  x4.</strong></p>
                <a class="btnInscribete" href="javascript:void(0);"><img src="images/bo_inscribete.png" /></a>
                <div class="stamp"><img src="images/stamp.png" alt="" /></div>
            </div>

            <!--<div class="box promonavidad">
                <div class="titu">Campa�a Navidad</div>
                <h1>�l ve regalos,<br />t� ves tu pr�ximo viaje.</h1>
                <p><strong>Del 15 de noviembre al 31 de diciembre,</strong> todas las compras que hagas con tus <strong>Tarjetas BCP LANPASS</strong> en <strong>tiendas por departamento</strong> de Lima, Arequipa, Chiclayo, Trujillo o Piura, <strong>multiplican tus KMS. LANPASS X4</strong>.</p>
                <a class="btnInscribete" href="javascript:void(0);"><img src="images/bo_inscribete.png" /></a>
                <div class="stamp"><img src="images/stamp.png" alt="" /></div>
            </div>-->
		</section>
		<footer>
			<!--<h2>Campa�a Navidad</h2>
            <p>Vigencia de la promoci�n y plazo de inscripci�n: del 15/11/13 al 31/12/13. Aplican restricciones. Participan de la promoci�n s�lo clientes con Tarjeta BCP LANPASS inscritos a trav�s de www.viabcp.com, www.bcplanpass.com, Banca por Tel�fono al 311 9898 opci�n 9-9 o Banca Exclusiva al 311 9797 y Facebook del BCP. Aplica para las tiendas por departamentos tal y como aparecen en la afiliaci�n y clasificaci�n en el sistema de Visa. La promoci�n es v�lida para Lima, Arequipa, Chiclayo, Trujillo y Piura. Para Tarjetas de Cr�dito, el primer abono de KMS. LANPASS corresponder� a los consumos regulares y se realizar� 48 horas despu�s de la fecha de corte de facturaci�n de su tarjeta. Para las Tarjetas de D�bito el abono correspondiente a la acumulaci�n regular se realizar� los 10 del mes siguiente al del consumo. El segundo abono de Kms. LANPASS ser� acreditado en cada cuenta de socio LANPASS a m�s tardar el 28/02/14 y corresponder� a los KMS. LANPASS adicionales por la promoci�n, hasta por un monto m�ximo de 8,000 KMS. LANPASS. Stock m�nimo: 20,000 Kms. LANPASS. Para fines de abono la tarjeta del cliente debe encontrarse activa y sin bloqueo. No acumulan Kms. las disposiciones de efectivo, compras de deuda, efectivo preferente, consumos en casinos, pago de impuestos, consumos en servicios legales y abogados y pagos de servicios que se realizan por www.viabcp.com ni cargos por d�bito autom�tico. Para realizar operaciones en LAN utilizando tarjetas de cr�dito American Express debe dirigirse a cualquiera de las Oficinas de venta LAN y Contact Center LAN, las cuales estar�n sujetas a un cargo por servicio de $ 15.02 para destinos nacionales, $ 29.80 para destinos regionales y $ 40.00 para destinos a Norteam�rica, Europa y Australia. Tambi�n puede utilizar tarjetas de cr�dito American Express al elegir un medio de pago presencial que acepte tarjetas de cr�dito American Express para culminar sus operaciones realizadas en LAN.com, donde no aplicar� cargo por servicio. Las precios de los boletos a�reos, cargos por servicio de emisi�n (service fee), cupos, destinos y dem�s temas relacionados con el servicio de transporte de pasajeros en vuelos de LAN, as� como el canje de pasajes, dem�s servicios y la operatividad del programa LANPASS son de responsabilidad de LAN. La acumulaci�n, canje, uso y dem�s condiciones aplicables a los KMS. LANPASS se rigen bajo el reglamento de LANPASS publicado en www.latam.com/latampass/lanpass. Para m�s informaci�n sobre la promoci�n, restricciones, costos y tributos aplicables a las tarjetas del BCP llama a nuestra Banca por Tel�fono al 311-9898 o ingresa a viabcp.com.</p>
            <hr />-->
            <h2>Campa�a Verano</h2>
            <p>Vigencia de la promoci�n y plazo de inscripci�n: del 01/01/14 al 28/02/14. Aplican restricciones. Participan de la promoci�n s�lo clientes con Tarjeta de cr�dito BCP LATAM Pass inscritos a trav�s de www.viabcp.com, www.bcpLATAM Pass.com, Banca por Tel�fono al 311 9898 opci�n 9-9 o Banca Exclusiva al 311 9797 y Facebook del BCP.  Aplica para Grifos de Lima y establecimientos de Asia tal y como aparecen en la afiliaci�n y clasificaci�n en el sistema de Visa. La promoci�n es v�lida s�lo para Lima. Para Tarjetas de Cr�dito, el primer abono de KMS. LATAM Pass corresponder� a los consumos regulares y se realizar� 48 horas despu�s de la fecha de corte de facturaci�n de su tarjeta. Para las Tarjetas de Cr�dito el segundo abono de Kms. LATAM Pass ser� acreditado en cada cuenta de socio LATAM Pass a m�s tardar el 30/04/14 y corresponder� a los KMS. LATAM Pass adicionales por la promoci�n, hasta por un monto m�ximo de 8,000 KMS. LATAM Pass. Stock m�nimo: 20,000 Kms. LATAM Pass. Para fines de abono la tarjeta del cliente debe encontrarse activa y sin bloqueo. No acumulan Kms. las disposiciones de efectivo, compras de deuda, efectivo preferente, consumos en casinos, pago de impuestos, consumos en servicios legales y abogados y pagos de servicios que se realizan por www.viabcp.com ni cargos por d�bito autom�tico. Para realizar operaciones en LAN utilizando tarjetas de cr�dito American Express debe dirigirse a cualquiera de las Oficinas de venta LAN y Contact Center LAN, las cuales estar�n sujetas a un cargo por servicio de $ 15.02 para destinos nacionales, $ 29.80 para destinos regionales y $ 40.00 para destinos a Norteam�rica, Europa y Australia. Tambi�n puede utilizar tarjetas de cr�dito American Express al elegir un medio de pago presencial que acepte tarjetas de cr�dito American Express para culminar sus operaciones realizadas en LAN.com, donde no aplicar� cargo por servicio. Las precios de los boletos a�reos, cargos por servicio de emisi�n (service fee), cupos, destinos y dem�s temas relacionados con el servicio de transporte de pasajeros en vuelos de LAN, as� como el canje de pasajes, dem�s servicios y la operatividad del programa LATAM Pass son de responsabilidad de LAN. La acumulaci�n, canje, uso y dem�s condiciones aplicables a los KMS. LATAM Pass se rigen bajo el reglamento de LATAM Pass publicado en www.latam.com/es_pe/latam-pass/. Para m�s informaci�n sobre la promoci�n, restricciones, costos y tributos aplicables a las tarjetas del BCP llama a nuestra Banca por Tel�fono al 311-9898 o ingresa a viabcp.com.</p>
		</footer>
	</section>
	
	<script src="js/vendor/jquery-1.8.1.min.js"></script>
	<script src="js/vendor/cufon-yui.js"></script>
	<script src="js/vendor/Flexo.font.js"></script>
    <script src="js/vendor/jquery.fancybox.pack.js"></script>
	<script src="js/init.js?v=1"></script>
</body>
</html>
