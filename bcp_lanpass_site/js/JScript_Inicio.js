﻿var ie = (function () {

    var undef,
            v = 3,
            div = document.createElement('div'),
            all = div.getElementsByTagName('i');

    while (
            div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
            all[0]
        );

    return v > 4 ? v : undef;
} ());

function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

function offsetToRangeCharacterMove(el, offset) {
    return offset - (el.value.slice(0, offset).split("\r\n").length - 1);
}

function setInputSelection(el, startOffset, endOffset) {
    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        el.selectionStart = startOffset;
        el.selectionEnd = endOffset;
    } else {
        var range = el.createTextRange();
        var startCharMove = offsetToRangeCharacterMove(el, startOffset);
        range.collapse(true);
        if (startOffset == endOffset) {
            range.move("character", startCharMove);
        } else {
            range.moveEnd("character", offsetToRangeCharacterMove(el, endOffset));
            range.moveStart("character", startCharMove);
        }
        range.select();
    }
}

(function($) {
    $.fn.doctypeselector = function(){
        //$('#selectDocType li:not(":first")').addClass('unselected');
        $(document).on("click",'#selectDocType li:first',function(){
            $('#selectDocType .unselected').css('display','list-item');
            //$(this).removeClass('markinput');
        });
        $(".unselected").click(function(){
            var texto = $(this).text();
            $('#selectDocType li:first').html('<span style="float:left;">'+texto+'</span><div class="comboselect"></div>');
            $('#selectDocType .unselected').css('display','none');

            var dataid = $(this).attr('data-id');

            if(dataid==1){
                $("#inputDoc").attr({
                    'maxlength': '8',
                    'placeholder': 'Ingresa tu DNI',
                    'data-id':dataid
                });
                $("#inputDoc").val('');
            }else{
                $("#inputDoc").attr({
                    'maxlength': '12',
                    'placeholder': 'Ingresa tu CE',
                    'data-id':dataid
                });
                $("#inputDoc").val('');

            }
            $(function() { $.fn.placeholder();});
        });
        $(document).mouseup(function(event) {
            var container = $("#selectDocType li:first");
            if(!container.is(event.target) && container.has(event.target).length===0){
                $('#selectDocType .unselected').hide('fast');

            }
        });
    }
})(jQuery);

    (function($) {
        $.fn.placeholder = function() {
            if(typeof document.createElement("input").placeholder == 'undefined') {
                $('[placeholder]').focus(function() {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                      input.val('');
                      input.removeClass('placeholder');
                    }
                }).blur(function() {
                    var input = $(this);
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                      input.addClass('placeholder');
                      input.val(input.attr('placeholder'));
                    }
                }).blur().parents('form').submit(function() {
                    $(this).find('[placeholder]').each(function() {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                        input.val('');
                        }
                    })
                });
            }
        }
    })(jQuery); 

    (function($) {
        $.fn.fboard = function(){
            var linetochange = 1;
            var destinos = [['MIAMI', 'IQUITOS'], ['AREQUIPA', 'TARAPOTO'], ['CUZCO', 'TRUJILLO'], ['IQUITOS', 'SANTIAGO'], ['TARAPOTO', 'BOGOTA'], ['TRUJILLO', 'AREQUIPA']];
            var kms = [['48000', '12000'], ['10000', '10000'], ['10000', '10000'], ['12000', '22000'], ['10000', '24000'], ['10000', '10000']]; 
            
            function animateboard(str) {       
                if(str == 'replay'){
                    linetochange = 1;
                    $('.basicBoard2[data-id='+linetochange+']').flightboard('flip');
                    $('.basicBoard[data-id='+linetochange+']').flightboard('flip');
                }else{
                    $('.basicBoard2[data-id='+linetochange+']').flightboard('flip');
                    $('.basicBoard[data-id='+linetochange+']').flightboard('flip'); 
                }               
            }

            if(ie<9){
                destinos = [['MIAMI'], ['AREQUIPA'], ['CUZCO'], ['IQUITOS'], ['TARAPOTO'], ['TRUJILLO'],['IQUITOS'], ['TARAPOTO'], ['TRUJILLO'], ['SANTIAGO'], ['BOGOTA'], ['AREQUIPA']];
                kms = [['48000'], ['10000'], ['10000'], ['12000'], ['10000'], ['10000'], ['12000'], ['10000'], ['10000'], ['22000'], ['24000'], ['10000']]; 
                
                /*$('.arrow-next, .Pagflights').show();*/
                $('.Pagflights').show();
                $('.arrow-next').css("display", "block");
                
                for(var i=1;i<7;i++){
                    $('.basicBoard[data-id='+i+']').flightboard({
                        messages: destinos[i-1],
                        lettersImage: 'images/boardlettersonly.png',
                        maxLength: 9,
                        speed: parseInt(300),
                        pause: parseFloat(10000),
                        repeat: false,
                        lettersSeq: ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                    });
                    $('.basicBoard2[data-id='+i+']').flightboard({
                        messages: kms[i-1],
                        lettersImage: 'images/boardnumbersonly.png',
                        maxLength: 5,
                        speed: parseInt(300),
                        pause: parseFloat(10000),
                        repeat: false,
                        lettersSeq: ' 0123456789'
                    });
                }
                
            }else{
                for (var i = 1; i < 7; i++) {
                    $('.basicBoard[data-id=' + i + ']').flightboard({ messages: destinos[i - 1],
                        lettersImage: 'images/boardlettersonly.png',
                        maxLength: 9,
                        speed: parseFloat(300),
                        pause: parseFloat(10000),
                        repeat: false,
                        lettersSeq: ' ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                        afterFlip: function (current, next) {
                            linetochange++;
                            if (linetochange <= 6) animateboard();
                        }
                    });
                    $('.basicBoard2[data-id=' + i + ']').flightboard({ messages: kms[i - 1],
                        lettersImage: 'images/boardnumbersonly.png',
                        maxLength: 5,
                        speed: parseFloat(300),
                        pause: parseFloat(10000),
                        repeat: false,
                        lettersSeq: ' 0123456789'
                    });
                }
                
                animateboard();
                
                setInterval(function(){
                    animateboard('replay');
                }, 10000)

            }           
            
        }
    })(jQuery);

    function IniciaPaginador(){
        destinos = [['MIAMI'], ['AREQUIPA'], ['CUZCO'], ['IQUITOS'], ['TARAPOTO'], ['TRUJILLO'],['IQUITOS'], ['TARAPOTO'], ['TRUJILLO'], ['SANTIAGO'], ['BOGOTA'], ['AREQUIPA']];
        kms = [['48000'], ['10000'], ['10000'], ['12000'], ['10000'], ['10000'], ['12000'], ['10000'], ['10000'], ['22000'], ['24000'], ['10000']];
        $('.Mainflights, .vIE').hide();
        $('.basicBoard, .basicBoard2').flightboard('destroy');
    }

    $('.arrow-prev, .arrow-next').on('click', function(){
        itm = $(this).attr('class').replace('arrow-','');
        
        IniciaPaginador();

        $('.Pagflights a').removeClass('activo');

        if(itm == 'prev'){
            for(var i=1;i<7;i++){
                $('.basicBoard[data-id='+i+']').flightboard({
                    messages: destinos[i-1],
                    lettersImage: 'images/boardlettersonly.png',
                    maxLength: 9,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                });
                $('.basicBoard2[data-id='+i+']').flightboard({
                    messages: kms[i-1],
                    lettersImage: 'images/boardnumbersonly.png',
                    maxLength: 5,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' 0123456789'
                });
            }
            $('.Mainflights').show();
            $('.arrow-prev, .arrow-next').hide();
            /*$('.arrow-next').show();*/
            $('.arrow-next').css("display", "block");
            $('.Pagflights a[rel="01"]').addClass('activo');
        }
        if(itm == 'next'){
            for(var i=7;i<13;i++){
                $('.basicBoard[data-id='+i+']').flightboard({
                    messages: destinos[i-1],
                    lettersImage: 'images/boardlettersonly.png',
                    maxLength: 9,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                });
                $('.basicBoard2[data-id='+i+']').flightboard({
                    messages: kms[i-1],
                    lettersImage: 'images/boardnumbersonly.png',
                    maxLength: 5,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' 0123456789'
                });
            }
            $('.vIE').show();
            $('.arrow-prev, .arrow-next').hide();
            /*$('.arrow-prev').show()*/
            $('.arrow-prev').css("display", "block");
            $('.Pagflights a[rel="02"]').addClass('activo');
        }
    })

    $('.Pagflights a').on('click', function(){
        var itm = $(this).attr('rel');
        IniciaPaginador();
        
        if(itm == 01){
            for(var i=1;i<7;i++){
                $('.basicBoard[data-id='+i+']').flightboard({
                    messages: destinos[i-1],
                    lettersImage: 'images/boardlettersonly.png',
                    maxLength: 9,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                });
                $('.basicBoard2[data-id='+i+']').flightboard({
                    messages: kms[i-1],
                    lettersImage: 'images/boardnumbersonly.png',
                    maxLength: 5,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' 0123456789'
                });
            }
            $('.Mainflights').show();
            $('.arrow-prev, .arrow-next').hide();
            /*$('.arrow-next').show();*/
            $('.arrow-next').css("display", "block");
        }
        if(itm == 02){
            for(var i=7;i<13;i++){
                $('.basicBoard[data-id='+i+']').flightboard({
                    messages: destinos[i-1],
                    lettersImage: 'images/boardlettersonly.png',
                    maxLength: 9,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                });
                $('.basicBoard2[data-id='+i+']').flightboard({
                    messages: kms[i-1],
                    lettersImage: 'images/boardnumbersonly.png',
                    maxLength: 5,
                    speed: parseInt(300),
                    pause: parseFloat(10000),
                    repeat: false,
                    lettersSeq: ' 0123456789'
                });
            }
            $('.vIE').show();
            $('.arrow-prev, .arrow-next').hide();
            /*$('.arrow-prev').show();*/
            $('.arrow-prev').css("display", "block");
        }
        $('.Pagflights a').removeClass('activo');
        $(this).addClass('activo');

    });

    (function($) {
        $.fn.inputdni = function(){
            $('#inputDoc').keydown(function(event) {
                //$('#inputDoc').removeClass("markinput");.
                if((event.keyCode!=9)) $('.inputDocerror').css({'visibility':'hidden'});
                // Allow: backspace, delete, tab, escape, enter and .
                
                var dataid = $(this).attr('data-id');

                if ( (event.keyCode==8) || (event.keyCode==9) ||
                     // Allow: Ctrl+C
                    (event.keyCode == 67 && event.ctrlKey === true) 
                    ) {
                         return;
                } else if(event.keyCode == 13){
                    var input = $('#inputDoc');
                    var dataid = input.attr('data-id');

                    var Numdoc = input.val();//variable almacena el numero del documento
                    var TipoDoc = dataid;//variable almacena el tipo del documento 1->DNI,2->Carné

                    if(dataid==1){
                        var maxl = 8;
                        var minl = 8;
                    }else{
                        var maxl = 12;
                        var minl = 8;
                    }
                    var inputlength = input.val().length;
                    if(inputlength >= minl && inputlength <= maxl ){
                        Consultar_Ofertas(Numdoc,TipoDoc);
                        if(ie < 9) $(".modal2").show();
                       /* 
                        if(input.val() == "00000000"){
                         $(".modal[data-id=3]").show();
                         _gaq.push(['_trackPageview', '/WebsiteOficial/popup_oferta3']);
                        }
                        else $(".modal[data-id=5]").show();*/

                        $('html, body').animate({scrollTop: 100}, "fast");
                        input.blur();           
                        $('.inputDocerror').css({'visibility':'hidden'});
                    }else{
                        $('.inputDocerror').css({'visibility':'visible'});
                        if(dataid==1){
                            if(inputlength == 0){
                                $('.inputDocerror').text('(*) Recuerda ingresar tu número de DNI.');
                            }
                            else{
                                $('.inputDocerror').text('(*) Tu DNI debe tener 8 números.');
                            }
                        }
                        else{
                            if(inputlength == 0){
                                $('.inputDocerror').text('(*) Recuerda ingresar tu número de CE.');
                            }else{
                                $('.inputDocerror').text('(*) Tu CE debe tener mínimo 8 dígitos.');
                            }
                        }
                        //$('#inputDoc').addClass("markinput");
                    }
                }
                else {
                    if(dataid==2){
                        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) && (event.keyCode < 65 || event.keyCode > 90 ) && (event.keyCode < 97 || event.keyCode > 122 ) ) {
                            event.preventDefault(); 
                        }else if(event.shiftKey){
                            if(event.keyCode >= 65 || event.keyCode <= 90){
                                return;
                            }else{
                                event.preventDefault();
                            }
                        }
                    }else{
                        // Ensure that it is a number and stop the keypress
                        if(event.shiftKey){
                            event.preventDefault();     
                        }
                        else if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                            event.preventDefault(); 
                        }   
                    }
                }
            });
            $('#inputDoc').keyup(function() {
                var input = $('#inputDoc');
                var dataid = input.attr('data-id');
                var $th = $(this);
                var thdom = $th[0];
                var sel = getInputSelection(thdom);
                var value1 = $th.val();
                if(dataid==2){ 
                    var value2 = value1.replace(/[^a-zA-Z0-9]/g, function(str) {  return ''; } );               
                    if(value1 != value2) {$th.val(value2); sel.start--; sel.end--;}
                    setInputSelection(thdom, sel.start, sel.end);
                }else{
                    var value2 = value1.replace(/[^0-9]/g, function(str) {  return ''; });
                    if(value1 != value2) {$th.val(value2); sel.start--; sel.end--;}
                    setInputSelection(thdom, sel.start, sel.end);
                }
            });
        }
    })(jQuery);

    (function($) {
        $.fn.bannerfade = function(){
            $(".btnPagSlide").click(function(event){
                event.preventDefault();
                var element = $(this);
                var id = element.attr("data-id");       
                $(".btnPagSlide").removeClass("active");
                $('.bannerslide').removeClass("banneractivo");
                $(".bannerslide:visible").fadeOut(500,function(){
                        element.addClass("active");             
                        $(".bannerslide[data-id="+id+"]").delay(0).fadeIn(500).addClass("banneractivo");
                });     
            });
        }   
    })(jQuery);

    (function($) {
        $.fn.popups = function(){
            $(".ModalClose").click(function(event) {
                if(ie < 9) $(".modal2").hide();
                $(".modal").hide();
            });
            $(".btnbanner3").click(function(event){
                event.preventDefault();
                var input = $('#inputDoc');
                var dataid = input.attr('data-id');

                var Numdoc = input.val();//variable almacena el numero del documento
                var TipoDoc = dataid;//variable almacena el tipo del documento 1->DNI,2->Carné

                if(dataid==1){
                    var maxl = 8;
                    var minl = 8;
                }else{
                    var maxl = 12;
                    var minl = 8;
                }
                var inputlength = input.val().length;
                if(inputlength >= minl && inputlength <= maxl ){
                    Consultar_Ofertas(Numdoc,TipoDoc);
                    if(ie < 9) $(".modal2").show();
                    /*if(input.val() == "00000000"){
                     $(".modal[data-id=3]").show();
                     _gaq.push(['_trackPageview', '/WebsiteOficial/popup_oferta3']);
                    }
                    else $(".modal[data-id=5]").show();*/
                    $('html, body').animate({scrollTop: 100}, "fast");
                    input.blur();           
                    $('.inputDocerror').css({ 'visibility': 'hidden' });
                    _gaq.push(['_trackEvent', 'WebsiteOficial', 'home', 'btn_consultar-ofertas']);

                }else{
                    $('.inputDocerror').css({'visibility':'visible'});
                    if(dataid==1){
                        if(inputlength == 0){
                            $('.inputDocerror').text('(*) Recuerda ingresar tu número de DNI.');
                        }
                        else{
                            $('.inputDocerror').text('(*) Tu DNI debe tener 8 números.');
                        }
                    }
                    else{
                        if(inputlength == 0){
                            $('.inputDocerror').text('(*) Recuerda ingresar tu número de CE.');
                        }else{
                            $('.inputDocerror').text('(*) Tu CE debe tener mínimo 8 dígitos.');
                        }
                    }
                }
            });
        }
    })(jQuery);

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=230019327043691";
        fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));

    //var fbsharebutton = 0;
    var popped = false;
    $(document).on("ready", function () {
        $(function () { $.fn.placeholder(); $.fn.fboard(); $.fn.inputdni(); $.fn.bannerfade(); $.fn.popups(); $.fn.doctypeselector(); });

        if (ie <= 9) { }
        else {

            var History = window.History;
            History.pushState({ state: '0' }, '', '');
            var State = History.getState();

            /*$(document).on("click",".promo",function(event){                        
            event.preventDefault();
            dataid = $(this).attr('data-id');
            var promourl = $(this).attr('href');
            var content = $(".Content");                    
                    
            /*content.slideUp(2000, function(){
            $('.basicBoard').flightboard('destroy');
            $('.basicBoard2').flightboard('destroy');       
            content.load((promourl+"?"+new Date().getTime()) + ' .contentSlided', function(response, status, xhr){                  
            var title = $(response).filter("title").text();
            History.pushState({state:dataid},title,promourl);
            content.slideDown(2000);
            });
            });
            $('.basicBoard').flightboard('destroy');
            $('.basicBoard2').flightboard('destroy');       
            content.load((promourl+"?"+new Date().getTime()) + ' .contentSlided', function(response, status, xhr){                  
            var title = $(response).filter("title").text();
            History.pushState({state:dataid},title,promourl);
            //content.slideDown(2000);
            });
            });*/
            $(document).on("click", ".promobtn", function (event) {
                event.preventDefault();
                var promourl = $(this).attr('href');
                var content = $(".Content");
                /*content.slideUp(2000, function(){
                content.load((promourl+"?"+new Date().getTime()) + ' .contentSlided', function(response, status, xhr){
                var title = $(response).filter("title").text();
                History.pushState({state:'0'},title,promourl);
                content.slideDown(2000);
                Cufon.refresh();    
                });
                });*/

                content.load((promourl + "?" + new Date().getTime()) + ' .contentSlided', function (response, status, xhr) {
                    var title = $(response).filter("title").text();
                    History.pushState({ state: '0' }, title, promourl);
                    //content.slideDown(2000);
                    Cufon.refresh();
                });

            });
            History.Adapter.bind(window, 'statechange', function () {
                State = History.getState();
                $('.basicBoard').flightboard('destroy');
                $('.basicBoard2').flightboard('destroy');
                var url = State.url;
                var pageopened = State.data.state;

                $(".Content").load(url + "?" + new Date().getTime() + " .contentSlided", function () {
                    switch (pageopened) {
                        case '0':
                            _gaq.push(['_trackPageview', '/WebsiteOficial/home']);
                            Cufon.refresh();
                            $(function () { $.fn.placeholder(); $.fn.fboard(); $.fn.inputdni(); $.fn.bannerfade(); $.fn.popups(); $.fn.doctypeselector(); });
                            $('#NavHome').hide();
                            break;
                        case '1':
                            _gaq.push(['_trackPageview', '/WebsiteOficial/verano']);
                            $('#NavHome').show();
                            break;
                        case '2':
                            _gaq.push(['_trackPageview', '/WebsiteOficial/campana1']);
                            $('#NavHome').show();
                            break;
                        case '3':
                            _gaq.push(['_trackPageview', '/WebsiteOficial/campana1']);
                            $('#NavHome').show();
                            break;
                    }


                });
            });
        }
        /*$(document).on("click", ".fbbuttonclick", function (event) {
            event.preventDefault();
            _gaq.push(['_trackEvent', 'WebsiteOficial', 'Footer', 'Btn_Facebook']);
            urlcomp = encodeURIComponent('http://www.bcplanpass.com/');
            window.open("https://www.facebook.com/sharer/sharer.php?u=" + urlcomp, '', 'left=20,top=20,width=500,height=550,toolbar=1,resizable=0');

        });*/
    });

function Consultar_Ofertas(Numdoc,TipoDoc)
{
    PageMethods.Consultar_Ofertas(Numdoc,(TipoDoc==1?"DNI":"CE"),Consultar_Ofertas_PostBack);
}
String.prototype.capitalize = function() {
    return this.toLowerCase().replace(/(^|\s)([a-z])/g, function(m, p1, p2) { return p1 + p2.toUpperCase(); });
};
function Consultar_Ofertas_PostBack(r)
{
    if(r==null)
    {
        $(".modal[data-id=5]").show();
        //_gaq.push(['_trackPageview', '/WebProducto-popup_oferta5']);
    }
    else{
        $(".message[message-id=1]").removeClass("messagebig");
        if((r[2] + r[4] + r[3]).length > 350) $(".message[message-id=1]").addClass("messagebig");

        $(".modal[data-id=1]").show();
        _gaq.push(['_trackPageview', '/WebProducto-popup_oferta']);
        _gaq.push(['_trackPageview', '/WebsiteOficial/popup_oferta' + r[0].replace(/C00/g,"")]);

        $("#data-nombre-1").text(r[1].capitalize());
        $("#data-titulo-1").text(r[2]);
        $("#data-descrip-1").text(r[3]);
        $("#data-subdescrip-1").html(r[4].replace(/\n/g,"<br/>"));       
    }
    $('.inputDocerror').css({ 'visibility': 'hidden' });
}