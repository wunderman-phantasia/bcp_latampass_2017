
var android, iphone, ipad, uagent, vwidth;
uagent = navigator.userAgent.toLowerCase();
android = uagent.search(/android/);
iphone = uagent.search(/iphone/);
ipad = uagent.search(/ipad/);
vwidth = $(document).width();

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=171557349613774";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(document).on('ready', function () {
    _gaq.push(['_trackPageview', '/WebsiteOficial/campana-usala-todos-los-dias']);

    $(".fbbuttonclick").on("click", function (event) {
        event.preventDefault();
        _gaq.push(['_trackEvent', 'WebProducto', 'Footer', 'Btn_Facebook']);
        window.open("https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.bcplanpass.com%2Fusala-todos-los-dias.aspx", '', 'left=20,top=20,width=500,height=550,toolbar=1,resizable=0');
    });
    $(".active").removeClass("active");
    $('.btn-suscribete').on('click', function () {
        $('body').removeClass('promolanpass');
        $.colorbox({
            fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
            innerHeight: true,
            innerWidth: true,
            width: 534,
            height: 412,
            onOpen: function () {
                $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=0.99, maximum-scale=0.99');
                $('#cboxClose').hide();
                if ($.browser.msie) {
                    if (parseInt($.browser.version) == 6) {
                        $('#cboxOverlay').css({
                            'position': 'absolute',
                            'width': $(window).width(),
                            'height': $(document).height()
                        });
                    }
                }
                if (android > 1) {
                    //Si es un dispositivo en android
                    $('#cboxOverlay').css({
                        'position': 'absolute',
                        'width': '285%',
                        'height': $(document).height(),
                        'overflow': 'hidden'

                    });
                    //var w_modal = $("#colorbox").width();
                    //var new_left = Math.ceil(w_modal / 2) * (-1);
                    nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                    $('#colorbox').css("left", nwidth + "px");
                }
                if (ipad > 1) {
                    //alert("Ipad");
                }
                if (iphone > 1) {
                    nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                    $('#colorbox').css("left", nwidth + "px");
                }

            },
            onCleanup: function () {
                $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
            },
            onClosed: function () { $('#cboxClose').hide(); },
            onLoad: function () { $('#cboxClose').hide(); },
            onComplete: function () {
                iframe();
                $('#cboxClose').show();
            },
            close: '',
            href: 'promos-demos/popFormPromSept2015.aspx'
        });
    });
});

function VentanRegisrarDatos() {
    $('body').removeClass('promolanpass');
    $.colorbox({
        fastIframe: true,
        iframe: true,
        preloading: true,
        scrolling: false,
        escKey: false,
        overlayClose: false,
        innerHeight: true,
        innerWidth: true,
        width: 484,
        height: 800,
        maxHeight: 1250,
        onOpen: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=0.99, maximum-scale=0.99');
            $('#cboxClose').hide();
            if ($.browser.msie) {
                if (parseInt($.browser.version) == 6) {
                    $('#cboxOverlay').css({
                        'position': 'absolute',
                        'width': $(window).width(),
                        'height': $(document).height()
                    });
                }
            }
            if (android > 1) {
                $('#cboxOverlay').css({
                    'position': 'absolute',
                    'width': '285%',
                    'height': $(document).height(),
                    'overflow': 'hidden'

                });
                nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                $('#colorbox').css("left", nwidth + "px");
            }
            if (ipad > 1) {
            }
            if (iphone > 1) {
                nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                $('#colorbox').css("left", nwidth + "px");
            }

        },
        onCleanup: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
        },
        onClosed: function () { $('#cboxClose').hide(); },
        onLoad: function () { $('#cboxClose').hide(); },
        onComplete: function () {
            iframe();
            $('#cboxClose').show();
        },
        close: '',
        href: 'promos-demos/popFormPromSept2015Register.aspx'
    });
}

function RegistroExistoso() {
    $('body').addClass('promolanpass');
    $.colorbox({
        fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
        width: 650, height: 450,
        onOpen: function () { $('#cboxClose').hide(); },
        onClosed: function () { $('#cboxClose').hide(); },
        onLoad: function () { $('#cboxClose').hide(); },
        onComplete: function () { iframe(); $('#cboxClose').show(); },
        close: '',
        href: 'registro-correcto.aspx',
        onCleanup: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
        }
    });
}

function CerrarVentana() {
    _gaq.push(['_trackEvent', 'WebsiteOficial', 'campana-usala-todos-los-dias', 'btn_enviar-datos']);
    $('body').addClass('promolanpass');
    $.colorbox({
        fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
        width: 600, height: 385,
        onOpen: function () { $('#cboxClose').hide(); },
        onClosed: function () { $('#cboxClose').hide(); },
        onLoad: function () { $('#cboxClose').hide(); },
        onComplete: function () {
            iframe(); $('#cboxClose').show();
            _gaq.push(['_trackPageview', '/WebsiteOficial/campana-usala-todos-los-dias/formulario-registrosatisfactorio']);
        },
        close: '',
        href: '/promos-demos/popFormPromSept2015Register.aspx',
        onCleanup: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
        }
    });
}

function RegistroDuplicado() {
    _gaq.push(['_trackEvent', 'WebsiteOficial', 'campana-usala-todos-los-dias', 'btn_enviar-datos']);
    $('body').addClass('promolanpass');
    $.colorbox({
        fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
        width:580, height: 280,
        onOpen: function () { $('#cboxClose').hide(); },
        onClosed: function () { $('#cboxClose').hide(); },
        onLoad: function () { $('#cboxClose').hide(); },
        onComplete: function () { iframe(); $('#cboxClose').show(); },
        close: '',
        href: 'registro-duplicado.aspx',
        onCleanup: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
        }
    });
}

function NoHayCupos() {
    $('body').addClass('promolanpass');
    $.colorbox({
        fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
        width: 473, height: 250,
        onOpen: function () { $('#cboxClose').hide(); },
        onClosed: function () { $('#cboxClose').hide(); },
        onLoad: function () { $('#cboxClose').hide(); },
        onComplete: function () { iframe(); $('#cboxClose').show(); },
        close: '',
        href: 'no-hay-cupos.aspx',
        onCleanup: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
        }
    });
}

function NoHayTarjeta() {
    $('body').addClass('promolanpass');
    $.colorbox({
        fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
        width: 580, height: 280,
        onOpen: function () { $('#cboxClose').hide(); },
        onClosed: function () { $('#cboxClose').hide(); },
        onLoad: function () { $('#cboxClose').hide(); },
        onComplete: function () { iframe(); $('#cboxClose').show(); },
        close: '',
        href: 'no-hay-tarjeta.aspx',
        onCleanup: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
        }
    });
}

function NavegadorSoportado() {


    var navegador = get_browser_info();

    if ((navegador.name.toLowerCase() == 'msie' || navegador.name.toLowerCase() == 'ie') && navegador.version < 8)
        return NavegdorNosoportado();

    if (navegador.name.toLowerCase() == 'chrome' && navegador.version < 34)
        return NavegdorNosoportado();

    if (navegador.name.toLowerCase() == 'safari' && navegador.version < 5)
        return NavegdorNosoportado();

    if (navegador.name.toLowerCase() == 'firefox' && navegador.version < 28)
        return NavegdorNosoportado();

    return true;
}

function NavegdorNosoportado() {
    $('body').addClass('promolanpass');
    $.colorbox({
        fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
        width: 530, height: 290,
        onOpen: function () { $('#cboxClose').hide(); },
        onClosed: function () { $('#cboxClose').hide(); },
        onLoad: function () { $('#cboxClose').hide(); },
        onComplete: function () { iframe(); $('#cboxClose').show(); },
        close: '',
        href: 'actualiza-navegador.aspx',
        onCleanup: function () {
            $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
        }
    });
}

function iframe() {
    $('iframe').load(function () { $(this).fadeIn('fast'); });
}

$(document).ready(function () {
    $(window).resize(function () {
    });
});

function get_browser_info() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return { name: 'IE', version: (tem[1] || '') };
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) { return { name: 'Opera', version: tem[1] }; }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return {
        name: M[0],
        version: M[1]
    };
}

function closeMessage() {
    $.colorbox.close();
}