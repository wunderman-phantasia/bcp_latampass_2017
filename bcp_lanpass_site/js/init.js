/**
*
* @fileoverview Libreria con funciones de utilidad
* @author Alex Villanueva Mercado
* @date Fecha_inicio
* @version 1.0
*/

$(document).ready(function () {

    Cufon.replace('#wrapper h1, .bgForm h1', {
        fontFamily: 'Flexo-Italic'
    });
    Cufon.replace('.box p, .titu', {
        fontFamily: 'Formata-Light-Italic'
    });
    Cufon.replace('.box p strong, footer h2', {
        fontFamily: 'Formata-Medium-Italic'
    });

    $(".promo-septiembre-2015 .btn-suscribete").click(function () {
        modal("popForm.aspx", 476, 595);
        _gaq.push(['_trackEvent', 'campanacuadriplica', 'home', 'btn-suscribete']);
    });

    $(".promonavidad .btnInscribete").click(function () {
        modal("popForm.aspx", 476, 595);
        _gaq.push(['_trackEvent', 'campanacuadriplica', 'home', 'btn_inscribete_navidad']);
    });

    $(".promoverano .btnInscribete").click(function () {
        modal("popFormProm.aspx", 476, 595);
        _gaq.push(['_trackEvent', 'campanacuadriplica', 'home', 'btn_inscribete']);
    });

    function modal(_URL, w, h) {
        itm = _URL.replace('.aspx', '');
        if (itm == 'popFormProm') btn = $('#fancybox-close').addClass('btnNaranja');
        else btn = '';
        
        $.fancybox({
            type: 'iframe',
            href: _URL,

            padding: 0,
            margin: 0,
            scrolling: 'no', // 'auto', 'yes' or 'no'

            width: w,
            height: h,

            autoScale: false,
            autoDimensions: true,
            centerOnScroll: true,

            hideOnOverlayClick: false,
            hideOnContentClick: false,

            overlayShow: true,
            overlayOpacity: 0.7,
            overlayColor: '#000',

            transitionIn: 'elastic', // 'elastic', 'fade' or 'none'
            transitionOut: 'fade', // 'elastic', 'fade' or 'none'

            speedIn: 300,
            speedOut: 300,

            changeSpeed: 300,
            changeFade: 'fast',

            easingIn: 'swing',
            easingOut: 'swing',

            showCloseButton: true,
            showNavArrows: true,
            enableEscapeButton: false,
            enableKeyboardNav: false,

            onStart: function () { btn },
            onClosed: function () { $('#fancybox-close').removeClass('btnNaranja'); },
            onComplete: function () { iframe(); }
        });
    }

    $('#btnCerrar').on('click', function () {
        parent.jQuery.fancybox.close();
    })

    $('#btnInformate').click(function () {
        $.fancybox({
            type: 'iframe',
            href: 'informate.html',
            padding: 0,
            margin: 0,
            scrolling: 'no', // 'auto', 'yes' or 'no'
            width: 580,
            height: 270,
            autoScale: false,
            autoDimensions: true,
            centerOnScroll: true,
            hideOnOverlayClick: false,
            hideOnContentClick: false,
            overlayShow: true,
            overlayOpacity: 0.7,
            overlayColor: '#000',
            //transitionIn: 'elastic', // 'elastic', 'fade' or 'none'
            //transitionOut: 'fade', // 'elastic', 'fade' or 'none'
            speedIn: 300,
            speedOut: 300,
            changeSpeed: 300,
            //changeFade: 'fast',
            //easingIn: 'swing',
            //easingOut: 'swing',
            showCloseButton: false,
            showNavArrows: false,
            enableEscapeButton: true,
            enableKeyboardNav: false,
            onComplete: function () { iframe(); }
        });
    });


});

function iframe() {
    $('iframe').load(function () {
        $(this).fadeIn('fast');
    });
}