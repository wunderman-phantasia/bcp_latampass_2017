﻿function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

function offsetToRangeCharacterMove(el, offset) {
    return offset - (el.value.slice(0, offset).split("\r\n").length - 1);
}

function setInputSelection(el, startOffset, endOffset) {
    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        el.selectionStart = startOffset;
        el.selectionEnd = endOffset;
    } else {
        var range = el.createTextRange();
        var startCharMove = offsetToRangeCharacterMove(el, startOffset);
        range.collapse(true);
        if (startOffset == endOffset) {
            range.move("character", startCharMove);
        } else {
            range.moveEnd("character", offsetToRangeCharacterMove(el, endOffset));
            range.moveStart("character", startCharMove);
        }
        range.select();
    }
}

(function ($) {
    $.fn.doctypeselector2 = function () {
        $("input[name=doctype]").change(function () {
            var dataid = $(this).attr('data-id');
            if (dataid == 161) {
                $(".forminputtext[data-id=5]").val('');
                $(".forminputtext[data-id=5]").attr('name', 'dni');
                $(".forminputtext[data-id=5]").attr('maxlength', '8');
                $("span[data-id=5]").attr('title', 'Recuerda ingresar tu número de DNI. Debe tener 8 dígitos. Aún no lo has hecho o no has ingresado la cantidad correcta de números.');
            } else {
                $(".forminputtext[data-id=5]").val('');
                $(".forminputtext[data-id=5]").attr('name', 'ce');
                $(".forminputtext[data-id=5]").attr('maxlength', '12');
                $("span[data-id=5]").attr('title', 'Recuerda ingresar tu número de Carné de extranjería. Debe tener entre 8 y 12 dígitos.');
            }
            $(".inputerrorimage[data-id=dni]").css({ 'display': 'none' });
            $(".inputcheckimage[data-id=dni]").css({ 'display': 'none' });
            $(".forminputtext[data-id=5]").css({ 'border-color': '' });
            $(".error[for=dni]").css({ 'display': 'none' });
        });
    }
})(jQuery);

$(document).on("ready", function () {

    $(function () { $.fn.doctypeselector2(); });

    $('input').keydown(function (event) {
        //alert($(this).prop('data-id'));
        var id = $(this).attr('data-id');
        var ttip = $(".tooltip[data-id=" + id + "]");
        ttip.css('visibility', 'hidden');
    }).change(function (event) {
        //alert($(this).prop('data-id'));
        var id = $(this).attr('data-id');
        var ttip = $(".tooltip[data-id=" + id + "]");
        ttip.css('visibility', 'hidden');
    });

    $('input[name=phone],input[name=dni],input[name=income],input[name=ce]').keydown(function (event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 67 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if ($(this).attr('name') == 'ce') {
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && (event.keyCode < 65 || event.keyCode > 90) && (event.keyCode < 97 || event.keyCode > 122)) {
                    event.preventDefault();
                } else if (event.shiftKey) {
                    if (event.keyCode >= 65 || event.keyCode <= 90) {
                        return;
                    } else {
                        event.preventDefault();
                    }
                }
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        }
    });
    $('input[name=phone],input[name=dni],input[name=income],input[name=ce]').keyup(function () {
        var $th = $(this);
        var thdom = $th[0];
        var sel = getInputSelection(thdom);
        var value1 = $th.val();
        if ($(this).attr('name') == 'ce') {
            var value2 = value1.replace(/[^a-zA-Z0-9]/g, function (str) { return ''; });
        } else {
            var value2 = value1.replace(/[^0-9]/g, function (str) { return ''; });
        }
        if (value1 != value2) { $th.val(value2); sel.start--; sel.end--; }
        setInputSelection(thdom, sel.start, sel.end);
    });
    $('input[name=firstname], input[name=lastnamefather], input[name=lastnamemother]').keyup(function () {
        var $th = $(this);
        var thdom = $th[0];
        var sel = getInputSelection(thdom);
        var value1 = $th.val();
        var value2 = value1.replace(/[^a-zA-Z\ \áéíóúÁÉÍÓÚñÑäëïöüÄËÏÖÜ]/g, function (str) { return ''; });
        if (value1 != value2) { $th.val(value2); sel.start--; sel.end--; }
        setInputSelection(thdom, sel.start, sel.end);
    });
    $('input[name=email]').keyup(function () {
        var $th = $(this);
        var thdom = $th[0];
        var sel = getInputSelection(thdom);
        var value1 = $th.val();
        var value2 = value1.replace(/[^a-zA-Z0-9\ \-._@]/g, function (str) { return ''; });
        if (value1 != value2) { $th.val(value2); sel.start--; sel.end--; }
        setInputSelection(thdom, sel.start, sel.end);
    });

    jQuery.validator.setDefaults({
        debug: false
    });

    var spantt = $(".regular11redcursor");
    spantt.on("mouseup", function () {
        var id = $(this).attr('data-id');
        var ttip = $(".tooltip[data-id=" + id + "]");
        if (ttip.css('visibility') == 'hidden') ttip.css('visibility', 'visible');
        else ttip.css('visibility', 'hidden');
    });

    var minAmount = 0;
    function CalcularMinSueldo() {
        var TipoCambio = 2.7;
        var income = $("input[name=income]").val();
        var incomecurrency = $("input[name=incomecurrency]:checked").val();
        var card = $("input[name=pickcard]:checked").val();

        var amount = income;

        if ($("input[name=incomecurrency]").is(":checked")) {
            if (incomecurrency == 'dolares')
                amount = TipoCambio * income;
        }

        switch (card) {
            case 'visaclasic':
                minAmount = 1600;
                break;
            case 'amexclasic':
                minAmount = 1800;
                break;
            case 'visagold':
            case 'amexgold':
            case 'masterplatinum':
                minAmount = 2500;
                break;
            case 'visaplatinum':
            case 'amexplatinum':
            case 'masterblack':
                minAmount = 6500;
                break;
            case 'visasignature':
                minAmount = 12000;
                break;
            default:
                minAmount = 1600;
                break;
        }

        return (amount < minAmount ? minAmount : 0);
    }

    $.validator.addMethod('letters', function (value) {
        return value.match(/^[a-zA-ZáéóúÁÉÍÓÚñÑü_\s]+$/);
    });


    $("#frmSolicitud").validate({
        showErrors: function (map, list) {
            this.successList = $.grep(this.successList, function (element) {
                var elementname = element.name;
                var errorname2 = elementname;

                if ($("input[name=" + elementname + "]").is(":focus")) { }
                else {
                    if (elementname == 'ce') errorname2 = 'dni';
                    $(".error[for=" + errorname2 + "]").css({ 'display': 'none' });
                    $(".forminputtext[name=" + elementname + "]").css({ 'border-color': '#03b307' });
                    $(".inputerrorimage[data-id=" + errorname2 + "]").css({ 'display': 'none' });
                    $(".inputcheckimage[data-id=" + errorname2 + "]").css({ 'display': 'inline-block' });
                }
                return !(element.name in list);
            });
            $.each(list, function (index, error) {
                var elementname = error.element.name;
                var errorname2 = elementname;


                if ($("input[name=" + elementname + "]").is(":focus")) { }
                else {
                    if (elementname == 'ce') errorname2 = 'dni';
                    $(".error[for=" + errorname2 + "]").text(error.message).css({ 'display': 'inline-block' });
                    $(".inputerrorimage[data-id=" + errorname2 + "]").css({ 'display': 'inline-block' });
                    $(".inputcheckimage[data-id=" + errorname2 + "]").css({ 'display': 'none' });
                    $(".forminputtext[name=" + elementname + "]").css({ 'border-color': '#dd0a0a' });
                }
                //return false;
            });
            if (list.length == 0)
                this.defaultShowErrors();
        },
        rules: {
            pickcard: {
                required: true
            },
            firstname: {
                required: true,
                minlength: 2,
                maxlength: 30,
                letters: true
            },
            lastnamefather: {
                required: true,
                minlength: 2,
                maxlength: 30,
                letters: true
            },
            lastnamemother: {
                required: true,
                minlength: 2,
                maxlength: 30,
                letters: true
            },
            phone: {
                required: true,
                minlength: 6,
                maxlength: 9,
                digits: true
            },
            doctype: {
                required: true
            },
            dni: {
                required: true,
                minlength: 8,
                maxlength: 8,
                digits: true
            },
            ce: {
                required: true,
                minlength: 8,
                maxlength: 12
            },
            email: {
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            hasaccount: {
                required: true
            },
            income: {
                required: true,
                digits: true,
                min: function () {
                    return CalcularMinSueldo();
                }
            },
            incomecurrency: {
                required: true
            }
        },
        messages: {
            firstname: {
                required: "Recuerda ingresar tu nombre.",
                letters: "Recuerda ingresar tu nombre.",
                minlength: "Recuerda ingresar tu nombre.",
                maxlength: "Recuerda ingresar tu nombre."
            },
            lastnamefather: {
                required: "Recuerda ingresar tu apellido paterno.",
                letters: "Recuerda ingresar tu apellido paterno.",
                minlength: "Recuerda ingresar tu apellido paterno.",
                maxlength: "Recuerda ingresar tu apellido paterno."
            },
            lastnamemother: {
                required: "Recuerda ingresar tu apellido materno.",
                letters: "Recuerda ingresar tu apellido materno.",
                minlength: "Recuerda ingresar tu apellido materno.",
                maxlength: "Recuerda ingresar tu apellido materno."
            },
            phone: {
                required: "Recuerda ingresar tu teléfono.",
                digits: "Tu teléfono debe tener mínimo 6 números.",
                minlength: "Tu teléfono debe tener mínimo 6 números.",
                maxlength: "Recuerda ingresar tu teléfono."
            },
            doctype: {
                required: "Recuerda seleccionar una respuesta."
            },
            dni: {
                required: "Recuerda ingresar tu DNI.",
                digits: "Tu DNI debe tener 8 números",
                minlength: "Tu DNI debe tener 8 números.",
                maxlength: "Tu DNI debe tener 8 números."
            },
            ce: {
                required: "Recuerda ingresar tu CE.",
                digits: "Tu CE debe tener entre 8 y 12 dígitos.",
                minlength: "Tu CE debe tener entre 8 y 12 dígitos.",
                maxlength: "Tu CE debe tener entre 8 y 12 dígitos."
            },
            email: {
                required: "Recuerda ingresar tu e-mail.",
                email: "Tu e-mail debe tener @ y una extensión válida (.pe, .com, .net, etc.).",
                minlength: "Tu e-mail debe tener @ y una extensión válida (.pe, .com, .net, etc.).",
                maxlength: "Recuerda ingresar tu e-mail."
            },
            hasaccount: {
                required: "Recuerda seleccionar una respuesta."
            },
            income: {
                required: "Recuerda indicar tu ingreso mensual.",
                digits: "Recuerda indicar tu ingreso mensual.",
                min: function () {
                    var tt = jQuery.validator.format("Debes tener un ingreso mínimo mensual de S/. {0} para solicitar esta tarjeta.");
                    return tt(minAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                }
            },
            incomecurrency: {
                required: "Recuerda seleccionar una moneda."
            },
            pickcard: {
                required: "Recuerda seleccionar la tarjeta que quieras solicitar."
            }
            //typecard: {
            //    required: "Recuerda seleccionar la tarjeta que quieras solicitar."
            //}
        }
    });

    $('input').focus(function (event) {
        var thisname = $(this).attr('name');
        if (thisname != 'pickcard') {
            //$(".error[for="+thisname+"]").css({'display':'none'});
            $(".forminputtext[name=" + thisname + "]").css({ 'border-color': '#1fc2ff' });
            //$(".inputerrorimage[data-id="+thisname+"]").css({'display':'none'});
            $(".inputcheckimage[data-id=" + thisname + "]").css({ 'display': 'none' });
        }
    });
});
