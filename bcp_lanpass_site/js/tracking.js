$(function(){
	$('#Inicio').on('click',function () {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Navegación',
		'action': 'Menú superior',
		'label': 'Inicio'
		});
		console.log('Tracking Inicio');
	});
	
	$('#Tarjetas').on('click',function () {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Navegación',
		'action': 'Menú superior',
		'label': 'Nuestras tarjetas'
		});
		console.log('Tracking Tarjetas');
	});
	
	$('#Quees').on('click',function () {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Navegación',
		'action': 'Menú superior',
		'label': 'Que es LATAM Pass'
		});
		console.log('Tracking LATAM Pass');
	});
	
	$('#Acumulo').on('click',function () {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Navegación',
		'action': 'Menú superior',
		'label': 'Como acumulo kms'
		});
		console.log('Tracking Acumular KMS');
	});
	
	$('#Canjeo').on('click',function () {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Navegación',
		'action': 'Menú superior',
		'label': 'Que puedo canjear'
		});
		console.log('Tracking Canjear');
	});
	
	$("#bannerSlider").on("click", function() {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Home',
		'action': 'Ver Banner superior',
		'label': 'http://www.latam.com/latampass/es_pe/sitio_personas/promociones/kms-que-vuelan-2016/vuelos-desde-lima'
		});
		console.log('Tracking Banner Slider')
	});
	
	$('.avion').css('cursor', 'pointer').on('click', function () {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Home',
		'action': 'Ver Banner central',
		'label': 'http://www.latam.com/latampass/es_pe/sitio_personas/lanpass/promociones/desde-ahora-conoce-el-peru-por-menos-kms/desde-lima/'
		});
		console.log('Tracking Banner Central')
  });
	
	$('.logolan').on('click',function() {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Home',
		'action': 'Ver LATAM Pass',
		'label': '' + $(this).attr("href")
		});
		console.log('Tracking Logo Lan')
	});
	
	/*$('.fbbuttonclick').on('click', function() {
		dataLayer.push({'event': 'socialEvent',
		'network': 'Facebook',
		'action': 'Compartir',
		'target': 'https://www.bcplanpass.com/'
		});
	  console.log('Tracking Compratir Facebook');
	});*/
	
	$('.btnbanner1').on('click', function() {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Home',
		'action': 'Pide tu tarjeta',
		'label': 'https://www.bcplanpass.com/formulario.aspx'
		});
	  console.log('Tracking Solicita tu Tarjeta');
	});
	
	$('#frmSolicitud').submit(function() {
		dataLayer.push({
		'event': 'virtualPage',
		'pageUrl': '/Solicitud-Enviada'
		});
		
		var card = "",
				pickcard = $("input[name='pickcard']:checked").val();
		
		if(pickcard == "visaclasic"){
			card = "Visa Clásica"
			
		}else if(pickcard == "visagold"){
			card = "Visa Oro"
		}else if(pickcard == "visaplatinum"){
			card = "Visa Platinum"
		}else if(pickcard == "visasignature"){
			card = "Visa Signature"
		}else if(pickcard == "amexclasic"){
			card = "American Express Clásica"
		}else if(pickcard == "amexgold"){
			card = "American Express Gold"
		}else if(pickcard == "amexplatinum"){
			card = "American Express Platinum"
		}
		
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Pide tu tarjeta',
		'action': 'Envía tus datos',
		'label': card
		});
		console.log('Tracking Solicitud');
	});
	
	$('.cards').on('click', function() { 
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Nuestras tarjetas',
		'action': 'Ver tarjeta',
		'label': $(this).attr('title')
		});
	  console.log('Tracking Tarjetas');
	});
	
	$('.btnbanner2').on('click', function () {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Tarjeta Crédito Visa',
		'action': 'Pide tu tarjeta',
		'label': 'https://www.bcplanpass.com/formulario.aspx'
		});
	  console.log('Tarjetacking Banner Pide tu Tarjeta');
	});
	
	$('.lightitalic14dark a').on('click', function() {
		dataLayer.push({'event': 'virtualEvent',
		'category': 'Qué puedo canjear',
		'action': 'Ver Catálogo',
		'label': 'http://www.lan.com/catalogolatampassperu/'
		});
	  console.log('Tracking Ver Catalogo');
	});
	
});