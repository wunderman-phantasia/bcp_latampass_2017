﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Banco de Crédito BCP</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="index, follow" />
<meta http-equiv="cache-control" content="no-cache" />
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<link href="../Styles/fonts.css" rel="Stylesheet" />
<style>
body{
	background:#ffffff !important;
}
.wrapper-error{
	margin:0 auto;
	width:580px;
	height:auto;
	margin-top:10%;
}
a.btn-volver-error{
	float:left;
	width:99px;
	height:45px;
	margin: 15px 0 0 0;
	background:url(/images/btn-volver-error.png) no-repeat 0 0;
}

a.btn-volver-error:hover, a.btn-volver-error:focus {
background: url("/images/btn-volver-error.png") no-repeat;
}
a.btn-volver-error {
background: url("/images/btn-volver-error.png") 0px bottom no-repeat;
height: 31px;
line-height: 31px;
text-align: center;
text-decoration: none;
width: 91px;
font-family: 'Flexo-Italic';
font-size: 14px;
color: #000068;
padding-left: 2px;
}



.bar {
    background:#004e8d url("/images/header-bar.jpg") left top repeat-y;
    height: 45px;
    margin: 13px auto;
    width: 100%;
}
	.bar img{
		float: right;
        height: 26px;
        margin: 10px 10px 0 0;
	}

.msg{display: block;}
.msg strong{font-weight: bold; font-family: "flexo-regular", Arial;font-size: 30px;color: #000068;}

</style>
</head>
<body>
<div class="bar"><img src="/images/logo_bcp.png" margin-right="0" width="96" height="26" alt="Banco de Crédito BCP" /></div>
<div class="wrapper-error"> 
	<div class="msg">
    <strong>Lo sentimos ócurrio un error con <br />
    la página solicitada</strong>
    </div>

	<a href="/" target="_parent" class="btn-volver-error">Volver</a>
</div>
</body>
</html>
