﻿<%@ WebHandler Language="C#" Class="SecureCaptcha" %>

using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Imaging;

public class SecureCaptcha : IHttpHandler, IReadOnlySessionState
{

    public void ProcessRequest(HttpContext context)
    {
        MemoryStream memStream = new MemoryStream();
        string phrase = Convert.ToString(context.Session["captcha"]);
        context.Session["captcha"] = BitConverter.ToString(new System.Security.Cryptography.SHA512Managed().ComputeHash(new System.Text.ASCIIEncoding().GetBytes(phrase))).Replace("-", "");
        //Generate an image from the text stored in session
        Bitmap imgCapthca = GenerateImage(230, 70, phrase);
        imgCapthca.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        byte[] imgBytes = memStream.GetBuffer();

        imgCapthca.Dispose();
        memStream.Close();

        //Write the image as response, so it can be displayed
        context.Response.ContentType = "image/jpeg";
        context.Response.BinaryWrite(imgBytes);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public Bitmap GenerateImage(int Width, int Height, string Phrase)
    {
        Bitmap CaptchaImg = new Bitmap(Width, Height);
        Random Randomizer = new Random();
        Graphics Graphic = Graphics.FromImage(CaptchaImg);
        Graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        Graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        //Set height and width of captcha image
        Graphic.FillRectangle(new SolidBrush(Color.White), 0, 0, Width, Height);
        //Rotate text a little bit
        Graphic.RotateTransform(Randomizer.Next(-5, 5));
        Graphic.ScaleTransform(1.3F, 1);
        int floatx = 15;
        string[] fonts = new string[] { "Comic Sans MS", "Verdana", "Courier New", "sans-serif", "Tahoma", "Times New Roman", "Lucida Console", "Arial Black", "Comic Sans MS", "cursive", "Helvetica" };
        foreach (char letra in Phrase.ToCharArray())
	    {         
            Graphic.ScaleTransform(1 + (Randomizer.Next(1, 5)) / 10, 1 + (Randomizer.Next(1, 5)) / 10);
            Graphic.DrawString(letra.ToString(), new Font(fonts[Randomizer.Next(0, fonts.Length)], Randomizer.Next(25, 30)),
                new SolidBrush(Color.DarkBlue), floatx += Randomizer.Next(15, 19), Randomizer.Next(15, 25));   
	    }
        /* generate random noise in background */
        Pen p; Point point1, point2, point3;
        for (int i = 0; i < 8; i++)
        {
            point1 = new Point(Randomizer.Next(0, Width), Randomizer.Next(0, Height));
            point2 = new Point(Randomizer.Next(0, Width), Randomizer.Next(0, Height));
            point3 = new Point(Randomizer.Next(0, Width), Randomizer.Next(0, Height));
            Point[] points = { point1, point2, point3 };
            p = new Pen(Brushes.Gray, Randomizer.Next(1, 3));
            Graphic.DrawCurve(p, points);
        }
        for (int i = 0; i < (Width * Height) / 50; i++)
        {
            Graphic.FillEllipse(Brushes.Gray, Randomizer.Next(0, Width), Randomizer.Next(0, Height), 2, 2);
        }
        Graphic.Flush();
        return CaptchaImg;
    }
}