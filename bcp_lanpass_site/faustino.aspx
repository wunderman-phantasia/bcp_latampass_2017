﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="faustino.aspx.cs" Inherits="faustino" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

	<script type="text/javascript">
	    //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
	    Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.pull-left', { fontFamily: 'Flexo-Light' });
	</script>	

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

	<section class="ContentFaustino">
		<a class="faustinobtn" href="Default.aspx" title="Regresar">Regresar</a>
		<iframe src="faustino/index.aspx" scrolling="no" frameborder="0" style="width:982px; height: 810px;"></iframe>	
	</section>
	
	<footer class="footer">
		<div class="pull-left" style="width:100px; margin-top:25px;">
			Compartir en: 
		</div>
		<div class="pull-left" style="width:50px; margin-top:20px;">
			<a class="fbbuttonclick" href="javascript:void(0)"><img src="images/facebook.png" width="22" height="22" alt=""></a>
		</div>
		<div class="pull-left" style=" margin-top:20px;">
			<div class="fb-like" data-href="http://www.bcplatampass.com/" data-width="450" data-layout="button_count" data-show-faces="true" data-send="false"></div>
		</div>
	</footer>

<div id="fb-root"></div>
<script>    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=171557349613774";
        fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript">
	_gaq.push(['_trackPageview', '/WebsiteOficial/faustino']);
	$(document).on("click",".fbbuttonclick",function(event){
		event.preventDefault();
		_gaq.push(['_trackEvent', 'WebProducto','Footer','Btn_Facebook']);
		window.open("https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.bcplatampass.com", '', 'left=20,top=20,width=500,height=550,toolbar=1,resizable=0');
	});
</script>

</asp:Content>

