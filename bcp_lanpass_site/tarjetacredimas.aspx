﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="tarjetacredimas.aspx.cs" Inherits="tarjetacredimas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

	<script type="text/javascript">
	    //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
	    Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.italic30orange', { fontFamily: 'Flexo-Italic' });
	    Cufon.replace('.mediumitalic30orange', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.visa figure h1', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.lista section header h1', { fontFamily: 'Flexo-Bold' });
	</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

	<section class="Content contentb">
		<header style="height:34px;">
			<h1 tabindex="7" title="Tarjetas de Débito Credimás Visa: Disponemos de 01 tipos: Credimás LATAM Pass.">
				<span class="italic30orange">Nuestras Tarjetas</span><span class="mediumitalic30orange"> - Débito Visa</span>
			</h1>
      <div class="div_regresa"><a class="promobtn" href="masinformacion.aspx" tabindex="8" title="Regresar">Regresar</a></div>
		</header>
		<div class="tarjetas" style="height: 290px;">
				<section class="visa">
					<figure style="width:305px;">
            <h1>Tarjeta de Débito</h1>
						<h1>Clásica LATAM Pass</h1>
						<img src="images/credimas/visadebit_credimascard.png" width="146" height="91" alt="">
						<figcaption style="padding-left:50px;">
							<p tabindex="8" title="Úsala para comprar sin necesidad de usar efectivo y acumula KMS. LATAM Pass" style="padding:0px;margin:0px;text-align:center;padding-right: 41px;">
								<span class="lightitalic14blue">Úsala para comprar sin necesidad de usar efectivo y acumula KMS. LATAM Pass.</span>
							</p>
						</figcaption>
					</figure>					
				</section>
		</div>
		<div class="lista">		
			<section>
				<header style="height:34px;">						
					<h1><img src="images/visacard/visacard_lanpassprogramicon.png" width="23" height="20" alt=""/>Programa LATAM Pass</h1>
                    
				</header>
				<hr>
				<p style="margin-top:25px;line-height:1.3;" tabindex="9" title="Programa LATAM Pass. Acumulas KMS. LATAM Pass para que los canjees por viajes alrededor del mundo o productos del catálogo de LATAM Pass.">
					<span class="lightitalic14light">Por tus compras acumula </span><span class="mediumitalic14dark"> KMS. LATAM Pass</span><span class="lightitalic14light">, que podrás canjear por viajes dentro del Perú o alrededor del mundo, o por productos del catálogo LATAM Pass.</span>
					</p>				
				<div class="clear"></div>
			</section>			

			<section style="margin-top:25px;">
				<header style="height:34px;">						
					<h1><img src="images/visacard/visacard_featuresicon.png" width="25" height="23" alt=""/>Características</h1>
				</header>
				<hr>
				<ul style="margin: 30px 0;" tabindex="10" title="Características comunes: puedes usarla para realizar compras en establecimientos afiliados a Visa, no te cobra ninguna comisión por compras y funciona como cualquier tarjeta de débito. Tu Tarjeta Credimás LATAM Pass no tiene límite para realizar compras, solo depende del saldo que dispongas en tus cuentas. Además es aceptada en 12,000 millones de establecimientos afiliados a Visa en todo el mundo y más de 60,000 establecimientos afiliados en el Perú. Credimás LATAM Pass te permite realizar todas las operaciones que ya conoces, como comprar, retirar efectivo, transferir dinero, comprar por internet, pagar tu tarjeta o servicios, consultas y más. Con tu Tarjeta Credimás LATAM Pass puedes realizar líbremente operaciones en todos los canales BCP 1 y en cajeros automáticos afiliados a Visa Plus en el Perú y el extranjero.">
					<li><span class="lightitalic14light">Úsala para comprar en cualquier establecimiento afiliado a VISA. También podrás comprar por internet de manera segura con el
código de seguridad (CVV2) que se encuentra al reverso de la tarjeta.</span></li>
					<li><span class="lightitalic14light">Usar tu Tarjeta de Débito BCP LATAM Pass para comprar </span><span class="mediumitalic14dark"> no genera cobro de intereses o pago de comisiones.</span></li>
					<li><span class="lightitalic14light">Es aceptada para comprar en más de 98,000 establecimientos afiliados a VISA en el Perú y en millones de establecimientos a
nivel mundial.
</span></li>
					<li><span class="lightitalic14light">También podrás usarla para ingresar a Banca por Internet y consultar tus saldos, o hacer diversas operaciones<sup>1</sup> como: pagos de
servicios, transferencias, pago de tu Tarjeta de Crédito, etc. Además, podrás realizar operaciones por Agentes BCP o retirar
efectivo de nuestros cajeros automáticos.</span></li>
				</ul>
			</section>
			
			<section>
				<header style="height:34px;">						
					<h1><img src="images/credimas/visadebit_acumulateicon.png" width="27" height="29" alt=""/>Acumulación de KMS. LATAM Pass</h1>
				</header>
				<hr>
				<div tabindex="11" title="Comprando con tu Tarjeta de Débito BCP LATAM Pass, en cualquier establecimiento en el Perú o en el extranjero, por cada S/. 10.00 o US$3.00 acumularás 1 KM. LATAM Pass. El total de los KMS. acumulados con tu Tarjeta de Débito se sumará mensualmente a tu cuenta de socio LATAM Pass.">
					<p style="margin-top:25px;line-height:1.3;margin-bottom:30px;">
						<span class="lightitalic14light">Comprando<sup>2</sup> con tu <span class="mediumitalic14dark">Tarjeta de Débito BCP LATAM Pass</span>, en cualquier establecimiento en el Perú o en el extranjero, por cada S/. 10.00 o US$3.00 acumularás 1 KM. LATAM Pass. El total de los KMS. acumulados con tu Tarjeta de Débito se sumará mensualmente a tu cuenta de socio LATAM Pass<sup>3</sup></span>
						<!--<br /><br />
						<span class="mediumitalic14dark">Seguro por accidente de viajes</span>
						<br /><span class="lightitalic14light">Cobertura alrededor del mundo a titulares, cónyuges e hijos menores de 23 años. Válido si el ticket de viaje fue cancelado en su totalidad con la tarjeta.</span>
						<br /><span class="lightitalic14light">Monto máximo cubierto: US$ 150,000.</span>-->
					</p>
          <!--
					<ul style="margin: 30px 0;">
						<li><span class="lightitalic14light">Asistencia en Viajes.</span></li>
						<li><span class="lightitalic14light">Servicio de Asistencia Médica.</span></li>
						<li><span class="lightitalic14light">Asistencia Legal de Emergencia.</span></li>
						<li><span class="lightitalic14light">Reemplazo urgente de Boletos de Viaje.</span></li>
						<li><span class="lightitalic14light">Asistencia de Transporte de Emergencia.</span></li>
						<li><span class="lightitalic14light">Servicios de Emergencia.</span></li>
					</ul>-->
				</div>
			</section>

      <section>
				<header style="height:34px;">						
					<h1><img src="images/credimas/visadebit_ilimiticon.png" width="28" height="20" alt=""/>Beneficios Exclusivos</h1>
				</header>
				<hr>
				<p style="margin-top:25px;margin-bottom:25px;line-height:1.3;" tabindex="12" title="">
          <span class="mediumitalic14dark">1. Seguro por accidentes de viajes</span><br />
          <span class="lightitalic14light">Cobertura alrededor del mundo para titulares, cónyuges e hijos menores de 23 años. Monto máximo cubierto hasta por US$ 150,000.
Válido solo si el boleto de viaje fue pagado en su totalidad con la Tarjeta de Débito BCP LATAM Pass.</span>
				</p>
        <p style="margin-top:25px;line-height:1.3;" tabindex="12" title=""">
          <span class="mediumitalic14dark">2. Otros Beneficios</span></p>
        <ul>
          <li><span class="lightitalic14light">Centro de Asistencia al Cliente Visa.</span></li>
          <li><span class="lightitalic14light">Centro de Información para Viajes.</span></li>
        </ul>
        <p style="margin-top:25px;line-height:1.3;margin-bottom:30px;" tabindex="12" title=""">
          <span class="lightitalic14light">Para mayor información sobre beneficios ingresa a <a class="mediumitalic14dark" style="text-decoration:none;" href="https://www.viabcp.com">www.viabcp.com</a></span></p>
				<div class="clear"></div>
			</section>	

			<section>
				<header style="height:34px;">						
					<h1><img src="images/credimas/visadebit_obtainicon.png" width="26" height="28" alt=""/>¿Cómo la obtengo?</h1>
				</header>
				<hr>
				<p style="margin-top:25px;margin-bottom:25px;line-height:1.3;" tabindex="12" title="Solicítala en cualquiera de nuestras Agencias BCP. Si ya cuentas con una Tarjeta de Débito Clásica u Oro BCP, puedes cambiarla por una Tarjeta de Débito Clásica BCP LATAM Pass sin costo.">
				<span class="lightitalic14light">Solicítala en cualquiera de nuestras Agencias BCP. Si ya cuentas con una Tarjeta de Débito Clásica u Oro BCP, puedes cambiarla por una Tarjeta de Débito Clásica BCP LATAM Pass sin costo.</span> 
				</p>				
				<div class="clear"></div>
			</section>	
      <br />  

			<hr style="border: 1px solid #EFEFEF; margin-bottom: 20px;">
			<div style="padding-right: 8px;margin-bottom: 30px;" tabindex="13" title="Condiciones legales: 
1. Debes generar tu clave de internet para: Revisar saldos y movimientos, y transferir dinero entre tus cuentas. Si requieres transferir dinero a cuentas de terceros, pagar servicios, y/o pagar tu Tarjeta de Crédito BCP, necesitarás una clave Token, que puedes adquirir en cualquier agencia BCP. 
2. Los consumos en casinos, los pagos de servicios realizados en www.viabcp.com, los débitos automáticos, los pagos de impuestos y los servicios legales no acumulan KMS. LATAM Pass. 
3. Si no tienes una cuenta de socio LATAM Pass, crearemos una por ti el mes posterior a la fecha en la que sacaste tu Tarjeta de Débito. LATAM confirmará la creación de tu cuenta de socio a través de un email, y podrás revisar el estado de cuenta de tus KMS. en www.latam.com/es_pe/latam-pass/
">
        <br />
        <span class="lightitalic11dark" style="font-family:cursive;">
(1) Debes generar tu clave de internet para: Revisar saldos y movimientos, y transferir dinero entre tus cuentas. Si requieres transferir dinero a cuentas de terceros, pagar servicios, y/o pagar tu Tarjeta de Crédito BCP, necesitarás una clave Token, que puedes adquirir en cualquier agencia BCP.
<br /><br />
(2) Los consumos en casinos, los pagos de servicios realizados en <a style="text-decoration:none;" class="lightitalic11dark" href="https://www.viabcp.com">www.viabcp.com</a>, los débitos automáticos, los pagos de impuestos y los servicios legales no acumulan KMS. LATAM Pass.
<br /><br />
(3) Si no tienes una cuenta de socio LATAM Pass, crearemos una por ti el mes posterior a la fecha en la que sacaste tu Tarjeta de Débito. LATAM confirmará la creación de tu cuenta de socio a través de un email, y podrás revisar el estado de cuenta de tus KMS. en <a href="https://www.latam.com/es_pe/latam-pass/" class="lightitalic11dark" style="text-decoration:none;">www.latam.com/es_pe/latam-pass/</a><br /><br />
				</span>	
			</div>
			
		</div>
		<div class="clear"></div>
	</section>

    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Tarjetas").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/nuestras_tarjetas/credimas']);
    </script>

</asp:Content>

