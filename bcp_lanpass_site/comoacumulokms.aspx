﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="comoacumulokms.aspx.cs" Inherits="comoacumulokms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

	<script type="text/javascript">
	    //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
	    Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.Content header h1', { fontFamily: 'Flexo-Bold' });
	</script>	

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

	<section class="Content contentb" style="padding-left: 40px;">
		<header style="height:34px;">
            <h1 tabindex="7" title="¿Cómo acumulo kms.?">¿Cómo acumulo kms.?</h1>
            <div class="div_regresa"><a class="promobtn" href="Default.aspx" tabindex="8" title="Regresar">Regresar</a></div>
		</header>
		
		<section class="texto" tabindex="8" title="Los socios LATAM Pass podrán acumular kilómetros comprando con sus Tarjetas de Crédito BCP LATAM Pass, viajando en LAN, TAM, líneas aéreas líneas aéreas oneworld o líneas aéreas asociadas al programa, adquiriendo productos o servicios de los partners de LATAM Pass como: REPSOL, Pacífico Seguros, Los Portales, Yamaha, Divemotor, Bonus, Casinelli e Imagina. Además de las promociones especiales de acumulación con LATAM Pass o con las distintas empresas asociadas a LATAM Pass.">
			<hr>
				<p class="fixwidth"><span class="mediumitalic16dark">Los socios LATAM Pass podrán acumular kms:</span></p>
				<p class="fixwidth"><img src="images/acumulatekms_cardicon.png" width="18" height="12" alt=""/>
					<span class="lightitalic14dark">Comprando con su </span><span class="mediumitalic14dark">Tarjeta de Crédito o Débito BCP LATAM Pass.</span>
				</p>
				<p class="fixwidth"><img src="images/acumulatekms_planeicon.png" width="18" height="17" alt=""/>
					<span class="lightitalic14dark">Viajando en </span> <span class="mediumitalic14dark">LATAM Airlines, </span><span class="lightitalic14dark">líneas aéreas </span><span class="mediumitalic14dark">oneworld</span><span class="lightitalic14dark"> o en líneas aéreas asociadas al programa.</span>
				</p>
				<p class="fixwidth"><img src="images/acumulatekms_checkicon.png" width="16" height="16" alt=""/>
					<span class="lightitalic14dark">Adquiriendo productos o servicios de la red de comercios asociados como: <span class="mediumitalic14dark">Cabify, Martinizing,        Rocketmiles, iShop, Büro, Delhel, Imagina, V&V Grupo Inmobiliario, Autoland- KIA</span> y otros que puedes       revisar <a class="lightitalic14dark" href="https://www.latam.com/es_pe/latam-pass/como-acumular-mas-kms/comercios-asociados/">aquí</a>.</span>
				</p>
			<hr>
			<p class="lightitalic11dark fixwidth"><span>Además de las promociones especiales de acumulación con LATAM Pass o con las distintas empresas asociadas a LATAM Pass.</span></p>
		</section>
		<section class="image image-km" style="float:right; margin-top:55px; right: 0px !important">
				<img src="images/acumulatekms_imagebag.png" width="309" height="313" alt="" >
		</section>
		<div class="clear"></div>
	</section>


        <script type="text/javascript">
            $(".active").removeClass("active");
            $("#Acumulo").addClass("active");
            _gaq.push(['_trackPageview', '/WebsiteOficial/como_acumulo']);
    </script>
</asp:Content>

