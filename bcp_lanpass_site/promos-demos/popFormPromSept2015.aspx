﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="popFormPromSept2015.aspx.cs" Inherits="popForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>BCP</title>

	<link href="../Styles/fonts.css" rel="stylesheet" type="text/css" />
	<link href="css/ufd-base.css" rel="stylesheet" type="text/css" />
	<link href="css/plain/plain.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/main.css?v=3" />
	<style>
		#btnGrabar {
			background: transparent url("/images/promo2015septiembre/dni-bt-inscribite.jpg") no-repeat scroll center top;
		}
		
		.bgForm ul li .texto,
		.ufd.plain input,
		#ufd-container .plain li  {
			font-family: "flexo-italic";
		}

	</style>
	<script type="text/javascript" src="js/vendor/jquery-1.8.1.min.js"></script>
	<script type="text/javascript" src="js/vendor/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/vendor/numeric.js"></script>
	<script type="text/javascript" src="js/vendor/jquery.alphanumeric.pack.js"></script>
	<script type="text/javascript" src="js/vendor/udf/ui.core.js"></script>
	<script type="text/javascript" src="js/vendor/udf/jquery.bgiframe.min.js"></script>
	<script type="text/javascript" src="js/vendor/jquery.ui.ufd.js"></script>

	<script type="text/javascript">

		/*Validaciones*/
		var ie = (navigator.appName.indexOf("Microsoft") >= 0);
		var firefox = navigator.userAgent.toLowerCase().indexOf('firefox/') > -1;
		var vtdoc = "";

		$(document).ready(function () {
			
			//Efectos sobre los campos de texto
			$("#cboTipDoc").on("focus", function () {
				borde_campos($(this), $("#ic5"), 2);
				$(".er_tdoc").hide();

				$("#combo1 .plain").removeClass("brd_alert1");
				$("#combo1 .plain").addClass("brd_alert2");
				$("#combo1 .plain").removeClass("brd_alert3");
				$("#combo1 .plain").removeClass("brd_alert4");

			});
			$("#cboTipDoc").on("blur", function () {
				if (this.value.trim() == '') {
					borde_campos($(this), $("#ic5"), 1);

					$("#combo1 .plain").addClass("brd_alert1");
					$("#combo1 .plain").removeClass("brd_alert2");
					$("#combo1 .plain").removeClass("brd_alert3");
					$("#combo1 .plain").removeClass("brd_alert4");
				}
				else {
					borde_campos($(this), $("#ic5"), 4);

					$("#combo1 .plain").removeClass("brd_alert1");
					$("#combo1 .plain").removeClass("brd_alert2");
					$("#combo1 .plain").removeClass("brd_alert3");
					$("#combo1 .plain").addClass("brd_alert4");
				}
			});


			$("#txtNumDoc").on("focus", function () {
				if (this.value == 'Nro de doc') {
					this.value = '';
				}
				borde_campos($(this), $("#ic6"), 2);
				$(".er_ndoc").hide();
			});
			$("#txtNumDoc").on("blur", function () {
				if (this.value.trim() == '') {
					this.value = 'Nro de doc';
					borde_campos($(this), $("#ic6"), 1);
				}
				if (this.value != 'Nro de doc') {
					borde_campos($(this), $("#ic6"), 4);
				}
			});

			$('#txtNumDoc').numeric({ allow: "1234567890", ichars: '=àèìòùÀÈÌÒÙ()áéíóúÁÉÍÓÚ!"·$%&/çÇ¡¿?~+-_.;,^`{}[]\'*¨´<>?:"{}|+_)(*&^%$#@!~≤≥çÇ…æ“‘«≠–ºª•¶§∞¢£™¡`÷\`\¡Ω≈√∫˜≤≥æ…˚∆˙©ƒ∂ßåœ∑´†¥¨ˆπ“‘«≠–ºª•¶§∞¢£™°\\' });
			$('input').on("cut copy paste", function (e) { e.preventDefault(); });

		});

		function borde_campos(obj, ic ,b) {
			if (b == 1) {
				//Efecto simple (default)
				obj.addClass("brd_alert1");
				obj.removeClass("brd_alert2");
				obj.removeClass("brd_alert3");
				obj.removeClass("brd_alert4");

				ic.removeClass("icon_error");
				ic.removeClass("icon_ok");
			}
			if (b == 2) {
				//Efecto active (azul)
				obj.removeClass("brd_alert1");
				obj.addClass("brd_alert2");
				obj.removeClass("brd_alert3");
				obj.removeClass("brd_alert4");

				ic.removeClass("icon_error");
				ic.removeClass("icon_ok");
			}
			if (b == 3) {
				//Efecto Error (rojo)
				obj.removeClass("brd_alert1");
				obj.removeClass("brd_alert2");
				obj.addClass("brd_alert3");
				obj.removeClass("brd_alert4");

				//icono rojo
				ic.addClass("icon_error");
				ic.removeClass("icon_ok");
			}
			if (b == 4) {
				//Efecto ok (verde)
				obj.removeClass("brd_alert1");
				obj.removeClass("brd_alert2");
				obj.removeClass("brd_alert3");
				obj.addClass("brd_alert4");

				//icono verde
				ic.removeClass("icon_error");
				ic.addClass("icon_ok");
			}
		}

		function setLength(objSelect) {
			//$("#txtNumDoc").focus();
			if (objSelect.value == 'CE') {
				document.getElementById('txtNumDoc').maxLength = 12;
				vtdoc = "CE";
				//$("#txtNumDoc").attr("onkeypress", "return alfanum(event);");
				$('#txtNumDoc').off();
				$('#txtNumDoc').val("").focus().alphanumeric({ ichars: '=àèìòùÀÈÌÒÙ()áéíóúÁÉÍÓÚ!"·$%&/çÇ¡¿?~+-_.;,^`{}[]\'*¨´<>?:"{}|+_)(*&^%$#@!~≤≥çÇ…æ“‘«≠–ºª•¶§∞¢£™¡`÷\`\¡Ω≈√∫˜≤≥æ…˚∆˙©ƒ∂ßåœ∑´†¥¨ˆπ“‘«≠–ºª•¶§∞¢£™°\\ ' }).on("cut copy paste", function (e) { e.preventDefault(); }); ;
				//$("#txtNumDoc").rules("remove");
				//$("#txtNumDoc").rules("add", { required: true, rangelength: [4, 11] });
			}
			if (objSelect.value == 'DNI') {
				document.getElementById('txtNumDoc').maxLength = 8;
				vtdoc = "DNI";
				$('#txtNumDoc').off();
				$('#txtNumDoc').val("").focus().numeric({ allow: "1234567890", ichars: '=àèìòùÀÈÌÒÙ()áéíóúÁÉÍÓÚ!"·$%&/çÇ¡¿?~+-_.;,^`{}[]\'*¨´<>?:"{}|+_)(*&^%$#@!~≤≥çÇ…æ“‘«≠–ºª•¶§∞¢£™¡`÷\`\¡Ω≈√∫˜≤≥æ…˚∆˙©ƒ∂ßåœ∑´†¥¨ˆπ“‘«≠–ºª•¶§∞¢£™°\\ ' }).on("cut copy paste", function (e) { e.preventDefault(); }); ;
			}
			if (objSelect.value == 'PA') {
				document.getElementById('txtNumDoc').maxLength = 12;
				vtdoc = "PA";
				$('#txtNumDoc').off();
				$('#txtNumDoc').val("").focus().alphanumeric({ ichars: '=àèìòùÀÈÌÒÙ()áéíóúÁÉÍÓÚ!"·$%&/çÇ¡¿?~+-_.;,^`{}[]\'*¨´<>?:"{}|+_)(*&^%$#@!~≤≥çÇ…æ“‘«≠–ºª•¶§∞¢£™¡`÷\`\¡Ω≈√∫˜≤≥æ…˚∆˙©ƒ∂ßåœ∑´†¥¨ˆπ“‘«≠–ºª•¶§∞¢£™°\\ ' }).on("cut copy paste", function (e) { e.preventDefault(); }); ;
			}
			$("#txtNumDoc").val("Nro de doc");
			$("#txtNumDoc").on("focus", function () {
				if (this.value == 'Nro de doc') {
					this.value = '';
				}
				borde_campos($(this), $("#ic6"), 2);
				$(".er_ndoc").hide();
			});
			$("#txtNumDoc").on("blur", function () {
				if (this.value.trim() == '') {
					this.value = 'Nro de doc';
					borde_campos($(this), $("#ic6"), 1);
				}
				if (this.value != 'Nro de doc') {
					borde_campos($(this), $("#ic6"), 4);
				}
			});
		}

		function enviarForm() {
			parent.VentanRegisrarDatos();
		}
		function showError() {
			parent.RegistroDuplicado();
		}
		function checkFields() {
			var boolReturn = true;
			var countErrors = 0;

			//Tipo de documento
			if ($("#cboTipDoc").val() == "") {
				countErrors++;
				borde_campos($("#cboTipDoc"), $("#ic5"), 3);
				$(".er_tdoc").html("Recuerda ingresar tu tipo de documento.");
				$(".er_tdoc").css('display', 'block');

				$("#combo1 .plain").removeClass("brd_alert1");
				$("#combo1 .plain").removeClass("brd_alert2");
				$("#combo1 .plain").addClass("brd_alert3");
				$("#combo1 .plain").removeClass("brd_alert4");
			}
			else {
				borde_campos($("#cboTipDoc"), $("#ic5"), 4);
				$(".er_tdoc").css('display', 'none');

				$("#combo1 .plain").removeClass("brd_alert1");
				$("#combo1 .plain").removeClass("brd_alert2");
				$("#combo1 .plain").removeClass("brd_alert3");
				$("#combo1 .plain").addClass("brd_alert4");
			}
			//vtdoc

			//Numero de documento
			if ($("#txtNumDoc").val().trim() == "" || $("#txtNumDoc").val() == "Nro de doc") {
				countErrors++;
				borde_campos($("#txtNumDoc"), $("#ic6"), 3);
				$(".er_ndoc").html("Recuerda ingresar tu número de documento.");
				$(".er_ndoc").attr("title", "Recuerda ingresar tu número de documento. Debe tener mínimo 8 dígitos. Aún no lo has hecho o no lo has ingresado de forma correcta.");
				$(".er_ndoc").css('display', 'block');

			}
			else {
				if (vtdoc == "CE") {
					if ($("#txtNumDoc").val().length < 8) {
						countErrors++;
						borde_campos($("#txtNumDoc"), $("#ic6"), 3);
						$(".er_ndoc").html("Tu Carné de extrangería debe tener entre 8 y 12 dígitos.");
						$(".er_ndoc").attr("title", "Recuerda ingresar tu número de Carné de extranjería. Debe tener entre 8 y 12 dígitos. Aún no lo has hecho o no lo has ingresado de forma correcta. ");
						$(".er_ndoc").css('display', 'block');
					}
					else {
						borde_campos($("#txtNumDoc"), $("#ic6"), 4);
						$(".er_ndoc").css('display', 'none');
					}
				}
				if (vtdoc == "DNI") {
					if ($("#txtNumDoc").val().length < 8) {
						countErrors++;
						borde_campos($("#txtNumDoc"), $("#ic6"), 3);
						$(".er_ndoc").html("Tu DNI debe tener entre 8 dígitos.");
						$(".er_ndoc").attr("title", "Recuerda ingresar tu número de DNI. Debe tener 8 dígitos. Aún no lo has hecho o no lo has ingresado de forma correcta.");
						$(".er_ndoc").css('display', 'block');
					}
					else {
						borde_campos($("#txtNumDoc"), $("#ic6"), 4);
						$(".er_ndoc").css('display', 'none');
					}
				}
				if (vtdoc == "PA") {
					if ($("#txtNumDoc").val().length < 8) {
						countErrors++;
						borde_campos($("#txtNumDoc"), $("#ic6"), 3);
						$(".er_ndoc").html("Tu pasaporte debe tener entre 8 y 12 dígitos.");
						$(".er_ndoc").attr("title", "Recuerda ingresar tu número de Pasaporte. Debe tener entre 8 y 12 dígitos. Aún no lo has hecho o no lo has ingresado de forma correcta. ");
						$(".er_ndoc").css('display', 'block');
					}
					else {
						borde_campos($("#txtNumDoc"), $("#ic6"), 4);
						$(".er_ndoc").css('display', 'none');
					}
				}
				
				
			}
			//vnumdoc

			//Ver errores
			if (countErrors > 0) {
				return false;
			}
		}

		function fnumerico(e) {
			var tecla = (ie) ? e.keyCode : e.which; if (tecla == 8 || tecla == 0) return true;
			var patron = /^[0-9]*$/; te = String.fromCharCode(tecla);
			if (!patron.test(te)) { (ie) ? e.keyCode = 0 : e.which = 0; return false; } else return true;
		}
		function alfanum(e) {
			key = e.keyCode || e.which;

			tecla = String.fromCharCode(key).toLowerCase();
			letras = " abcdefghijklmnopqrstuvwxyz0123456789";
			especiales = [8, 9, 37, 39, 46, 97, 98, 113, 119, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112,  114, 115, 116, 117, 118, 120, 121, 122];
			tecla_especial = false
			for (var i in especiales) {
				if (key == especiales[i]) {
					tecla_especial = true;
					break;
				}
			}
			//console.log(key);

			if (letras.indexOf(tecla) == -1 && !tecla_especial) {
				return false;
			}
		}
		function fletras(e) {
			key = e.keyCode || e.which;

			tecla = String.fromCharCode(key).toLowerCase();
			letras = " áéíóúäëïöüabcdefghijklmnñopqrstuvwxyz";
			especiales = [8, 9, 37, 39, 46];
			tecla_especial = false
			for (var i in especiales) {
				if (key == especiales[i]) {
					tecla_especial = true;
					break;
				}
			}

			if (letras.indexOf(tecla) == -1 && !tecla_especial) {
				return false;
			}

		}

		

	</script>
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-35160300-11']);
		_gaq.push(['_trackPageview', '/WebsiteOficial/campana-amex/formulario']);
		(function () {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

</head>
<body class="not-bg">
	<form id="form1" runat="server">
		<asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
		<div class="bgForm bgFormGris">
			<h1 class="dottedbottom" title="¡Averigua cuál es tu reto y bono!  Ingresa tu número de DNI.">¡Averigua cuál es tu reto y bono!  Ingresa tu número de DNI.</h1>
			<ul>
				<li class="cmbo1">
					<div id="ic5" class="icon_alert"></div>
					<div id="combo1" title="Selecciona tu tipo de documento.">
					<asp:DropDownList ID="cboTipDoc" runat="server" class="combo" title="Selecciona tu tipo de documento.">
						<asp:ListItem Value="">Tipo de doc</asp:ListItem>
						<asp:ListItem Value="DNI">DNI</asp:ListItem>  
						<asp:ListItem Value="CE">Carné de extranjería</asp:ListItem>
						<asp:ListItem Value="PA">Nro de pasaporte</asp:ListItem>
					</asp:DropDownList>
					</div>
					<script type="text/javascript">
						$(function () {
							$("#cboTipDoc").ufd({
								log: true,
								minWidth: 264, // don't autosize smaller then this.
								maxWidth: 264, // null, or don't autosize larger then this.
								manualWidth: 264
							});
							$(".cmbo1 input").attr('readonly', true);
						});
					</script>
					
					<label for="cboTipDoc" class="er er_tdoc" title="Recuerda ingresar tu tipo de documento. Aun no lo has hecho."></label>
					
				</li>
				<li>
					<asp:TextBox ID="txtNumDoc" class="texto brd_alert1" MaxLength="12" tabindex="1" runat="server" value="Nro de doc" title="Ingresa tu número de documento."></asp:TextBox>
					<label for="txtNumDoc" class="er er_ndoc"></label>
					<div id="ic6" class="icon_alert"></div>
				</li>
 				<li>
                    <p>*Si cuentas con carné de extranjería ingresa solo los 8 últimos dígitos de este.</p>
				</li>
 				<li>
					<asp:button ID="btnGrabar" CssClass="btnEnviar" OnClick="btnGrabar_Click"  tabindex="2" runat="server" text="" title="Inscríbete aquí."></asp:button>
				</li>
			</ul>      
		
			<asp:UpdatePanel id="upd" runat="server">
			<ContentTemplate></ContentTemplate>
			<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnGrabar" EventName="Click" />
			</Triggers>
			</asp:UpdatePanel>
			
		</div>
	</form>
</body>
</html>
