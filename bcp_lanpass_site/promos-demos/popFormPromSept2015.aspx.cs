﻿using System;
using System.Web.UI;

public partial class popForm : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btnGrabar.Attributes.Add("onclick", "return checkFields();");
        cboTipDoc.Attributes.Add("onchange", "setLength(this);");
    }

    private void mValidarDni()
    {
        var lstrTipoDocumento = cboTipDoc.SelectedValue.Trim();
        var lstrNumeroDocumento = txtNumDoc.Text.Trim();
        var service = new BLPromocion();

        var metaBono = service.BuscarMetaYBono(lstrTipoDocumento, lstrNumeroDocumento);

        Session["IdCliente"] = metaBono.IdCliente;

        var funcion = "";
        if (metaBono.Resultado == 0)
            funcion = "parent.RegistroDuplicado()";
        if (metaBono.Resultado == 1)
            funcion = "parent.NoHayCupos()";
        if (metaBono.Resultado == 2)
            funcion = "parent.NoHayTarjeta()";
        if (metaBono.Resultado == 3)
            funcion = "parent.VentanRegisrarDatos()";

         ScriptManager.RegisterClientScriptBlock(
            Page,
            GetType(),
            "ajaxBindings",
            funcion,
            true);
    }

    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        mValidarDni();
    }
}