﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="promoamexx5.aspx.cs" Inherits="promo_navidad" %>

<!doctype html>
<html lang="es">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1280">
    <title>BCP - Tarjetas BCP LANPASS</title>
    <meta property="og:title" content="BCP - LANPASS">
    <meta property="og:site_name" content="BCP - LANPASS">
    <meta property="og:description" content="Compra con tu Tarjeta American Express® y multiplica por 5 tus KMS. LANPASS. Entérate más aquí.">
    <meta property="og:type" content="company">
    <meta property="og:url" content="http://www.bcplatampass.com/promoamexx5">
    <meta property="og:image" content="http://www.bcplatampass.com/images/fb_share.png?v=723897129387">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico?v=1" />

    <link rel="stylesheet" href="/Styles/reset.css" type="text/css" />
    <link rel="stylesheet" href="/Styles/style.css?v=291014" type="text/css" />
    <link rel="stylesheet" href="/Styles/fonts.css" type="text/css" />
    <link rel="stylesheet" href="/Styles/colorbox.css" type="text/css" />
    <link rel="stylesheet" href="/promo-amexx5/cssM/main.css" type="text/css" />

    <!--[if lt IE 9]>
	        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	        <link rel="stylesheet" href="Styles/style-ie.css">
	    <![endif]-->
    <!--[if lt IE 7]>        
	        <link rel="stylesheet" href="Styles/style-ie6.css">
	    <![endif]-->

    <script src="/js/jquery.min.js"></script>
    <script src="/js/cufon-yui.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/jquery.colorbox.js"></script>
    <script src="/js/jquery.carouFredSel.min.js"></script>
    <script src="/js/jquery.placeholder.js"></script>
    <script src="/js/Flexo-Regular.font.js"></script>
    <script src="/js/Flexo-Light.font.js"></script>
    <script src="/js/Flexo-Bold.font.js"></script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35160300-11']);
    </script>
    </head>
<body>
    <div id="bodydivforbg">
        <header class="header">
            <div class="content">
                <div class="logobcp">
                </div>
                <a href="http://www.latam.com/latampass/es_pe/sitio_personas/index.html" target="_blank" class="logolan"></a>
            </div>
        </header>
        <nav class="navbar">
            <div class="content">
                <ul>
                    <li id="Inicio"><a href="/" tabindex="1" title="Inicio">Inicio</a></li>
                    <li id="Tarjetas"><a href="nuestrastarjetas.aspx" tabindex="2" title="Nuestras tarjetas">Nuestras tarjetas</a></li>
                    <li id="Quees"><a href="queslanpass.aspx" tabindex="3" title="¿Qué es LANPASS?">¿Qué es LANPASS?</a></li>
                    <li id="Acumulo"><a href="comoacumulokms.aspx" tabindex="4" title="¿Cómo acumulo kms.?">¿Cómo acumulo kms.?</a></li>
                    <li id="Canjeo"><a href="quepuedocanjear.aspx" tabindex="5" title="¿Qué puedo canjear?">¿Qué puedo canjear?</a></li>
                </ul>
            </div>
        </nav>

        <div class="wrap">
            <section class="headContent">
                <div class="contentwrap" tabindex="6" title="Tarjetas BCP LANPASS. Tus compras acumulan KMS. LANPASS y te acercan a tu próximo viaje.">
                    <header>
                        <h1>Tarjetas BCP LANPASS</h1>
                    </header>
                    <p>Tus compras acumulan KMS. LANPASS y te acercan a tu próximo viaje.</p>
                </div>
            </section>

    <section class="ContentPromo">
        <div class="wrapped">
            <div class="intern-lanpass-ae">
                <a href="/" class="btn-return"></a>
                <div class="note">
                	<p>Tu Tarjeta American Express®<br>
                	  BCP LANPASS te acerca<br>
               	    a tu pr&oacute;ximo viaje.</p>
                    <p style="font-size:18px;line-height:23px;font-family: 'flexoitalic';margin-top:-5px;"><br>
                      Del 1 al 30 de junio todas tus<br>
                    compras* multiplican tus KMS LANPASS X5.</p><p></p><a href="javascript:void(0);" class="btn-inscribete sprite fancybox"></a>
				</div>
            </div>
            <div class="legal">
            	<p style="background-color:#000;height:2px;margin-top:13px;">&nbsp;</p>
                <img style="text-align:center;" src="/promo-amexx5/img/bg-legal-navidad.jpg"  />
                <p style="margin-top:2px;">Esta campa&ntilde;a est&aacute; a cargo del BCP.  Promoci&oacute;n vigente del 01/06/15 al 30/06/15. Aplica solo para clientes con tarjeta de cr&eacute;dito American Express BCP LANPASS inscritos entre el 01/06/15  y el 30/06/15 a trav&eacute;s de bcplatampass.com, Banca por Tel&eacute;fono al 311 9898 opci&oacute;n 9-9 o Banca Exclusiva al 311 9797. Promoci&oacute;n v&aacute;lida a nivel nacional. El 1er abono de KMS. LANPASS corresponder&aacute; a los consumos regulares y se realizar&aacute; 48 horas despu&eacute;s de la fecha de corte de facturaci&oacute;n de su tarjeta.  El 2do abono de KMS. LANPASS ser&aacute; acreditando en cada cuenta de socio LANPASS titular de la tarjeta a m&aacute;s tardar el 31/08/15 y corresponder&aacute; a los kms. adicionales por la promoci&oacute;n. Abono m&aacute;ximo por cliente 15,000 KMS. LANPASS. Stock m&iacute;nimo: 20,000 KMS. LANPASS. Para fines de abono la tarjeta del cliente debe encontrarse activa y sin bloqueo. No acumulan Kms. las disposiciones de efectivo, compras de deuda, efectivo preferente, consumos en casinos, pago de impuestos, consumos en servicios legales y abogados y pagos de servicios que se realizan por viabcp.com ni cargos por d&eacute;bito autom&aacute;tico. Los temas relacionados con el servicio de transporte de LAN, as&iacute; como la operatividad del programa LANPASS, son de responsabilidad de LAN. La acumulaci&oacute;n, canje, uso y dem&aacute;s condiciones aplicables a los KMS. LANPASS se rigen bajo el reglamento de LANPASS publicado en lan.com/lanpass. Para m&aacute;s informaci&oacute;n sobre la promoci&oacute;n, restricciones, costos y tributos aplicables a las tarjetas del BCP llama a nuestra Banca por Tel&eacute;fono al 311-9898 o ingresa a viabcp.com. American Express es una marca de American Express. Las tarjetas son emitidas por BCP bajo una licencia de American Express.</p>
                <p style="background-color:#000;height:2px;margin-top:13px;margin-bottom:13px;">&nbsp;</p>
            </div>
        </div>



    </section>

    <footer class="footer">
        
        <div class="pull-left" style="width: 100px; margin-top: 25px;">
            Compartir en: 
        </div>
        <div class="pull-left" style="width: 50px; margin-top: 20px;">
            <a class="fbbuttonclick" href="">
                <img src="/images/facebook.png" width="22" height="22" alt=""></a>
        </div>
        
        <div class="pull-left" style="margin-top: 20px;">
            <div class="fb-like" data-href="http://www.bcplatampass.com/" data-width="450" data-layout="button_count" data-action="like" data-show-faces="false" data-send="false"></div>
        </div>
    </footer>

    <div id="fb-root"></div>
    <script type="text/javascript">
        var android, iphone, ipad, uagent, vwidth;
        uagent = navigator.userAgent.toLowerCase();
        android = uagent.search(/android/);
        iphone = uagent.search(/iphone/);
        ipad = uagent.search(/ipad/);
        vwidth = $(document).width();

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=171557349613774";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $(document).on('ready', function () {
            _gaq.push(['_trackPageview', '/WebsiteOficial/campana-amex']);

            $(".fbbuttonclick").on("click", function (event) {
                event.preventDefault();
                _gaq.push(['_trackEvent', 'WebProducto', 'Footer', 'Btn_Facebook']);
                window.open("https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.bcplatampass.com%2Fpromoamexx5", '', 'left=20,top=20,width=500,height=550,toolbar=1,resizable=0');
            });
            $(".active").removeClass("active");

            $('.btn-inscribete').on('click', function ()
            {
                $('body').removeClass('promolanpass');
                $.colorbox({
                    fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
                    innerHeight: true,
                    innerWidth: true,
                    width: 510,
                    height: 700,
                    onOpen: function () {
                        $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=0.99, maximum-scale=0.99');
                        $('#cboxClose').hide();
                        if ($.browser.msie) {
                            if (parseInt($.browser.version) == 6) {
                                $('#cboxOverlay').css({
                                    'position': 'absolute',
                                    'width': $(window).width(),
                                    'height': $(document).height()
                                });
                            }
                        }
                        if (android > 1) {
                            //Si es un dispositivo en android
                            $('#cboxOverlay').css({
                                'position': 'absolute',
                                'width': '285%',
                                'height': $(document).height(),
                                'overflow': 'hidden'

                            });
                            //var w_modal = $("#colorbox").width();
                            //var new_left = Math.ceil(w_modal / 2) * (-1);
                            nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                            $('#colorbox').css("left", nwidth + "px");
                        }
                        if (ipad > 1) {
                            //alert("Ipad");
                        }
                        if (iphone > 1) {
                            nwidth = Math.ceil(vwidth / 2) - Math.ceil(510 / 2);
                            $('#colorbox').css("left", nwidth + "px");
                        }

                    },
                    onCleanup: function () {
                        $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
                    },
                    onClosed: function () { $('#cboxClose').hide(); },
                    onLoad: function () { $('#cboxClose').hide(); },
                    onComplete: function () {
                        iframe();
                        $('#cboxClose').show();
                        //                    _gaq.push(['_trackPageview', '/WebsiteOficial/campana-usala-todos-los-dias/formulario']);
                    },
                    close: '',
                    href: '/promo-amexx5/popformprom.aspx'
                });
            });
        });
        function CerrarVentana() {
            _gaq.push(['_trackEvent', 'WebsiteOficial', 'campana-amex', 'btn_enviar-datos']);
            $('body').addClass('promolanpass');
            var msg = "Empieza a acumular KMS. LANPASS."; //Para agregar mensajes variados, dependiendo de la campaña - vFrancisco-291014
            $.colorbox({
                fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
                width: 600, height: 385,
                onOpen: function () { $('#cboxClose').hide(); },
                onClosed: function () { $('#cboxClose').hide(); },
                onLoad: function () { $('#cboxClose').hide(); },
                onComplete: function () {
                    iframe(); $('#cboxClose').show();
                   // _gaq.push(['_trackPageview', '/WebsiteOficial/campana-amexx5/formulario-registrosatisfactorio']);
                },
                close: '',
                href: '/promo-amexx5/registro-correcto.aspx',
                onCleanup: function () {
                    $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
                }
            });
        }
        function RegistroDuplicado() {
            _gaq.push(['_trackEvent', 'WebsiteOficial', 'campana-amex', 'btn_enviar-datos']);
            $('body').addClass('promolanpass');
            $.colorbox({
                fastIframe: true, iframe: true, preloading: true, scrolling: false, escKey: false, overlayClose: false,
                width: 600, height: 385,
                onOpen: function () { $('#cboxClose').hide(); },
                onClosed: function () { $('#cboxClose').hide(); },
                onLoad: function () { $('#cboxClose').hide(); },
                onComplete: function () { iframe(); $('#cboxClose').show(); },
                close: '',
                href: '/promo-amexx5/registro-duplicado.aspx',
                onCleanup: function () {
                    $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=0.99');
                }
            });
        }
        function iframe() {
            $('iframe').load(function () { $(this).fadeIn('fast'); });
        }

        $(document).ready(function () {
            $(window).resize(function () {
                /*if (android > 1) {
                    $('#cboxOverlay').width({
                        'width': '2024',
                        'height': $(document).height()
                    });
                }*/

            });


        });
    </script>

        </div>
    </div>

    <script type="text/javascript">
        //<![CDATA[
        Cufon.now(); //]]></script>
    <script type="text/javascript">
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!--
        Start of DoubleClick Floodlight Tag: Please do not remove
        Activity name of this tag: BPC / Retargeting
        URL of the webpage where the tag is expected to be placed: https://www.viabcp.com/wps/portal/viabcpp/personas
        This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
        Creation Date: 08/15/2014
    -->
    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="https://4489840.fls.doubleclick.net/activityi;src=4489840;type=invmedia;cat=WCA4wjru;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
    </script>
    <noscript>
        <iframe src="https://4489840.fls.doubleclick.net/activityi;src=4489840;type=invmedia;cat=WCA4wjru;ord=1?" width="1" height="1" frameborder="0" style="display: none"></iframe>
    </noscript>
    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

    <!-- Facebook Conversion Code for BCP -Noviembre 2014 - Lan Pass  -->
    <script>(function() {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
    }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6017462040590', {'value':'0.01','currency':'USD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6017462040590&amp;cd[value]=0.01&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
      _fbq.push(['addPixelId', '1493357820896130']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1493357820896130&amp;ev=PixelInitialized" /></noscript>
    
</body>
</html>
