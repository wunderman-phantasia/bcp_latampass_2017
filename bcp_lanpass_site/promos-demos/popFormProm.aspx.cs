﻿using System;
using System.Web.UI;
using BCPPLANTRIPLICA_FE_DAL;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.IO;

public partial class popForm : Page
{
    readonly string _lstrConnectionString = ConfigurationManager.ConnectionStrings["DB"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        btnGrabar.Attributes.Add("onclick", "return checkFields();");
        cboTipDoc.Attributes.Add("onchange", "setLength(this);");
    }

    private void mGrabarRegistro()
    {
        try
        {
            var lintResult = 0;
            var lgIdRegistro = Guid.NewGuid();
            var lstrNombre = txtNombre.Text.Trim();
            var lstrPaterno = txtApePat.Text.Trim();
            var lstrMaterno = txtApeMat.Text.Trim();
            var lstrCorreoElctronico = txtEmail.Text.Trim();
            var lstrTipoDocumento = cboTipDoc.SelectedValue.Trim();
            var lstrNumeroDocumento = txtNumDoc.Text.Trim();
            var lstrtelefono = txtCel.Text.Trim();
            var lstrDpto = cboDep.SelectedValue;
            const string lstrCampania = "Amex_x5_20150601";
            var lobjDAL = new CRegistroDAL(_lstrConnectionString);

            var ldt = lobjDAL.mListarRegistroBase2014(lstrTipoDocumento, lstrNumeroDocumento, lstrCampania);
            if (ldt.Rows.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(
                this.Page,
                this.GetType(),
                "YaExiste",
                "showError();",
                true);
            }
            else
            {
                lintResult = lobjDAL.mGrabarRegistroBase2014(
                        lgIdRegistro,
                        lstrNombre,
                        lstrPaterno,
                        lstrMaterno,
                        lstrCorreoElctronico,
                        lstrTipoDocumento,
                        lstrNumeroDocumento,
                        lstrtelefono,
                        lstrDpto,
                        lstrCampania
                    );

                try
                {
                    string lstrBody = "";
                    string lstrSubject = "";
                    string lstrToEmail = lstrCorreoElctronico;
                    string lstrToName = lstrNombre;

                    string lstrFromEmail = Convert.ToString(ConfigurationManager.AppSettings["WebMasterMail"]);
                    string lstrFromName = Convert.ToString(ConfigurationManager.AppSettings["WebMasterName"]);

                    string lstrAppPath = Server.MapPath("~");
                    StreamReader lstrStreamReader = new StreamReader(lstrAppPath + Path.GetDirectoryName(Request.Url.AbsolutePath) + "\\Mailing\\index.html");
                    string lstrEmailContent = lstrStreamReader.ReadToEnd();
                    lstrStreamReader.Close();

                    lstrBody = lstrEmailContent;
                    lstrBody = lstrBody.Replace("!#nombre#!", lstrNombre);

                    lstrSubject = "Confirmación de inscripción";
                    mEnviarCorreo(lstrFromEmail, lstrFromName, lstrToEmail, lstrToName, lstrSubject, lstrBody, true);
                }
                catch (Exception lobjEx)
                {

                }

                ScriptManager.RegisterClientScriptBlock(
                this.Page,
                this.GetType(),
                "ajaxBindings",
                "enviarForm();",
                true);
            }


        }
        catch (Exception lExcErr)
        {
            throw new Exception(lExcErr.Message);
        }
    }

    public void mEnviarCorreo(string pstrCorreoRemitente,
         string pstrNombreRemitente,
         string pstrCorreoDestinatario,
         string pstrNombreDestinatario,
         string pstrAsunto,
         string pstrMensaje,
         bool pboolEsHtml)
    {

        var lstmpCorreo = new SmtpClient();
        var lmsgMensaje = new MailMessage();
        var laddCorreoRemitente = new MailAddress(pstrCorreoRemitente, pstrNombreRemitente);
        var laddCorreoDestinatario = new MailAddress(pstrCorreoDestinatario, pstrNombreDestinatario);
        lmsgMensaje.From = laddCorreoRemitente;
        lmsgMensaje.To.Add(laddCorreoDestinatario);
        lmsgMensaje.Subject = pstrAsunto;
        lmsgMensaje.Body = pstrMensaje;
        lmsgMensaje.IsBodyHtml = pboolEsHtml;
        lmsgMensaje.Priority = MailPriority.Normal;
        lmsgMensaje.BodyEncoding = Encoding.UTF8;
        lstmpCorreo.Host = Convert.ToString(ConfigurationManager.AppSettings["CorreoHost"]);
        lstmpCorreo.Port = Convert.ToInt32(ConfigurationManager.AppSettings["CorreoPuerto"]);

        System.Net.NetworkCredential cred = new System.Net.NetworkCredential(Convert.ToString(ConfigurationManager.AppSettings["CorreoUser"]), Convert.ToString(ConfigurationManager.AppSettings["CorreoPassword"]));
        lstmpCorreo.UseDefaultCredentials = false;
        lstmpCorreo.DeliveryMethod = SmtpDeliveryMethod.Network;
        lstmpCorreo.EnableSsl = false;
        lstmpCorreo.Credentials = cred;

        lstmpCorreo.Send(lmsgMensaje);

    }


    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        mGrabarRegistro();
    }
}