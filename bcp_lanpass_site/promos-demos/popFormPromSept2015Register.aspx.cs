﻿using System;
using System.Web.UI;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.IO;
using Recaptcha.Web;

public partial class PopForm : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;

        var idCliente = (string) Session["IdCliente"];

        if (idCliente == null) Response.Redirect("/error/error.aspx");

        var persona = new BLPromocion().BuscarCliente(idCliente);

        txtNombre.Text = persona.Nombres;
        txtApePat.Text = persona.ApellidoPaterno;
        txtApeMat.Text = persona.ApellidoMaterno;
        txtNumDoc.Text = persona.Documento;
        txtTipDoc.Text = persona.TipoDocumento;
        Meta.Value = persona.Meta.ToString();
        Bono.Value = persona.Bono.ToString();

        btnGrabar.Attributes.Add("onclick", "return checkFields();");
    }

    private void mGrabarRegistro()
    {
        if (String.IsNullOrEmpty(ctrlGoogleReCaptcha.Response))
        {
            ScriptManager.RegisterClientScriptBlock(
                Page,
                GetType(),
                "ajaxBindings",
                "errorCaptcha('Debe llenar el captcha');",
                true);

            return;
        }

        var result = ctrlGoogleReCaptcha.Verify();
        if (result == RecaptchaVerificationResult.IncorrectCaptchaSolution)
        {
            ScriptManager.RegisterClientScriptBlock(
                Page,
                GetType(),
                "ajaxBindings",
                "errorCaptcha('Captcha incorrecto');",
                true);

            return;
        }

        if (result != RecaptchaVerificationResult.Success)
        {
            ScriptManager.RegisterClientScriptBlock(
                Page,
                GetType(),
                "ajaxBindings",
                "errorCaptcha('Captcha incorrecto');",
                true);

            return;
        }

        var lgIdRegistro = Guid.NewGuid();
        var idCliente = (string)Session["IdCliente"];
        if (idCliente == null) Response.Redirect("/error/error.aspxError.aspx");
        var persona = new BLPromocion().BuscarCliente(idCliente);


        var lstrNombre = persona.Nombres;
        var lstrPaterno = persona.ApellidoPaterno;
        var lstrMaterno = persona.ApellidoMaterno;
        var lstrCorreoElctronico = txtEmail.Text.Trim();
        var lstrTipoDocumento = persona.TipoDocumento;
        var lstrNumeroDocumento = persona.Documento;
        var lstrtelefono = txtCel.Text.Trim();
        var lstrDpto = cboDep.SelectedValue;

        var lstrBono = persona.Bono;
        var lstrMeta = persona.Meta;
        var service = new BLPromocion();

        var resultado = String.Empty;
        try
        {
             resultado = service.RegistrarCliente(
                lgIdRegistro,
                lstrNombre,
                lstrPaterno,
                lstrMaterno,
                lstrCorreoElctronico,
                lstrTipoDocumento,
                lstrNumeroDocumento,
                lstrtelefono,
                lstrDpto);

        }
        catch (Exception exception)
        {
            resultado = exception.Message;
        }

        if (!String.IsNullOrEmpty(resultado))
        {
            ScriptManager.RegisterClientScriptBlock(
                Page,
                GetType(),
                "ajaxBindings",
                "errorDatos('" + resultado + "');",
                true);

            return;
        }

        EnviarCorreoConfirmacion(lstrCorreoElctronico, lstrNombre, lstrMeta, lstrBono);

        ScriptManager.RegisterClientScriptBlock(
            Page,
            GetType(),
            "ajaxBindings",
            "enviarForm();",
            true);
    }

    private void EnviarCorreoConfirmacion(string lstrCorreoElctronico, string lstrNombre, long lstrMeta, long lstrBono)
    {
        try
        {
            var lstrBody = "";
            var lstrSubject = "";
            var lstrToEmail = lstrCorreoElctronico;
            var lstrToName = lstrNombre;

            var lstrFromEmail = Convert.ToString(ConfigurationManager.AppSettings["WebMasterMail"]);
            var lstrFromName = Convert.ToString(ConfigurationManager.AppSettings["WebMasterName"]);

            var lstrAppPath = Server.MapPath("~");
            var lstrStreamReader =
                new StreamReader(lstrAppPath + Path.GetDirectoryName(Request.Url.AbsolutePath) +
                                 "\\Mailing\\index.html");

            var lstrEmailContent = lstrStreamReader.ReadToEnd()
                .Replace("[Meta]", lstrMeta.ToString())
                .Replace("[Bono]", lstrBono.ToString());

            lstrBody = lstrEmailContent;
            lstrBody = lstrBody.Replace("!#nombre#!", lstrNombre);

            lstrSubject = "Confirmación de inscripción";
            mEnviarCorreo(lstrFromEmail, lstrFromName, lstrToEmail, lstrToName, lstrSubject, lstrBody, true);
        }
        catch (Exception)
        {
        }
    }

    public void mEnviarCorreo(string pstrCorreoRemitente,
        string pstrNombreRemitente,
        string pstrCorreoDestinatario,
        string pstrNombreDestinatario,
        string pstrAsunto,
        string pstrMensaje,
        bool pboolEsHtml)
    {

        var lstmpCorreo = new SmtpClient();
        var lmsgMensaje = new MailMessage();
        var laddCorreoRemitente = new MailAddress(pstrCorreoRemitente, pstrNombreRemitente);
        var laddCorreoDestinatario = new MailAddress(pstrCorreoDestinatario, pstrNombreDestinatario);
        lmsgMensaje.From = laddCorreoRemitente;
        lmsgMensaje.To.Add(laddCorreoDestinatario);
        lmsgMensaje.Subject = pstrAsunto;
        lmsgMensaje.Body = pstrMensaje;
        lmsgMensaje.IsBodyHtml = pboolEsHtml;
        lmsgMensaje.Priority = MailPriority.Normal;
        lmsgMensaje.BodyEncoding = Encoding.UTF8;
        lstmpCorreo.Host = Convert.ToString(ConfigurationManager.AppSettings["CorreoHost"]);
        lstmpCorreo.Port = Convert.ToInt32(ConfigurationManager.AppSettings["CorreoPuerto"]);

        var cred = new System.Net.NetworkCredential(Convert.ToString(ConfigurationManager.AppSettings["CorreoUser"]),
            Convert.ToString(ConfigurationManager.AppSettings["CorreoPassword"]));
        lstmpCorreo.UseDefaultCredentials = false;
        lstmpCorreo.DeliveryMethod = SmtpDeliveryMethod.Network;
        lstmpCorreo.EnableSsl = false;
        lstmpCorreo.Credentials = cred;

        lstmpCorreo.Send(lmsgMensaje);

    }


    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        mGrabarRegistro();
    }
}