﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="quepuedocanjear.aspx.cs" Inherits="comocanjeokms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

	<script type="text/javascript">
	    //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
	    Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.Content header h1', { fontFamily: 'Flexo-Bold' });
	</script>	

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

	<section class="Content contentb"  style="padding-left: 40px;">
		<header style="height:34px;">
            <h1 tabindex="7" title="¿Qué puedo canjear?">¿Qué puedo canjear?</h1>
            <div class="div_regresa"><a class="promobtn" href="Default.aspx" tabindex="8" title="Regresar">Regresar</a></div>
		</header>
		
		<section class="texto" tabindex="8" title="Podrás canjear tus kilómetros por viajes nacionales e internaciones y/o productos del Catálogo LANPASS. Los montos en kilómetros dependerán del destino y/o producto que elijas. Para ver el catálogo ingresa www.latam.com/es_pe/latam-pass/.">
			<hr>
				<p class="fixwidth"><span class="mediumitalic14dark">Podrás canjear tus kms. por viajes nacionales e internaciones y/o productos del Catálogo LANPASS.  Los montos en kms. dependerán del destino y/o producto que elijas.</span></p>
				<ul>
					<li><span class="mediumitalic14dark">Viajes :</span>
						<ul>
							<li><span class="mediumitalic14dark">Dentro del País</span>
							<span class="lightitalic14dark"><br />Desde 10,000 KMS. LANPASS</span>
							</li>
							<li><span class="mediumitalic14dark">Chile y Argentina</span>
								<span class="lightitalic14dark"><br />Desde 26, 000 KMS. LANPASS</span>	
							</li>
							<li><span class="mediumitalic14dark">Bolivia, Brasil, Colombia, Venezuela y Ecuador</span>
								<span class="lightitalic14dark"><br />Desde 28, 000 KMS. LANPASS</span>
							</li>
							<li><span class="mediumitalic14dark">Norteamérica, México y el Caribe</span>
								<span class="lightitalic14dark"><br />Desde 48,000 KMS. LANPASS</span>	
							</li>
							<li><span class="mediumitalic14dark">Europa</span>
								<span class="lightitalic14dark"><br />Desde 120,000 KMS. LANPASS</span>
							</li>
						</ul>
					</li>
					<li><span class="mediumitalic14dark">Catálogo LANPASS:</span>
						<p class="fixwidth"><span class="lightitalic14dark">Podrás canjear productos del <a class="lightitalic14dark" href="https://www.latam.com/es_pe/latam-pass/" target="_blank">Catálogo Lanpass</a> desde 5,000 KMS. LANPASS.</span></p>
					</li>
				</ul>
		</section>
		<section class="image" style="padding-top: 10px; margin-top:75px;">
				<img src="images/maleta.jpg" width="311" height="351" alt="">
		</section>
		<div class="clear"></div>
	</section>

    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Canjeo").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/que_canjeo']);
    </script>
</asp:Content>