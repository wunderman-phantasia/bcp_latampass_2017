﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="masinformacion.aspx.cs" Inherits="masinformacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.contentwrap header h1, .Content header h1', { fontFamily: 'Flexo-Bold' });
        Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.credito header h1, .debito header h1', { fontFamily: 'Flexo-Regular' });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <section class="Content contentb">
        <header tabindex="7" title="Nuestras Tarjetas" style="height: 34px;">
            <h1>Nuestras Tarjetas</h1>
            <div class="div_regresa"><a class="promobtn" href="default.aspx" tabindex="8" title="Regresar">Regresar</a></div>
        </header>

        <style>
            .tarjetas h1 {
                text-align: center;
                font-size: 15px;
                margin-bottom: 5px;
            }

            .tarjetas h2 {
                text-align: center;
                font-size: 20px;
            }
        </style>
        <div class="tarjetas" style="overflow:hidden; height:auto" >
            <div style="width: 30%; float: left;" >
                <div style="width: 100%; height: 122px; text-align: center;"><h2 style="margin-top: 65px;"><a href="tarjetacreditovisa.aspx" style=" color:black; text-decoration:none">Visa</a></h2></div>
                <div style="width: 100%; height: 122px; text-align: center;"><h2 style="margin-top: 11px;"><a href="tarjetaamex.aspx" style="color:black; text-decoration:none">American Express</a></h2></div>
                <div style="width: 100%; height: 73px; text-align: center;"><h2 style="margin-top: -4px;"><a href="tarjetacredimas.aspx" style=" color:black; text-decoration:none">Débito</a></h2></div>
            </div>
            
            <div style="width: 70%; float: left;">
                <div style="width: 25%; float: left; margin-top: 10px;">
                    <h1>Clásica</h1>
                    <img src="images/visacard/visacard_clasiccard.png" width="151" height="95" alt="" />
                </div>
                <div style="width: 25%; float: left; margin-top: 10px;">
                    <h1>Oro</h1>
                    <img src="images/visacard/visacard_goldcard.png" width="151" height="95" alt="">
                </div>
                <div style="width: 25%; float: left; margin-top: 10px;">
                    <h1>Platinum</h1>
                    <img src="images/visacard/visacard_platinumcard.png" width="151" height="95" alt="">
                </div>
                <div style="width: 25%; float: left; margin-top: 10px;">
                    <h1>Signature</h1>
                    <img src="images/visacard/visacard_signedcard.png" width="151" height="95" alt="">
                </div>
                <div style="width: 25%; float: left;margin-top: 10px;">
                    <h1>Clásica</h1>
                    <img src="images/amexcard/amexcard_clasiccard.png" width="151" height="95" alt="" />
                </div>
                <div style="width: 25%; float: left;margin-top: 10px;">
                    <h1>Oro</h1>
                    <img src="images/amexcard/amexcard_goldcard.png" width="151" height="95" alt="">
                </div>
                <div style="width: 25%; float: left;margin-top: 10px;">
                    <h1>Platinum</h1>
                    <img src="images/amexcard/amexcard_platinumcard.png" width="151" height="95" alt="">
                </div>
                <div style="width: 25%; float: left; visibility: hidden;">
                    <h1>Signature</h1>
                    <img src="images/visacard/visacard_signedcard.png" width="151" height="95" alt="">
                </div>
                <div style="content: ' '; display:block;"></div>
                <div>
                    <div style="width: 54%; float: left;  margin-top: 25px;"">
                        <img src="images/credimas/visadebit_credimascard.png" width="151" height="95" alt="" />
                    </div>
                    <div style="width: 36%; float: left;  margin-top: 10px; display:none">
                        <h1 style="margin-left:52px">Oro</h1>
                        <img src="images/visacard/visacard_goldcard.png" width="151" height="95" alt="">
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <header style="height: 34px;">
            <h1 tabindex="7" title="¿Qué es LATAM Pass?">¿Qué es LATAM Pass?</h1>
            <div class="div_regresa"><a class="btnpideloaqui" href="formulario.aspx" tabindex="8" title="Pídela Aquí">¡Pídela aquí!</a></div>
        </header>
        <section class="texto" tabindex="8" title="Es el Programa de Pasajero Frecuente de LATAM, creado para premiar la preferencia y lealtad de sus pasajeros, los cuales podrán acumular kilómetros LATAM para canjearlos por dos beneficios: Pasajes a más de 950 destinos alrededor del mundo. Válidos para rutas LATAM, TAM, aerolíneas oneworld o líneas aéreas asociadas. Productos del catálogo BCP LATAM Pass.">
            <hr>
            <p class="fixwidth">
                <span class="lightitalic16dark">Es el </span><span class="mediumitalic16dark">Programa de Pasajero Frecuente de LATAM Airlines, </span><span class="lightitalic16dark">creado para premiar la preferencia y lealtad de sus pasajeros, los cuales podrán acumular </span><span class="mediumitalic16dark">KMS. LATAM Pass </span><span class="lightitalic16dark">para canjearlos por:</span>
            </p>
            <p class="fixwidth">
                <img src="images/whatislanpass_airplaneicon.png" width="21" height="18" alt="" />
                <span class="lightitalic16dark">Pasajes a más de </span>
                <span class="mediumitalic16dark">950 destinos alrededor del mundo.*</span>
            </p>
            <p class="fixwidth">
                <img src="images/whatislanpass_gifticon.png" width="21" height="22" alt="" />
                <span class="lightitalic16dark">Productos del </span>
                <span class="mediumitalic16dark">catálogo LATAM Pass.</span>
            </p>
            <hr>
            <p class="fixwidth">
                <span class="lightitalic11dark">*Válido para rutas LATAM Airlines aerolíneas </span><span class="mediumitalic11dark">one</span><span class="lightitalic11dark">world o líneas aéreas asociadas.</span>
            </p>
        </section>
        <section class="image" style="margin-top: 55px;">
            <img src="images/whatislanpass_image.png" width="300" height="239" alt="">
        </section>
        <div class="clear"></div>
        <header style="height: 34px;">
            <h1 tabindex="7" title="¿Cómo acumulo kms.?">¿Cómo acumulo kms.?</h1>
            <div class="div_regresa"><a class="btnpideloaqui" href="formulario.aspx" tabindex="8" title="Pídela Aquí">¡Pídela aquí!</a></div>
        </header>
        <section class="texto" tabindex="8" title="Los socios LATAM Pass podrán acumular kilómetros comprando con sus Tarjetas de Crédito BCP LATAM Pass, viajando en LAN, TAM, líneas aéreas líneas aéreas oneworld o líneas aéreas asociadas al programa, adquiriendo productos o servicios de los partners de LATAM Pass como: REPSOL, Pacífico Seguros, Los Portales, Yamaha, Divemotor, Bonus, Casinelli e Imagina. Además de las promociones especiales de acumulación con LATAM Pass o con las distintas empresas asociadas a LATAM Pass.">
            <hr>
            <p class="fixwidth"><span class="mediumitalic16dark">Los socios LATAM Pass podrán acumular kms:</span></p>
            <p class="fixwidth">
                <img src="images/acumulatekms_cardicon.png" width="18" height="12" alt="" />
                <span class="lightitalic14dark">Comprando con su </span><span class="mediumitalic14dark">Tarjeta de Crédito o Débito BCP LATAM Pass.</span>
            </p>
            <p class="fixwidth">
                <img src="images/acumulatekms_planeicon.png" width="18" height="17" alt="" />
                <span class="lightitalic14dark">Viajando en </span><span class="mediumitalic14dark">LATAM Airlines, </span><span class="lightitalic14dark">líneas aéreas </span><span class="mediumitalic14dark">oneworld</span><span class="lightitalic14dark"> o en líneas aéreas asociadas al programa.</span>
            </p>
            <p class="fixwidth">
                <img src="images/acumulatekms_checkicon.png" width="16" height="16" alt="" />
                <span class="lightitalic14dark">Adquiriendo productos o servicios de la red de comercios asociados como: <span class="mediumitalic14dark">Cabify, Martinizing, 
       Rocketmiles, iShop, Büro, Delhel, Imagina, V&V Grupo Inmobiliario, Autoland- KIA</span> y otros que puedes 
      revisar <a class="lightitalic14dark" href="https://www.latam.com/es_pe/latam-pass/como-acumular-mas-kms/comercios-asociados/">aquí</a>.</span>
            </p>
            <hr>
            <p class="lightitalic11dark fixwidth"><span>Además de las promociones especiales de acumulación con LATAM Pass o con las distintas empresas asociadas a LATAM Pass.</span></p>
        </section>
        <section class="image image-km" style="float: right; margin-top: 55px; right: 0px !important">
            <img src="images/acumulatekms_imagebag.png" width="309" height="313" alt="">
        </section>
        <div class="clear"></div>
        <header style="height: 34px;">
            <h1 tabindex="7" title="¿Qué puedo canjear?">¿Qué puedo canjear?</h1>
            <div class="div_regresa"><a class="btnpideloaqui" href="formulario.aspx" tabindex="8" title="Pídela Aquí">¡Pídela aquí!</a></div>
        </header>
        <section class="texto" tabindex="8" title="Podrás canjear tus kilómetros por viajes nacionales e internaciones y/o productos del Catálogo LATAM Pass. Los montos en kilómetros dependerán del destino y/o producto que elijas. Para ver el catálogo ingresa www.latam.com/es_pe/latam-pass/.">
            <hr>
            <p class="fixwidth"><span class="mediumitalic14dark">Podrás canjear tus kms. por viajes nacionales e internaciones y/o productos del Catálogo LATAM Pass.  Los montos en kms. dependerán del destino y/o producto que elijas.</span></p>
            <ul>
                <li><span class="mediumitalic14dark">Viajes :</span>
                    <ul>
                        <li><span class="mediumitalic14dark">Dentro del País</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 10,000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">Chile</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 22, 000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">Argentina</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 32,000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">Ecuador</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 22,000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">Brasil</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 28,000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">Colombia</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 24,000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">México</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 28,000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">Norteamérica y el Caribe</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 48,000 KMS. LATAM Pass</span>
                        </li>
                        <li><span class="mediumitalic14dark">Europa</span>
                            <span class="lightitalic14dark">
                                <br />
                                Desde 120,000 KMS. LATAM Pass</span>
                        </li>
                    </ul>
                </li>
                <li><span class="mediumitalic14dark">Catálogo LATAM Pass:</span>
                    <p class="fixwidth"><span class="lightitalic14dark">Podrás canjear productos del <a class="lightitalic14dark" href="http://www.lan.com/catalogolatampassperu/" target="_blank">Catálogo LATAM Pass</a> desde 4,900 KMS. LATAM Pass.</span><br />
                     <span class="lightitalic14dark">Para más información haz clic <a class="lightitalic14dark" href="http://eligetutarjetabcp.dev.wundermanphantasia.pe/Views/PreguntasFrecuentes.aspx" target="_blank">aquí</a>.</span></p>
                </li>
            </ul>
        </section>
        <section class="image" style="padding-top: 10px; margin-top: 75px;">
            <img src="images/maleta.jpg" width="311" height="351" alt="">
        </section>
        <div class="clear"></div>
    </section>
    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#MasInformacion").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/nuestras_tarjetas']);
    </script>
</asp:Content>
