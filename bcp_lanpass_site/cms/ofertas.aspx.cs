﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using DA;

public partial class ofertas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Validacion"] != null)
        {
            
        }
        else Response.Redirect("Default.aspx");
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        using (DataClsLanPass db = new DataClsLanPass())
        {
            List<PA_LANPASS_CMS_CONSULTAR_OFERTASResult> lista = db.PA_LANPASS_CMS_CONSULTAR_OFERTAS(txtBNombres.Value, txtBRegIni.Text, txtBRegFin.Text).ToList();
            GridView.PageSize = int.Parse(txtBPagina.Text);
            GridView.DataSource = lista;
            GridView.DataBind();
            if (lista.Count == 0)
            {
                MostrarMensaje("No se han encontrado registros con los criterios de búsqueda seleccionados.");
            }
            else
            {
                updt_mensaje.Update();
                Session["reporte"] = lista;
                mensaje.Visible = false;
            }
        }
    }
    protected void MostrarMensaje(string text)
    {
        mensaje.Visible = true;
        lblEstado.Text = text;
        updt_mensaje.Update();
    }
    protected void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int id = Convert.ToInt32(e.CommandArgument);
        switch (e.CommandName)
        {
            /*case "Excel":
                Response.Redirect("export_excel.aspx?rep=3");
                break;*/
            case "Agregar":
                spn_popup_titulo.InnerText = "NUEVA OFERTA";
                Limpiar_Datos();
                txtCodigo.Enabled = true;
                chkEstado.Checked = true;
                chkEstado.Enabled = false;
                PopUp.Show();
                hf_emp.Value = id.ToString();
                updt_PopUp.Update();
                break;
            case "Editar":
                spn_popup_titulo.InnerText = "EDITAR OFERTA: ID " + id;
                BLOferta bl = new BLOferta();
                tbl_lan_oferta obj = bl.Datos_Oferta(id);
                chkEstado.Checked = obj.ofe_bit_activo;
                chkEstado.Enabled = true;
                txtCodigo.Text = obj.ofe_str_codigo;
                txtCodigo.Enabled = false;
                txtNombre.Text = obj.ofe_str_titulo;
                txtDescripcion.Text = obj.ofe_str_descripcion;
                txtAnexo.Text = obj.ofe_str_subdescripcion;
                PopUp.Show();
                hf_emp.Value = id.ToString();
                updt_PopUp.Update();
                break;
        }
    }
    protected void Limpiar_Datos()
    {
        txtCodigo.Text = "";
        txtNombre.Text = "";
        txtDescripcion.Text = "";
        txtAnexo.Text = "";
        chkEstado.Checked = true;
        spn_error.InnerText = "";
    }
    protected void GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView.PageIndex = e.NewPageIndex;
        btnBuscar_Click(null, null);
    }
    protected void btn_Grabar_Click(object sender, EventArgs e)
    {
        int id = new BLOferta().Grabar_Oferta(int.Parse(hf_emp.Value), txtCodigo.Text, txtNombre.Text, txtDescripcion.Text, txtAnexo.Text, chkEstado.Checked);
        if (id == 0) { spn_error.InnerText = "No se pudo grabar el registro. Verífique haber llenado todos los campos (*)"; updt_PopUp.Update(); }
        else
        {
            PopUp.Hide();
            btnBuscar_Click(null, null);
            updt_Principal.Update();
        }
    }
}