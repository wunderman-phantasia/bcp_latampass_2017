﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cms/MasterPage.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="cms_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<h1>Sitio Administrativo</h1>
<br />
    Desde este panel de control usted podrá gestionar el contenido de su portal web.<br /><br />
    <ul>
        <li>Administrador de Usuarios: <i>Podrá registrar, actualizar o inactivar usuarios administradores.</i></li><br />
        <li>Clientes: <i>Podrá visualizar a todos los clientes registrados.</i></li><br />
        <li>Consultas: <i>Podrá visualizar a todos los clientes no registrados que consultaron por promociones.</i></li><br />
        <li>Ofertas: <i>Podrá registrar, actualizar o inactivar ofertas.</i></li><br />
        <li>Ofertas de Clientes: <i>Visualizará las ofertas para los clientes, también podrá importar nuevas ofertas.</i></li><br />
    </ul>
</asp:Content>

