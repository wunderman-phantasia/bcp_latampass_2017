﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cms/MasterPage.master" AutoEventWireup="true" CodeFile="clientes.aspx.cs" Inherits="clientes" StylesheetTheme="skin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Import Namespace="DA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <h1>Listado de Clientes</h1>

    <form action="" id="frmDashboard" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true" EnableScriptGlobalization="true">
        </asp:ScriptManager>
        <div class="Filtros">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <label class="search_panel" for="txtBNombres">Nombres:</label>
                        <input type="text" id="txtBNombres" runat="server" style="width: 221px" />
                    </td>
                    <td>
                        <label class="search_panel" for="txtNDocumento">Documento:</label>
                        <input type="text" id="txtNDocumento" runat="server" style="width: 100px" />
                    </td>
                    <td>
                        <label class="search_panel" for="fechaini">F. Registro del: </label>
                        <asp:TextBox ID="txtBRegIni" runat="server" Width="75px" ClientIDMode="Static"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtBRegIni" runat="server" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </td>
                    <td>
                        <label class="search_panel" for="fechafin">al</label>
                        <asp:TextBox ID="txtBRegFin" runat="server" Width="75px" ClientIDMode="Static"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtBRegFin" runat="server" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </td>
                    <td>
                        <label class="search_panel" for="correo">Páginas:</label>
                        <asp:TextBox ID="txtBPagina" runat="server" Text="20" Width="25px" MaxLength="3" class="Numero" onkeypress="return SoloNumerico(event)" onblur="var pag = document.getElementById('MainContent_txtBPagina'); if(pag.value == '' || pag.value == 0){pag.value='20';}"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                    </td>
                </tr>
            </table>
            <!-- ***************Aquí va el recuadro de mensajes de la aplicación ******************************-->



            <asp:UpdatePanel ID="updt_mensaje" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="mensaje" runat="server" visible="false" style="padding-top: 10px; padding-bottom: 10px;" clientidmode="Static">
                        <table id="Table1" border="0" style="background-color: White; border: 1px solid black;" width="98%" cellpadding="0" cellspacing="0">
                            <tr style="background-color: #FFFFCC">
                                <td align="left" style="border-left: 0 solid #000000; width: 20px; height: 15px;"></td>
                                <td colspan="8" align="left">
                                    <asp:Label ID="lblEstado" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="right"></td>
                                <td width="5px">
                                    <img src="images/btn_close.png" width="16" height="16" onclick="Display('mensaje',false);" class="link" /></td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="clear"></div>

        <div id="reporte" runat="server" clientidmode="Static">

            <div class="cntDownload" style="margin: 10px 0;">
                <asp:Button ID="btnExport" runat="server" Text="Descargar Lista" OnClick="btnExportCSV_Click" />
                <asp:Button ID="btnExportRetoGanadiario" runat="server" Text="Descargar Reto Ganadiario Latam Pass Fase I" OnClick="btnExportRetoGanadiario_Click" BackColor="Blue" ForeColor="White"/>
                <asp:Button ID="btnExportRetoGanadiarioF2" runat="server" Text="Descargar Reto Ganadiario Latam Pass Fase II" OnClick="btnExportRetoGanadiarioF2_Click" BackColor="Blue" ForeColor="White"/>

            </div>

            <asp:UpdatePanel ID="UpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpdateProgress" runat="server">
                        <ProgressTemplate>
                            <div>Cargando data...</div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:GridView ID="GridView"
                        HeaderStyle-BackColor="#284775"
                        HeaderStyle-ForeColor="White"
                        RowStyle-BackColor="#DDDDDD"
                        AlternatingRowStyle-BackColor="White"
                        AlternatingRowStyle-ForeColor="#000"
                        runat="server"
                        AutoGenerateColumns="false"
                        AllowPaging="true"
                        OnPageIndexChanging="GridView_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                <ItemTemplate>
                                    <asp:Label ID="label0" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).ID%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cliente">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label1" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Cliente%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Teléfono">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label2" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Telefono%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo Documento">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label3" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Tipo_Documento%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nro Documento">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label4" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Nro_Documento%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label5" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).E_mail%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cta Sueldo">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label6" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Cta_Sueldo%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sueldo">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label7" runat="server" Text='<%# (((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Sueldo / 1m).ToString("N2")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Moneda">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label8" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Moneda.ToUpper()%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tarjeta">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label9" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Tarjeta%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Registro">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label10" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Fecha_Registro%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IP">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label11" runat="server" Text='<%# ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).IP%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UTM Source">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <ItemTemplate>
                                    <asp:Label ID="label12" runat="server" Text='<%# string.IsNullOrEmpty(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Source) ? "none" : ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Source%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UTM Medium">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label13" runat="server" Text='<%# string.IsNullOrEmpty(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Medium) ? "none" : ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Medium%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UTM Campaign">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label14" runat="server" Text='<%# string.IsNullOrEmpty(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Campaign) ? "none" : ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Campaign%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Diseño">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="label15" runat="server" Font-Size="10px" Text='<%# string.IsNullOrEmpty(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).Diseno) ? "none" : ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).Diseno%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings NextPageText="" PreviousPageText="" FirstPageText="&lt;&lt; Primero" LastPageText="&gt;&gt; Último" PageButtonCount="10" Mode="NumericFirstLast" />
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="GridView" EventName="PageIndexChanging"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>

    </form>
</asp:Content>

