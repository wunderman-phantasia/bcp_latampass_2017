﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

public partial class cms_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpContext.Current.Session["Validacion"] = null;
            Generar_Captcha();
        }
    }
    [WebMethod(EnableSession = true)]
    public static string Generar_Captcha()
    {
        HttpContext.Current.Session["Captcha"] = Guid.NewGuid().ToString().Substring(0, Convert.ToInt32(ConfigurationManager.AppSettings["Captcha_caracteres"]));
        return null;
    }
    protected void btnReGenerate_Click(object sender, EventArgs e)
    {
        Generar_Captcha();
    }
    [WebMethod(EnableSession = true)]
    public static int ValidarUsuario(string usuario, string clave,string captcha)
    {
        if (HttpContext.Current.Session["Captcha"] != null)
        {
            string hash = BitConverter.ToString(new SHA512Managed().ComputeHash(new ASCIIEncoding().GetBytes(captcha))).Replace("-", "");
            if (hash == HttpContext.Current.Session["Captcha"].ToString())
            {
                hash = BitConverter.ToString(new SHA512Managed().ComputeHash(new ASCIIEncoding().GetBytes(clave))).Replace("-", "");
                BEUsuario resp = new BLAdministrador().ValidarAdministrador(usuario, hash);
                if (resp != null)
                {
                    if (resp.id == 0) return 2;
                    HttpContext.Current.Session["Validacion"] = resp;
                    return 1;
                }
                return 0;
            }
            return 3;
        }
        return 3;
    }
}