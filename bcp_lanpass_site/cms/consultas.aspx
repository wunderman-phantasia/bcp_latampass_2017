﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cms/MasterPage.master" AutoEventWireup="true" CodeFile="consultas.aspx.cs" Inherits="clientesnoregistrados"  StyleSheetTheme="skin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<!-- @001 LAG 12/02/2014 Se agregaron validaciones para el control txtBPagina en sus métodos onkeypress y onblur -->
	<h1>Listado de Consultas de Ofertas</h1>

	<form action="" id="frmDashboard" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true" EnableScriptGlobalization="true" >
        </asp:ScriptManager>
		<div class="Filtros">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><label for="correo">Documento:</label>
                        <input type="text" id="txtBNombres" runat="server" style="width:100px"/>
                    </td>          
                    <td><label for="fechaini">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha consulta del: </label>
                        <asp:TextBox ID="txtBRegIni" runat="server" Width="75px" ClientIDMode="Static"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtBRegIni" runat="server" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                    </td>
                    <td>
                        <label for="fechafin">al&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <asp:TextBox ID="txtBRegFin" runat="server" Width="75px" ClientIDMode="Static"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtBRegFin" runat="server" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                    </td>   
                    <td><label for="correo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Paginación:</label><asp:TextBox ID="txtBPagina" runat="server" Text="20" Width="25px" MaxLength="3" class="Numero" onkeypress="return SoloNumerico(event)" onblur="var pag = document.getElementById('MainContent_txtBPagina'); if(pag.value == '' || pag.value == 0){pag.value='20';}"></asp:TextBox></td>
                    <td>
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                            onclick="btnBuscar_Click" />
                    </td>                        
                </tr>
            </table>
            <!-- ***************Aquí va el recuadro de mensajes de la aplicación ******************************-->    
            <asp:UpdatePanel ID="updt_mensaje" runat="server" UpdateMode="Conditional">
                <ContentTemplate>    
                    <div id="mensaje" runat="server" visible="false" style="padding-top:10px;padding-bottom:10px;" clientidmode="Static">
                        <table id="Table1" border="0" style="background-color:White;border: 1px solid black;" width="98%" cellpadding="0" cellspacing="0">
                            <tr style="background-color: #FFFFCC">
                                <td align="left" style="border-left: 0 solid #000000;width:20px;height:15px;"></td>
                                <td colspan="8" align="left">
                                    <asp:Label ID="lblEstado" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="right"></td>
                                <td width="5px"><img src="images/btn_close.png" width="16" height="16" onclick="Display('mensaje',false);" class="link"/></td>
                            </tr>               
                        </table>
                    </div>   
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="clear"></div>
        
            <asp:GridView ID="GridView" runat="server" GridLines="Horizontal" ShowHeaderWhenEmpty="true"
                AllowPaging="True" SkinID="Professional"
                ShowFooter="True" AutoGenerateColumns="True" 
                PageSize="3" OnRowCommand="GridView_RowCommand"
                onpageindexchanging="GridView_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="35px">
                        <HeaderTemplate>
                            <asp:HyperLink ID="LinkButton1" runat="server" NavigateUrl="export_excel.aspx?rep=5">
                                <asp:Image ID="Image1" ImageUrl="images/excel_ico.gif" Width="16" Height="16" style="margin-top:4px;" runat="server"/>
                            </asp:HyperLink>
                        </HeaderTemplate>
                    </asp:TemplateField>
                
                </Columns>
                    <PagerSettings NextPageText=""
                        PreviousPageText="" FirstPageText="&lt;&lt; Primero" LastPageText="&gt;&gt; Último" 
                        PageButtonCount="10" Mode="NumericFirstLast" />
                    <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        
			
    </form>      
</asp:Content>

