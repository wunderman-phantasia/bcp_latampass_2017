﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cms_export_excel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Validacion"] != null)
        {
            GridView.DataSource = Session["reporte"];
            GridView.DataBind();
            //lblTotal.Text = GridView.Rows.Count.ToString();
            Lblfecha.InnerText = DateTime.Now.ToString();
        }
        else Response.Redirect("Default.aspx");
    }



    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();

        string NombreXLS = "";
        switch (Request["rep"])
        {
            case "1": NombreXLS = "Listado de Usuarios";
                break;
            case "2": NombreXLS = "Listado de Clientes";
                break;
            case "3": NombreXLS = "Listado de Ofertas";
                break;
            case "4": NombreXLS = "Listado de Ofertas por Cliente";
                break;
            case "5": NombreXLS = "Listado de Clientes No Registrados";
                break;
        }

        Response.AddHeader("content-disposition", "attachment; filename=" + NombreXLS + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";        
        EnableViewState = false;
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);
    }

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {     
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.CssClass = "textmode";
        }
    }
}