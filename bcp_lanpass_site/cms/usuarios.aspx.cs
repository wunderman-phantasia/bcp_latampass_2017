﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using DA;
using System.IO;

public partial class usuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Validacion"] != null)
        {

        }
        else Response.Redirect("Default.aspx");
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        using (DataClsLanPass db = new DataClsLanPass())
        {
            List<PA_LANPASS_CMS_CONSULTAR_USUARIOSResult> lista = db.PA_LANPASS_CMS_CONSULTAR_USUARIOS(txtBNombres.Value, txtBRegIni.Text, txtBRegFin.Text).ToList();
            GridView.PageSize = int.Parse(txtBPagina.Text);
            GridView.DataSource = lista;
            GridView.DataBind();
            if (lista.Count == 0)
            {
                MostrarMensaje("No se han encontrado registros con los criterios de búsqueda seleccionados.");
            }
            else
            {
                updt_mensaje.Update();
                Session["reporte"] = lista;
                mensaje.Visible = false;
            }
        }
    }
    protected void MostrarMensaje(string text)
    {
        mensaje.Visible = true;
        lblEstado.Text = text;
        updt_mensaje.Update();
    }
    protected void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int id = Convert.ToInt32(e.CommandArgument);
            switch (e.CommandName)
            {
                /*case "Excel":
                    Response.Redirect("export_excel.aspx?rep=1");
                    break;*/
                case "Agregar":
                    spn_popup_titulo.InnerText = "NUEVO ADMINISTRADOR";
                    Limpiar_Datos();
                    txtNombre.Enabled = true;
                    chkEstado.Checked = true;
                    chkEstado.Enabled = false;
                    PopUp.Show();
                    hf_emp.Value = id.ToString();
                    updt_PopUp.Update();
                    break;
                case "Editar":
                    spn_popup_titulo.InnerText = "EDITAR ADMINISTRADOR: ID " + id;
                    BLAdministrador bl = new BLAdministrador();
                    tbl_lan_administrador obj = bl.Datos_Administrador(id);
                    chkEstado.Checked = obj.adm_bit_activo;
                    chkEstado.Enabled = true;
                    txtClave.Text = "";
                    txtNombre.Text = obj.adm_str_nombre;
                    txtNombre.Enabled = false;
                    PopUp.Show();
                    hf_emp.Value = id.ToString();
                    updt_PopUp.Update();
                    break;
            }
        }
        catch (Exception ex)
        {
            int p = 0;
        }
    }
    protected void Limpiar_Datos()
    {
        txtNombre.Text = "";
        txtClave.Text = "";
        chkEstado.Checked = true;
        spn_error.InnerText = "";
    }
    protected void GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView.PageIndex = e.NewPageIndex;
        btnBuscar_Click(null, null);
    }
    protected void btn_Grabar_Click(object sender, EventArgs e)
    {
        int id = new BLAdministrador().Grabar_Administrador(int.Parse(hf_emp.Value), txtNombre.Text, txtClave.Text, chkEstado.Checked);
        if (id == 0) { spn_error.InnerText = "No se pudo grabar el registro. Verífique haber llenado todos los campos (*)"; updt_PopUp.Update(); }
        else if (id == -1) { spn_error.InnerText = "No se pudo grabar el registro. El usuario ya existe (*)"; updt_PopUp.Update(); }
        else
        {
            PopUp.Hide();
            btnBuscar_Click(null, null);
            updt_Principal.Update();
        }
    }
}