﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cms/MasterPage.master" AutoEventWireup="true" CodeFile="ofertas.aspx.cs" Inherits="ofertas"  StyleSheetTheme="skin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<!-- @001 LAG 12/02/2014 Se agregaron validaciones para el control txtBPagina en sus métodos onkeypress y onblur -->
	<h1>Listado de Ofertas</h1>

	<form action="" id="frmDashboard" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true" EnableScriptGlobalization="true" >
        </asp:ScriptManager>
		<div class="Filtros">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><label for="correo">Código:</label>
                        <input type="text" id="txtBNombres" runat="server" style="width:70px"/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                    </td>          
                    <td><label for="fechaini">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha registro del: </label>
                        <asp:TextBox ID="txtBRegIni" runat="server" Width="75px" ClientIDMode="Static"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtBRegIni" runat="server" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                    </td>
                    <td>
                        <label for="fechafin">al</label>
                        <asp:TextBox ID="txtBRegFin" runat="server" Width="75px" ClientIDMode="Static"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtBRegFin" runat="server" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                    </td>   
                    <td><label for="correo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Paginación:</label><asp:TextBox ID="txtBPagina" runat="server" Text="20" Width="25px" MaxLength="3" class="Numero" onkeypress="return SoloNumerico(event)" onblur="var pag = document.getElementById('MainContent_txtBPagina'); if(pag.value == '' || pag.value == 0){pag.value='20';}"></asp:TextBox></td>
                    <td>
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                            onclick="btnBuscar_Click" />
                    </td>                        
                </tr>
            </table>
            <!-- ***************Aquí va el recuadro de mensajes de la aplicación ******************************-->    
            <asp:UpdatePanel ID="updt_mensaje" runat="server" UpdateMode="Conditional">
                <ContentTemplate>    
                    <div id="mensaje" runat="server" visible="false" style="padding-top:10px;padding-bottom:10px;" clientidmode="Static">
                        <table id="Table1" border="0" style="background-color:White;border: 1px solid black;" width="98%" cellpadding="0" cellspacing="0">
                            <tr style="background-color: #FFFFCC">
                                <td align="left" style="border-left: 0 solid #000000;width:20px;height:15px;"></td>
                                <td colspan="8" align="left">
                                    <asp:Label ID="lblEstado" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="right"></td>
                                <td width="5px"><img src="images/btn_close.png" width="16" height="16" onclick="Display('mensaje',false);" class="link"/></td>
                            </tr>               
                        </table>
                    </div>   
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="clear"></div>
        <asp:UpdatePanel ID="updt_Principal" runat="server" UpdateMode="Conditional">
          <ContentTemplate>
            <asp:GridView ID="GridView" runat="server" GridLines="Horizontal" ShowHeaderWhenEmpty="true"
                AllowPaging="True" SkinID="Professional"
                ShowFooter="True" AutoGenerateColumns="True" 
                PageSize="3" OnRowCommand="GridView_RowCommand"
                onpageindexchanging="GridView_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="20px">
                        <HeaderTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="export_excel.aspx?rep=3">
                                <asp:Image ID="Image1" ImageUrl="images/excel_ico.gif" Width="16" Height="16" style="margin-top:4px;" runat="server"/>
                            </asp:HyperLink>
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="20px">
                        <HeaderTemplate>
                            <asp:LinkButton ID="ImageButton2" runat="server" CommandName="Agregar" CommandArgument='0'>
                                <asp:Image ID="Image2" ImageUrl="images/new.gif" Width="16" Height="16" style="margin-top:4px;" runat="server"/>
                            </asp:LinkButton>
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="35px">
                        <ItemTemplate>
                            <asp:LinkButton ID="ImageButton3" runat="server" CommandName="Editar" CommandArgument='<%# Bind("ID") %>'>
                                <asp:Image ID="Image3" ImageUrl="images/editar.png" Width="16" Height="16" runat="server"/>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                
                </Columns>
                    <PagerSettings NextPageText=""
                        PreviousPageText="" FirstPageText="&lt;&lt; Primero" LastPageText="&gt;&gt; Último" 
                        PageButtonCount="10" Mode="NumericFirstLast" />
                    <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
          </ContentTemplate>
          <Triggers>
              <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
          </Triggers>
        </asp:UpdatePanel>

      <!-- *************** Aquí va el popup para administración ***********************************-->
    <asp:Panel ID="pnl_PopUp" runat="server" CssClass="panel">
        <asp:UpdatePanel ID="updt_PopUp" runat="server" UpdateMode="Conditional">
         <ContentTemplate>
            <table width="100%">
              <tr>
                  <td class="panel_cab"><span id="spn_popup_titulo" runat="server"></span><span class="close" onclick="$find('PopUp').hide(); return false;">&nbsp;</span></td>
              </tr>
              <tr>
                <td style="padding:10px;">
                  <table cellpadding="0" cellspacing="3" width="550px">
                        <tr>
                            <td><label>Código (*): </label></td><td colspan="3"><asp:TextBox ID="txtCodigo" ClientIDMode="Static" MaxLength="5" runat="server" width="100px"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td><label>Título (*): </label></td><td colspan="3"><asp:TextBox ID="txtNombre" ClientIDMode="Static" MaxLength="250" TextMode="MultiLine" Rows="2" runat="server" width="400px"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td><label>Descripción (*): </label></td><td colspan="3"><asp:TextBox ID="txtDescripcion" ClientIDMode="Static" MaxLength="1000" TextMode="MultiLine" Rows="4" runat="server" width="400px"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td><label>Anexo (*): </label></td><td colspan="3"><asp:TextBox ID="txtAnexo" ClientIDMode="Static" MaxLength="1000" TextMode="MultiLine" Rows="4" runat="server" width="400px"></asp:TextBox> </td>
                        </tr>
                        <tr>
                            <td><label>Estado: </label></td><td colspan="3"><asp:CheckBox ID="chkEstado" runat="server" Checked="true" /> </td>
                        </tr>
                        <tr>
                            <td colspan="4"><label>Notas:<br />&nbsp;Usar la sentencia <i>@vencimiento</i> para agregar la fecha de vencimiento en el Anexo</label></td></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="Botones_final"><span id="spn_error" runat="server" class="error" clientidmode="Static"></span></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="Botones_final">
                                <asp:Button ID="btn_Grabar" runat="server" Text="Grabar" EnableTheming="true" onclick="btn_Grabar_Click" OnClientClick="return ValidarDatosOferta();" />&nbsp;&nbsp;
                                <asp:Button ID="btn_Cancelar" OnClientClick="$find('PopUp').hide(); return false;" runat="server" Text="Cancelar" EnableTheming="true"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
          </table>
          <asp:HiddenField ID="hf_emp" runat="server" Value='0' />
         </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
      
   <asp:ModalPopupExtender ID="PopUp" BehaviorID="PopUp" PopupControlID="pnl_PopUp" runat="server" 
   TargetControlID="hfAux" BackgroundCssClass="modalBackground">
   </asp:ModalPopupExtender>
   <asp:HiddenField ID="hfAux" runat="server" />

			
    </form>      
</asp:Content>

