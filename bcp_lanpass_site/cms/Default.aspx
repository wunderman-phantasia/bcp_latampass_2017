﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="cms_Default" EnableViewState="false" %>

<!doctype html>
<html lang="es">
<head id="Head1" runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1280,  initial-scale=0.2">
    <title>BCP - LANPASS - CMS</title>

    <link rel="stylesheet" href="Styles/reset.css" type="text/css" />
    <link rel="stylesheet" href="Styles/style.css" type="text/css" />
    <link rel="stylesheet" href="Styles/fonts.css" type="text/css" />

    <!--[if lt IE 9]>
	<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/style-ie.css">
	<![endif]-->

    <!--[if lt IE 7]>
	<link rel="stylesheet" href="css/style-ie6.css">
	<![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/cufon-yui.js"></script>
    <script src="js/Flexo-Regular.font.js"></script>
    <script src="js/Flexo-Light.font.js"></script>
    <script src="js/Flexo-Bold.font.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="js/JScript_Login.js" type="text/javascript"></script>

</head>
<body>
    <form action="dashboard.aspx" id="frmLogin" runat="server" clientidmode="Static" onsubmit="ValidarUsuario();return false;">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <div class="wrap">
            <section class="headContent">
                <div class="contentwrap">
                    <header>
                        <h1>Acceso al Administrador de Contenidos</h1>
                    </header>
                </div>
            </section>
        </div>
        <nav class="navbar">
        </nav>
        <div class="clear"></div>
        <div class="Content">
            <div class="loginform">
                <fieldset>
                    <label for="username">
                        <div>
                            <span class="italic14light">Usuario: </span>
                        </div>
                        <div class="clear"></div>
                    </label>
                    <div class="clear"></div>
                    <input data-id="1" class="forminputtext" type="text" id="username" name="username" maxlength="15">
                    <label for="username" class="error" style="position: absolute"></label>
                </fieldset>
                <fieldset>
                    <label for="password">
                        <div style="padding-top: 12px;"><span class="italic14light">Contraseña: </span></div>
                        <div class="clear"></div>
                    </label>
                    <input data-id="2" class="forminputtext" type="password" id="password" name="password" maxlength="20">
                    <label for="password" class="error" style="position: absolute">*Debes ingresar la contraseña</label>
                    <br />
                </fieldset>
                <fieldset>
                    <label for="captcha">
                        <div style="padding-top: 12px;">
                            <span class="italic14light">Captcha: </span>
                            <br />
                            <img id="img_captcha" src="../SecureCaptcha.ashx" />
                        </div>
                        <div class="clear"></div>
                    </label>
                    <input data-id="3" class="forminputtext" type="text" id="captcha" name="captcha" maxlength="10" style="width: 100px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span onclick="CambiarCaptcha()" style="cursor: pointer">
                        <img src="images/recur.png" width="16px" height="16px" />Cambiar Imagen</span>
                    <label for="captcha" class="error" style="position: absolute">*Debes ingresar el código captcha</label>
                    <br />
                </fieldset>
                <fieldset>
                    <input type="submit" value="Ingresar" class="btnsubmit" /><br />
                    <br />
                    <label id="submit_error">*Error</label><br />
                </fieldset>
            </div>
        </div>
    </form>
</body>
</html>
