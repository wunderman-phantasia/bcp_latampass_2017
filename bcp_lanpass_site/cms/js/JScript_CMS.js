﻿/* @001 LAG 12/02/2014 Se agregó el método SoloNumerico(e) */
function Display(id, value) { if (value) document.getElementById(id).style.display = ''; else document.getElementById(id).style.display = 'none'; }
function SoloAlfanumerico(e) { var tecla = (document.all) ? e.keyCode : e.which; var patron = /[A-Za-z0-9]/; te = String.fromCharCode(tecla); if (!patron.test(te)) { (document.all) ? e.keyCode = 0 : e.which = 0; return false; } else return true; }
function SoloNumerico(e) { var tecla = (document.all) ? e.keyCode : e.which;if (tecla == 0 || tecla == 8) return true; var patron = /[0-9]/; te = String.fromCharCode(tecla); if (!patron.test(te)) { (document.all) ? e.keyCode = 0 : e.which = 0; return false; } else return true; }
function ValidarDatosUsuario() {
    if ($("#txtNombre").val().length <= 8){ $("#spn_error").html("Debe ingresar un usuario con más de 8 caracteres"); return false;}
    else if ($("#txtClave").val().length <= 8){ $("#spn_error").html("Debe ingresar una contraseña con más de 8 caracteres"); return false;}
    $("#spn_error").html(""); return true;
}
function ValidarDatosOferta() {
    if ($("#txtNombre").val() != "" && $("#txtAlias").val() != "" && $("#txtLink").val() != "") { $("#spn_error").html(""); return true; }
    else { $("#spn_error").html("Debe ingresar todos los campos (*)"); return false; }
}