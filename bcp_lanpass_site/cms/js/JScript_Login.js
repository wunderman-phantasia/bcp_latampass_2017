﻿$(document).on("ready", function () {
    
    $('input[name=username], input[name=password]').keyup(function () {
        //var $th = $(this);
        //$th.val($th.val().replace(/[^a-zA-Z0-9]/g, function (str) { return ''; }));
    });

    jQuery.validator.setDefaults({
        debug: false
    });

    $.validator.addMethod('letters', function (value) {
        return value.match(/^[a-zA-ZáéóúÁÉÍÓÚñÑü_\s]+$/);
    });

    $("#frmLogin").validate({
        showErrors: function (map, list) {
            this.successList = $.grep(this.successList, function (element) {
                return !(element.name in list);
            });
            $.each(list, function (index, error) {

                $(".error[for=" + error.element.name + "]").text(error.message).css({ 'display': 'inline-block' });
                return false;
            });
            if (list.length == 0)
                this.defaultShowErrors();
        },
        rules: {
            username: {
                required: true,
                minlength: 8,
                maxlength: 15
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 20
            }
,
            captcha: {
                required: true
            }
        },
        messages: {
            username: {
                required: "*Debe ingresar un usuario",
                letters: "*Debe ingresar un usuario con más de 8 caracteres",
                minlength: "*Debe ingresar un usuario con más de 8 caracteres",
                maxlength: "*Debe ingresar un usuario con más de 8 caracteres"
            },
            password: {
                required: "*Debes ingresar una contraseña",
                minlength: "*Debes ingresar una contraseña con más de 8 caracteres",
                maxlength: "*Debes ingresar una contraseña con más de 8 caracteres"
            }
            ,
            captcha: {
                required: "*Debes ingresar el código captcha"
            }
        }
    });
});

function ValidarUsuario() {
    var doc = document; var exito = true;
    $('.forminputtext').each(function () {
        if ($(this).val().length == 0) {
            exito = false;
        }
    });
    if (exito) {
        $('.error').each(function () {
            if ($(this).css("display") == "inline-block")
                exito = false;
        });
    }
    if (exito) { PageMethods.ValidarUsuario($("#username").val(), $("#password").val(), $("#captcha").val(), ValidarUsuario_PostBack); }
}
function ValidarUsuario_PostBack(r) {
    switch (r) {
        case 1: // Exito
            location.href = 'dashboard.aspx';
            break;
        case 2: // Correo o Clave no coinciden
            $("#submit_error").text("Usuario o Contraseña no coinciden");
            $('#submit_error').show();
            $('#username').val("");
            $('#password').val("");
            $('#captcha').val("");
            break;
        case 3: // Captcha
            $('#captcha').val("");
            $("#submit_error").text("El código captcha no coincide. Vuelva a intentarlo");
            $('#submit_error').show();
            CambiarCaptcha();
            break;
    }
}
function CambiarCaptcha() {
    PageMethods.Generar_Captcha(CambiarCaptcha_PostBack);
}
function CambiarCaptcha_PostBack(r) {
    $('#captcha').val("");
    document.getElementById('img_captcha').src = '../SecureCaptcha.ashx?' + Math.random;
}