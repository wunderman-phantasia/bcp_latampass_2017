﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cms_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Validacion"] != null)
        {
            if (!IsPostBack)
            {
                BEUsuario o = (BEUsuario)Session["Validacion"];
                bienvenido.InnerText = o.nombres;
            }
        }
        else Response.Redirect("Default.aspx");
    }
}
