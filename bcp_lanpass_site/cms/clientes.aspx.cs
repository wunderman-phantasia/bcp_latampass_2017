﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
//using ClosedXML.Excel;
using DA;
using System.Reflection;

public partial class clientes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Validacion"] != null)
        {

        }
        else Response.Redirect("Default.aspx");
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        using (var db = new DataClsLanPass())
        {
            var lista = db.PA_LANPASS_CMS_CONSULTAR_CLIENTES(txtBNombres.Value, txtNDocumento.Value, txtBRegIni.Text, txtBRegFin.Text).ToList();
            GridView.PageSize = int.Parse(txtBPagina.Text);

            GridView.DataSource = lista;
            GridView.DataBind();
            if (lista.Count == 0)
            {
                MostrarMensaje("No se han encontrado registros con los criterios de búsqueda seleccionados.");
            }
            else
            {
                updt_mensaje.Update();
                Session["reporte"] = lista;
                mensaje.Visible = false;
            }
        }
    }

    protected void MostrarMensaje(string text)
    {
        mensaje.Visible = true;
        lblEstado.Text = text;
        updt_mensaje.Update();
    }

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[14].Visible = false;
    }

    protected void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Created")
        {
            GridView.DeleteRow(1);
        }
    }

    protected void GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView.PageIndex = e.NewPageIndex;
        btnBuscar_Click(null, null);
    }

    private void BindGrid()
    {
        GridView.DataSource = Session["reporte"];
        GridView.DataBind();
    }

    protected void btnExportCSV_Click(object sender, EventArgs e)
    {
        MExportar();
        //GridView.AllowPaging = false;
        //btnBuscar_Click(null, null);

        //var dt = new DataTable("Listado de Clientes");
        //foreach (TableCell cell in GridView.HeaderRow.Cells)
        //{
        //    dt.Columns.Add(cell.Text);
        //}

        //foreach (GridViewRow row in GridView.Rows)
        //{
        //    dt.Rows.Add();
        //    for (var i = 0; i < row.Cells.Count; i++)
        //    {
        //        var label = row.Cells[i].FindControl("label" + i) as Label;
        //        if (label != null)
        //            dt.Rows[dt.Rows.Count - 1][i] = label.Text;
        //    }
        //}

        //using (var wb = new XLWorkbook())
        //{
        //    wb.Worksheets.Add(dt);
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    Response.AddHeader("content-disposition", "attachment;filename=GridView.xlsx");
        //    using (var myMemoryStream = new MemoryStream())
        //    {
        //        wb.SaveAs(myMemoryStream);
        //        myMemoryStream.WriteTo(Response.OutputStream);
        //        Response.Flush();
        //        Response.End();
        //    }
        //}
    }

    protected void ExportToExcel(object sender, EventArgs e)
    {
        var NombreXLS = "";
        var time = DateTime.Now.ToString("dd MMMM yyyy");
        switch (Request["rep"])
        {
            case "1":
                NombreXLS = "Listado de Usuarios";
                break;
            case "2":
                NombreXLS = "Listado de Clientes";
                break;
            case "3":
                NombreXLS = "Listado de Ofertas";
                break;
            case "4":
                NombreXLS = "Listado de Ofertas por Cliente";
                break;
            case "5":
                NombreXLS = "Listado de Clientes No Registrados";
                break;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=Listado de Clientes - " + time + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (var sw = new StringWriter())
        {
            var hw = new HtmlTextWriter(sw);

            GridView.AllowPaging = false;

            //this.BindGrid();
            btnBuscar_Click(null, null);

            GridView.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in GridView.HeaderRow.Cells)
            {
                cell.BackColor = GridView.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in GridView.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = GridView.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = GridView.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            GridView.RenderControl(hw);

            //style to format numbers to string
            var style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    public string XmLizeDt(DataTable dt, string baseStructHeader, string baseStructFooter, string nombreCliente, string nombreProducto, string fechaReporte, string nombreTabla)
    {
        #region DefaultStruct
        int lintColCount = 0;
        string lstrXMLHeader = @"<?xml version=""1.0""?>
<?mso-application progid=""Excel.Sheet""?>
<Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""
 xmlns:o=""urn:schemas-microsoft-com:office:office""
 xmlns:x=""urn:schemas-microsoft-com:office:excel""
 xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""
 xmlns:html=""http://www.w3.org/TR/REC-html40"">
 <DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">
  <Author>Francisco Cordova</Author>
  <LastAuthor>Francisco Cordova</LastAuthor>
  <Created>2007-03-15T23:04:04Z</Created>
  <Company>Wunderman Phantasia</Company>
  <Version>12.00</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">
  <WindowHeight>6795</WindowHeight>
  <WindowWidth>8460</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>15</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID=""Default"" ss:Name=""Normal"">
   <Alignment ss:Vertical=""Bottom""/>
   <Borders/>
   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID=""s68"">
   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#FFFFFF"" ss:Bold=""1""/>
   <Interior ss:Color=""#CC99FF"" ss:Pattern=""Solid""/>
  </Style>
  <Style ss:ID=""s65"">
   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#FFFFFF""/>
   <Interior ss:Color=""#5B9BD5"" ss:Pattern=""Solid""/>
  </Style>
 </Styles>
<Worksheet ss:Name=""REPLACENOMBRE"">
<Table  x:FullColumns=""1""   x:FullRows=""1"">".Replace("REPLACENOMBRE", nombreTabla);
        string lstrXMLFooter = @"  
  </Table>
</Worksheet>
</Workbook>";
        #endregion

        lintColCount = dt.Rows.Count - 1;

        if (baseStructHeader == "")
        {
            baseStructHeader = lstrXMLHeader;
        }
        if (baseStructFooter == "")
        {
            baseStructFooter = lstrXMLFooter;
        }
        StringBuilder output = new StringBuilder();
        // Escribir encabezados    
        output.Append(baseStructHeader);
        //Escribir la cantidad de columnas
        foreach (DataColumn dc in dt.Columns)
        {
            output.AppendLine(@"<Column ss:Width=""200"" />");
        }

        //Encabezados
        output.AppendLine(@"<Row ss:Index=""2"">");
        output.AppendLine(@"<Cell ss:StyleID=""s68""><Data ss:Type=""String"">Cliente</Data></Cell>");
        output.AppendLine(@"<Cell><Data ss:Type=""String"">" + nombreCliente + "</Data></Cell>");
        output.AppendLine("</Row>");

        output.AppendLine("<Row>");
        output.AppendLine(@"<Cell ss:StyleID=""s68""><Data ss:Type=""String"">Producto</Data></Cell>");
        output.AppendLine(@"<Cell><Data ss:Type=""String"">" + nombreProducto + "</Data></Cell>");
        output.AppendLine("</Row>");

        output.AppendLine("<Row>");
        output.AppendLine(@"<Cell ss:StyleID=""s68""><Data ss:Type=""String"">Fecha</Data></Cell>");
        output.AppendLine(@"<Cell><Data ss:Type=""String"">" + fechaReporte + "</Data></Cell>");
        output.AppendLine("</Row>");

        output.AppendLine(@"<Row ss:Index=""6"">");
        foreach (DataColumn dc in dt.Columns)
        {
            output.AppendLine(@"<Cell ss:StyleID=""s65""><Data ss:Type=""String"">" + dc.ColumnName + "</Data></Cell>");
        }
        output.AppendLine("</Row>");


        // Escribir datos    
        foreach (DataRow item in dt.Rows)
        {
            int lintCounter = 0;

            output.AppendLine("<Row>");
            foreach (object value in item.ItemArray)
            {
                if (dt.Columns[lintCounter].DataType == Type.GetType("System.String"))
                {

                    output.AppendLine(@"<Cell ><Data ss:Type=""String""><![CDATA[" + value + "]]></Data></Cell>");
                }
                else if (dt.Columns[lintCounter].DataType == Type.GetType("System.Decimal"))
                {

                    output.AppendLine(@"<Cell ><Data ss:Type=""Number""><![CDATA[" + value + "]]></Data></Cell>");
                }
                else
                {

                    output.AppendLine(@"<Cell ><Data ss:Type=""String""><![CDATA[" + value.ToString() + "]]></Data></Cell>");
                }

                lintCounter++;
            }
            output.AppendLine("</Row>");
        }
        //Escribir pie
        output.Append(baseStructFooter);
        // Valor de retorno    
        return output.ToString();
    }

    private void MExportar()
    {
        try
        {
            GridView.AllowPaging = false;
            btnBuscar_Click(null, null);

            DataTable dt = new DataTable("Listado de Clientes");

            foreach (TableCell cell in GridView.HeaderRow.Cells)
            {
                dt.Columns.Add(cell.Text);
            }

            foreach (GridViewRow row in GridView.Rows)
            {
                dt.Rows.Add();
                for (var i = 0; i < row.Cells.Count; i++)
                {
                    var label = row.Cells[i].FindControl("label" + i) as Label;
                    if (label != null)
                        dt.Rows[dt.Rows.Count - 1][i] = label.Text;
                }
            }

            var lstrXml = this.XmLizeDt(dt, "", "", "BCP", "LANPASS", DateTime.Now.ToString(), "NOMBRETABLA");
            var path = Server.MapPath("~/Download") + "\\";
            var filename = DateTime.Now.Ticks.ToString() + ".xls";

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            htw.Write(lstrXml);
            Response.Clear();
            Response.Buffer = true;
            System.IO.FileInfo toDownload = new System.IO.FileInfo(path + filename);
            Page.Response.AddHeader("Content-Disposition", "attachment; filename=" + toDownload.Name);
            Page.Response.ContentType = "application/octet-stream";
            Page.Response.ContentEncoding = Encoding.UTF8;
            Response.Write(sb.ToString());
            Response.End();
        }
        catch (Exception lobjException)
        {
            //Error
        }
    }

    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }

    protected void btnExportRetoGanadiario_Click(object sender, EventArgs e)
    {
        var db = new BCPGanadiario2017DataContext();
        DataTable dt = new DataTable();
        dt = ToDataTable(db.PA_LANPASS_CMS_CONSULTAR_CLIENTES_GANADIARIO_2017().ToList());
        string attachment = "attachment; filename=reporte_ganadiario_" + DateTime.Now.ToString("yyyy-MMM-dd-HHmmss") + ".xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");
        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write(tab + dr[i].ToString());
                }
                else
                {
                    Response.Write(tab + RijndaelSimple.Decrypt(dr[i].ToString()));
                }
                
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }






    protected void btnExportRetoGanadiarioF2_Click(object sender, EventArgs e)
    {
        var db = new BCPGanadiario2017F2DataContext();
        DataTable dt = new DataTable();
        dt = ToDataTable(db.PA_LANPASS_CMS_CONSULTAR_CLIENTES_GANADIARIO_2017F2().ToList());
        string attachment = "attachment; filename=reporte_ganadiario_" + DateTime.Now.ToString("yyyy-MMM-dd-HHmmss") + ".xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        Response.ContentEncoding = System.Text.Encoding.Unicode;
        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");
        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write(tab + dr[i].ToString());
                }
                else
                {
                    Response.Write(tab + RijndaelSimple.Decrypt(dr[i].ToString()));
                }

                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }
}