﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using DA;

public partial class clientesnoregistrados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Validacion"] != null)
        {
            
        }
        else Response.Redirect("Default.aspx");
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        using (DataClsLanPass db = new DataClsLanPass())
        {
            List<PA_LANPASS_CMS_CONSULTAR_CLIENTESNOREGISTRADOSResult> lista = db.PA_LANPASS_CMS_CONSULTAR_CLIENTESNOREGISTRADOS(txtBNombres.Value, txtBRegIni.Text, txtBRegFin.Text).ToList();
            GridView.PageSize = int.Parse(txtBPagina.Text);
            GridView.DataSource = lista;
            GridView.DataBind();
            if (lista.Count == 0)
            {
                MostrarMensaje("No se han encontrado registros con los criterios de búsqueda seleccionados.");
            }
            else
            {
                updt_mensaje.Update();
                Session["reporte"] = lista;
                mensaje.Visible = false;
            }
        }
    }
    protected void MostrarMensaje(string text)
    {
        mensaje.Visible = true;
        lblEstado.Text = text;
        updt_mensaje.Update();
    }
    protected void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        /*int id = Convert.ToInt32(e.CommandArgument);
        switch (e.CommandName)
        {
            case "Excel":
                Response.Redirect("export_excel.aspx?rep=2");
                break;
        }*/
    }
    protected void GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView.PageIndex = e.NewPageIndex;
        btnBuscar_Click(null, null);
    }
}