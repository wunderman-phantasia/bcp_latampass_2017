﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using DA;
using System.Data.OleDb;
using System.Data;

public partial class ofertacliente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Validacion"] != null)
        {
            
        }
        else Response.Redirect("Default.aspx");
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        using (DataClsLanPass db = new DataClsLanPass())
        {
            List<PA_LANPASS_CMS_CONSULTAR_OFERTAxCLIENTEResult> lista = db.PA_LANPASS_CMS_CONSULTAR_OFERTAxCLIENTE(txtBNombres.Value, txtBDocumento.Value,txtBRegIni.Text, txtBRegFin.Text).ToList();
            GridView.PageSize = int.Parse(txtBPagina.Text);
            GridView.DataSource = lista;
            GridView.DataBind();
            if (lista.Count == 0)
            {
                MostrarMensaje("No se han encontrado registros con los criterios de búsqueda seleccionados.");
            }
            else
            {
                updt_mensaje.Update();
                Session["reporte"] = lista;
                mensaje.Visible = false;
            }
        }
    }
    protected void MostrarMensaje(string text)
    {
        mensaje.Visible = true;
        lblEstado.Text = text;
        updt_mensaje.Update();
    }
    protected void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        /*int id = Convert.ToInt32(e.CommandArgument);
        switch (e.CommandName)
        {
            case "Excel":
                Response.Redirect("export_excel.aspx?rep=4");
                break;
            case "Agregar":
                
                break;
        }*/
    }
    protected void GridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView.PageIndex = e.NewPageIndex;
        btnBuscar_Click(null, null);
    }
    protected void btn_SubirArchivo_Click(object sender, EventArgs e)
    {
        if ((flimage.PostedFile != null) && (flimage.PostedFile.ContentLength > 0))
        {
            if (flimage.Value.EndsWith(".xls", StringComparison.OrdinalIgnoreCase))
            {
                if (flimage.PostedFile.ContentLength <= 5000000) // 5000kbs
                {
                    string fn = System.IO.Path.GetFileName(flimage.PostedFile.FileName);
                    string rutaExcel = Server.MapPath(@"~\Archivos") + "\\" + DateTime.Now.Year + "" + DateTime.Now.Month + "_" + fn;
                    try
                    {
                        flimage.PostedFile.SaveAs(rutaExcel);
                    }
                    catch (Exception ex)
                    {
                        this.lblmessage.Text = "Error al guardar el archivo, revise los permisos de administrador.";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
                    }
                    DataTable hoja = Leer_Excel(rutaExcel);
                    if (hoja != null)
                    {
                        Grabar_Datos(hoja);
                    }
                    div_loading.Style.Add("display", "none");
                }
                else
                {
                    this.lblmessage.Text = "El tamaño del archivo debe ser menor a 5000kbs";
                    ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
                }
            }
            else
            {
                this.lblmessage.Text = "No se pudo cargar el archivo seleccionado, por favor seleccione un archivo válido";
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
            }
        }
        else
        {
            this.lblmessage.Text = "Seleccione un archivo que cargar.";
            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
        }
    }
    protected DataTable Leer_Excel(string rutaExcel)
    {
        try
        {
            //Creamos la cadena de conexión con el fichero excel
            OleDbConnectionStringBuilder cb = new OleDbConnectionStringBuilder();
            cb.DataSource = rutaExcel;

            if (Path.GetExtension(rutaExcel).ToUpper() == ".XLS")
            {
                cb.Provider = "Microsoft.Jet.OLEDB.4.0";
                cb.Add("Extended Properties", "Excel 8.0;HDR=YES;IMEX=0;");
            }
            else if (Path.GetExtension(rutaExcel).ToUpper() == ".XLSX")
            {
                cb.Provider = "Microsoft.ACE.OLEDB.12.0";
                cb.Add("Extended Properties", "Excel 12.0 Xml;HDR=YES;IMEX=0;");
            }

            DataTable dt = new DataTable("Hoja1");

            using (OleDbConnection conn = new OleDbConnection(cb.ConnectionString))
            {
                //Abrimos la conexión
                conn.Open();
                try
                {
                    using (OleDbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT DOCUMENTO,TIPO_DOCUMENTO,NOMBRES,APELLIDOS,FECHA_INICIO_CAMPANA,FECHA_FIN_CAMPANA,CODIGO_CAMPANA,NOMBRE_OFERTA,CODIGO_OFERTA FROM [Datos$]";

                        //Guardamos los datos en el DataTable
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(dt);
                    }

                    //Cerramos la conexión
                    conn.Close();
                    return dt;
                }
                catch (Exception ex)
                {
                    this.lblmessage.Text = "El formato del archivo excel no es el correcto, revise.";
                    ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
                }
            }
        }
        catch (Exception ex)
        {
            this.lblmessage.Text = "Error: " + ex.Message;
            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
        }
        return null;
    }
    protected bool Grabar_Datos(DataTable dt)
    {
        List<tbl_lan_clienteoferta> lista = new List<tbl_lan_clienteoferta>();
        tbl_lan_clienteoferta o = new tbl_lan_clienteoferta();
        long number = 0;
        DateTime date = DateTime.Now;
        bool exito = true;
        foreach (DataRow item in dt.Rows)
        {
            if (item["DOCUMENTO"].ToString() == "") break;
            if(!long.TryParse(item["DOCUMENTO"].ToString(), out number)){
                lblmessage.Text = "El campo tipo de documento solo acepta valores numéricos, revisar.";
                exito = false;break;
            }
            else if (!item["TIPO_DOCUMENTO"].ToString().Equals("DNI", StringComparison.OrdinalIgnoreCase) && !item["TIPO_DOCUMENTO"].ToString().Equals("CE", StringComparison.OrdinalIgnoreCase))
            {
                lblmessage.Text = "El campo tipo documento sólo acepta los tipos DNI y CE, revisar.";
                exito = false; break;
            }
            else if (!DateTime.TryParse(item["FECHA_INICIO_CAMPANA"].ToString(), out date) || !DateTime.TryParse(item["FECHA_FIN_CAMPANA"].ToString(), out date))
            {
                lblmessage.Text = "Los campos fecha de campaña solo aceptan valores de fecha, revisar.";
                exito = false; break;
            }
            else if (Convert.ToDateTime(item["FECHA_INICIO_CAMPANA"].ToString()) > Convert.ToDateTime(item["FECHA_FIN_CAMPANA"].ToString()))
            {
                lblmessage.Text = "La fecha de inicio de campaña no puede ser mayor a la fecha de fin, revisar.";
                exito = false; break;  
            }
            
            if(item["TIPO_DOCUMENTO"].ToString().Equals("DNI",StringComparison.OrdinalIgnoreCase))
            {
                if (item["DOCUMENTO"].ToString().Length != 8)
                {
                    lblmessage.Text = "El campo DNI, deber tener 8 números, revisar.";
                    exito = false;break;
                }
            }
            else{
                if ((item["DOCUMENTO"].ToString().Length < 8 || item["DOCUMENTO"].ToString().Length > 12) && long.TryParse(item["DOCUMENTO"].ToString(), out number))
                {
                    lblmessage.Text = "El campo CE, deber tener mínimo 8 dígitos, revisar.";
                    exito = false;break;
                }
            }

            if (lista.Where(x => x.cof_str_documento == item["DOCUMENTO"].ToString()).Count() == 0)
            {
                o = new tbl_lan_clienteoferta();
                o.cof_str_documento = item["DOCUMENTO"].ToString();
                o.cof_str_tipodocumento = item["TIPO_DOCUMENTO"].ToString();
                o.cof_str_nombres = item["NOMBRES"].ToString();
                o.cof_str_apellidos = item["APELLIDOS"].ToString();
                o.cof_dat_iniciocampana = Convert.ToDateTime(item["FECHA_INICIO_CAMPANA"].ToString());
                o.cof_dat_fincampana = Convert.ToDateTime(item["FECHA_FIN_CAMPANA"].ToString());
                o.cof_str_codcampana = item["CODIGO_CAMPANA"].ToString();
                o.cof_str_oferta = item["NOMBRE_OFERTA"].ToString();
                o.cof_str_codigo = item["CODIGO_OFERTA"].ToString();
                lista.Add(o);
            }
            else
            {
                lblmessage.Text = "No se puede cargar archivo ya que existe información duplicada, revisar.";
                exito = false;break;    
            }
        }
        if(!exito)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
            return false;
        }
        exito = new BLCliente().Grabar_OfertasxCliente(lista);
        if (!exito) { lblmessage.Text= "No se pudieron grabar los registros. Verífique haber llenado todos los campos";
            ClientScript.RegisterStartupScript(this.GetType(), "myScript", "$('.modal[data-id=1]').show();", true);
        }
        else
        {
            lblmessage.Text = "";
            btnBuscar_Click(null, null);
            updt_Principal.Update();
            MostrarMensaje("El archivo se cargó correctamente");
        }
        return exito;
    }
}