﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="export_excel.aspx.cs" Inherits="cms_export_excel" StylesheetTheme="skin" %>
<%@ Import Namespace="DA" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BCP - LANPASS</title>
    <style>
        .textmode
        {
            mso-number-format: \@;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr style='height: 70px'>
                <td></td>
                <td>
                    <img src="https://www.bcplanpass.com/images/logobcpexcel.jpg" /></td>
            </tr>
            <tr>
                <td style="height: 35px;">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <table id="MiTabla1" runat="server" width="100%" border="1" style="font-family: Arial; font-size: 15px; width: 100%; border-collapse: collapse;">
                        <tr>
                            <td style="background-color: #CC99FF; font-weight: bold;" class="tabla_phantasia_cabecera">Cliente&nbsp;&nbsp;</td>
                            <td style="text-align: left;">BCP</td>
                        </tr>
                        <tr>
                            <td style="background-color: #CC99FF; font-weight: bold;" class="tabla_phantasia_cabecera">Producto&nbsp;&nbsp;</td>
                            <td style="text-align: left;">LANPASS</td>
                        </tr>
                        <tr>
                            <td style="background-color: #CC99FF; font-weight: bold;" class="tabla_phantasia_cabecera">Fecha&nbsp;&nbsp;</td>
                            <td style="text-align: left;" id="Lblfecha" runat="server" clientidmode="Static"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>

        <table>
            <tr>
                <td></td>
                <td>
                    <asp:GridView ID="GridView" runat="server"
                        GridLines="Horizontal"
                        ShowHeaderWhenEmpty="true"
                        AllowPaging="False"
                        SkinID="Phantasia"
                        OnRowDataBound="GridView_RowDataBound"
                        ShowFooter="False"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).ID%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cliente">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Cliente%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Teléfono">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Telefono%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo Documento">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Tipo_Documento%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nro Documento">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Nro_Documento%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).E_mail%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cta Sueldo">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Cta_Sueldo%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sueldo">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Sueldo / 1m).ToString("N2")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Moneda">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Moneda.ToUpper()%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tarjeta">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Tarjeta%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Registro">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).Fecha_Registro%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IP">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult)Container.DataItem).IP%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UTM Source">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#string.IsNullOrEmpty(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Source) ? "none" : ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Source%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UTM Medium">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#string.IsNullOrEmpty(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Medium) ? "none" : ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Medium%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UTM Campaign">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <ItemTemplate>
                                    <%#string.IsNullOrEmpty(((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Campaign) ? "none" : ((PA_LANPASS_CMS_CONSULTAR_CLIENTESResult) Container.DataItem).UTM_Campaign%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <%--<br />&nbsp;<asp:label id="Label3" runat="server">Total Registros</asp:label>&nbsp;&nbsp;&nbsp; <asp:label id="lblTotal" runat="server"></asp:label>--%>
    </form>
</body>
</html>
