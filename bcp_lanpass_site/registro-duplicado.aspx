﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registro-duplicado.aspx.cs" Inherits="registro_duplicado" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles/reset.css" type="text/css" />
    <link rel="stylesheet" href="Styles/fonts.css" type="text/css" />
    <style type="text/css">
        .message {
            border: 3px;
            width: 470px;
            height: 100px;
            padding-top: 50px;
            padding-bottom: 50px;
            padding-left:30px;
            padding-right:20px;
            background: #DADADA;
            font-family: 'Flexo-Italic';
            color: #FF4F00;
            border-style: solid;
            border-color: #ffffff;
        }
        .message div {
            font-size: 19px;
        }
        #cboxClose {
            right: 10px !important;
            top: 10px !important;
        }
        .btn-aceptar {
            display: block;
            width: 112px;
            height: 25px;
            background: url('../images/promo2015septiembre/message-bt-aceptar.png') no-repeat;
            left: 35px;
            top: 336px;
            z-index: 60;
            margin-left: 170px;
            margin-top: 32px;
        }
    </style>
</head>
<body>
    <div class="message">
        <div>
            ¡Felicitaciones! Ya estás participando de esta campaña.<br/> 
            Ahora llegar a tu próximo viaje, ¡solo depende de ti!<br/> 
            Cumple con tu reto y gana tu bono de KMS. LANPASS.</div>
        <a href="javascript:void(0)" class="btn-aceptar" onclick="parent.closeMessage();"></a>
    </div>
</body>
</html>
