﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="no-hay-cupos.aspx.cs" Inherits="registro_duplicado" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles/reset.css" type="text/css" />
    <link rel="stylesheet" href="Styles/fonts.css" type="text/css" />
    <style type="text/css">
        .message {
            border: 3px;
            width: 300px;
            height: auto;
            padding: 50px;
            background: #DADADA;
            font-family: 'Flexo-Italic';
            color: #FF4F00;
            border-style: solid;
            border-color: #ffffff;
        }

            .message div {
                font-size: 19px;
            }

    </style>
</head>
<body>
    <div class="message">
        <div>
            Llegamos a los <b><strong>20 mil inscritos</strong></b><br />
            ¡No dejes de participar en nuestra pr&oacute;xima campa&ntilde;a!
        </div>
    </div>
</body>
</html>
