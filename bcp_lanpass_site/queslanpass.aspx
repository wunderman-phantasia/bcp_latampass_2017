﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="queslanpass.aspx.cs" Inherits="queslanpass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

	<script type="text/javascript">
	    //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
	    Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.Content header h1', { fontFamily: 'Flexo-Bold' });
	</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <section class="Content  contentb">
		<header style="height:34px;">
            <h1 tabindex="7" title="¿Qué es LATAM Pass?">¿Qué es LATAM Pass?</h1>
            <div class="div_regresa"><a class="promobtn" href="Default.aspx" tabindex="8" title="Regresar">Regresar</a></div>
		</header>
		
		<section class="texto" tabindex="8" title="Es el Programa de Pasajero Frecuente de LATAM, creado para premiar la preferencia y lealtad de sus pasajeros, los cuales podrán acumular kilómetros LATAM para canjearlos por dos beneficios: Pasajes a más de 950 destinos alrededor del mundo. Válidos para rutas LATAM, TAM, aerolíneas oneworld o líneas aéreas asociadas. Productos del catálogo BCP LATAM Pass.">
			<hr>
			<p class="fixwidth">
				<span class="lightitalic16dark">Es el </span><span class="mediumitalic16dark">Programa de Pasajero Frecuente de LATAM Airlines, </span><span class="lightitalic16dark">creado para premiar la preferencia y lealtad de sus pasajeros, los cuales podrán acumular </span><span class="mediumitalic16dark">KMS. LATAM Pass </span><span class="lightitalic16dark">para canjearlos por:</span>
			</p>
			<p class="fixwidth"><img src="images/whatislanpass_airplaneicon.png" width="21" height="18" alt=""/>
				<span class="lightitalic16dark">Pasajes a más de </span>
				<span class="mediumitalic16dark">950 destinos alrededor del mundo.*</span>
			</p>
			<p class="fixwidth"><img src="images/whatislanpass_gifticon.png" width="21" height="22" alt=""/>
				<span class="lightitalic16dark">Productos del </span>
				<span class="mediumitalic16dark">catálogo LATAM Pass.</span>
			</p>
			<hr>
			<p class="fixwidth">
				<span class="lightitalic11dark">*Válido para rutas LATAM Airlines aerolíneas </span><span class="mediumitalic11dark">one</span><span class="lightitalic11dark">world o líneas aéreas asociadas.</span>
			</p>
		</section>
		<section class="image" style="margin-top:55px;">
				<img src="images/whatislanpass_image.png" width="300" height="239" alt="">
		</section>
		<div class="clear"></div>
	</section>
	
    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Quees").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/que_es']);
    </script>
</asp:Content>

