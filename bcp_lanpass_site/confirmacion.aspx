﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="confirmacion.aspx.cs" Inherits="confirmacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
        Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.Content header h1', { fontFamily: 'Flexo-Bold' });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <section class="Content" style="padding-left: 40px; position: relative; height: 384px;" tabindex="7" title="Tu solicitud ha sido enviada. Pronto nos comunicaremos contigo.">
        <header style="height: 34px;">
            <h1>Solicitud enviada</h1>
            <div class="div_regresa"><a class="promobtn" href="Default.aspx" tabindex="8" title="Regresar">Regresar</a></div>
        </header>
        <section class="visaconfirmation">
            <h1 style="font-size: 31px; font-weight: bold; margin-bottom: 24px; color: #474747;">¡Gracias!</h1>
           
            <p style="font-size: 16px;">
                Ya tenemos tus datos, en un plazo de 03 días hábiles, uno de nuestros ejecutivos se estará contactando contigo.
                <br />
                <br />

                   En caso de no recibir una comunicación en el plazo indicado, significa que su solicitud no ha sido aprobada. 
                <br />
                <br />
                    En tus planes contigo, BCP.
            </p>

            <!--<p style="font-family:Flexo-Regular;font-size:14px;color:#474747;">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, risus in tincidunt ullamcorper, purus purus mattis elit, a facilisis lectus nisi quis leo. 
            </p>-->
            <div class="imgAbsolute img_confirmacion" style="margin-right: 15px;"></div>
        </section>



        <div class="clear"></div>
    </section>

    <script type="text/javascript">
        _gaq.push(['_trackPageview', '/WebsiteOficial/formulario/confirmacion']);
    </script>

</asp:Content>

