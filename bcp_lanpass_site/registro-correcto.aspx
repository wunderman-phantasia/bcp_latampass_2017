﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registro-correcto.aspx.cs" Inherits="registro_correcto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="Styles/fonts.css" type="text/css" />
    <style type="text/css">
        .thanks
        {
            width: 421px;
            height: auto;
            padding: 50px;
            background: #FF5000;
            font-family: 'Flexo-Italic';
            color: #FFFFFF;
            -webkit-border-radius: 15px;
            border-radius: 15px;
        }

            .thanks h3
            {
                font-size: 24px;
                padding: 0;
            }

            .thanks p
            {
                font-size: 21px;
            }
    </style>
</head>
<body>
    <div class="thanks">
        <h3>¡Listo! Te has inscrito correctamente.</h3>
        <p>
            Dentro de unos minutos recibirás un E-mail<br/> 
            confirmando tu participación.        
        </p>
    </div>
</body>
</html>
