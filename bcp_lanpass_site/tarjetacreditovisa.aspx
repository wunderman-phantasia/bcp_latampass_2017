﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="tarjetacreditovisa.aspx.cs" Inherits="tarjetacreditovisa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.contentwrap p, .italic30orange, .visa figure h1', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.contentwrap header h1, .mediumitalic30orange, .lista section header h1', { fontFamily: 'Flexo-Bold' });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <section class="Content contentb">
        <header style="height: 34px;">
            <h1 tabindex="7" title="Tarjetas de Crédito Visa: Disponemos de 04 tipos: Clásica, Oro, Platinum, Signature.">
                <span class="italic30orange">Nuestras Tarjetas</span><span class="mediumitalic30orange"> - Crédito VISA</span>
            </h1>
            <div class="div_regresa2"><a class="promobtn" href="masinformacion.aspx" tabindex="8" title="Regresar">Regresar</a></div>
        </header>
        <div class="tarjetas">
            <section class="visa">
                <figure>
                    <h1>Clásica</h1>
                    <img src="images/visacard/visacard_clasiccard.png" width="151" height="95" alt="" />
                    <figcaption>
                        <p tabindex="8" title="Tarjeta de Crédito Visa Clásica: Bono de bienvenida 1,500 kilómetros LATAM Pass.">
                            <span class="lightitalic14blue">Bono de bienvenida<sup>1</sup></span>
                            <br />
                            <span class="mediumitalic14blue">1,500 KMS. LATAM Pass</span>
                        </p>
                    </figcaption>
                </figure>

                <figure>
                    <h1>Oro</h1>
                    <img src="images/visacard/visacard_goldcard.png" width="151" height="95" alt="">
                    <figcaption>
                        <p tabindex="9" title="Tarjeta de Crédito Visa Oro: Bono de bienvenida 1,500 kilómetros LATAM Pass.">
                            <span class="lightitalic14blue">Bono de bienvenida<sup>1</sup></span>
                            <br />
                            <span class="mediumitalic14blue">2,500 KMS. LATAM Pass</span>
                        </p>
                    </figcaption>
                </figure>

                <figure>
                    <h1>Platinum</h1>
                    <img src="images/visacard/visacard_platinumcard.png" width="151" height="95" alt="">
                    <figcaption>
                        <p tabindex="10" title="Tarjeta de Crédito Visa Platinum: Bono de bienvenida 3,500 kilómetros LATAM Pass.">
                            <span class="lightitalic14blue">Bono de bienvenida<sup>1</sup></span>
                            <br />
                            <span class="mediumitalic14blue">3,500 KMS. LATAM Pass</span>
                        </p>
                    </figcaption>
                </figure>

                <figure>
                    <h1>Signature</h1>
                    <img src="images/visacard/visacard_signedcard.png" width="151" height="95" alt="">
                    <figcaption>
                        <p tabindex="11" title="Tarjeta de Crédito Visa Signature: Bono de bienvenida 5,000 kilómetros LATAM Pass.">
                            <span class="lightitalic14blue">Bono de bienvenida<sup>1</sup></span>
                            <br />
                            <span class="mediumitalic14blue">5,000 KMS. LATAM Pass</span>
                        </p>
                    </figcaption>
                </figure>
            </section>

            <hr>
            <span class="cardsasktext">¿Aún no tienes tu Tarjeta de Crédito BCP LATAM Pass?</span>
            <a class="btnbanner1" href="formulario.aspx?pickcard=visa" style="margin-top: 20px;" tabindex="12" title="Pide tu Tarjeta BCP LATAM Pass online.">¡Pídela aquí!</a>

        </div>
        <div class="clear"></div>
        <div class="lista" style="display: block; width: 100%;">
            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_bennefitsicon.png" width="22" height="21" alt="" />Beneficios</h1>
                </header>
                <hr>
                <section tabindex="13" title="Tarjetas de Crédito Visa Clásica: Beneficios: Seguro de accidentes de viaje, centro de información para viajes, servicio al cliente Visa.">
                    <header>
                        <h3>Clásica</h3>
                    </header>
                    <ul class="first">
                        <li style="visibility: hidden">&nbsp;</li>
                    </ul>
                    <ul class="last">
                         <li style="visibility: hidden">&nbsp;</li>
                        <li><span class="lightitalic14light">Centro de información<br />
                            para viajes</span></li>
                        <li><span class="lightitalic14light">Servicio al cliente Visa</span></li>
                    </ul>
                </section>
                <section tabindex="14" title="Tarjeta de Crédito Visa Oro. Beneficios: Seguro de accidentes de viaje, seguro de autos alquilados, centro de información para viajes, servicios de emergencia, servicio al cliente.">
                    <div>
                        <header>
                            <h3>Oro</h3>
                        </header>
                        <ul class="first">

                            <li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
                        </ul>
                        <ul class="last">
                             <li style="visibility: hidden">&nbsp;</li>
                            <li><span class="lightitalic14light">Centro de información para viajes</span></li>
                            <li><span class="lightitalic14light">Servicios de emergencia</span></li>
                            <li><span class="lightitalic14light">Servicio al cliente</span></li>
                        </ul>
                    </div>
                </section>
                <section style="height: 660px;" tabindex="15" title="Tarjeta de Crédito Visa Platinum. Beneficios: Seguro de accidentes de viaje, seguro de autos alquilados, seguro de pérdida de equipaje, seguro médico de emergencias, concierge personal, centro de información para viajes, servicios de emergencia, centro de atención telefónica Visa Platinum.">
                    <div>
                        <header>
                            <h3>Platinum</h3>
                        </header>
                        <ul class="first">
                            <li><span class="lightitalic14light">Seguro de accidentes en viajes</span></li>
                            <li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
                            <li><span class="lightitalic14light">Seguro médico de emergencias</span></li>
                        </ul>
                        <ul class="last">
                            <li><span class="lightitalic14light">Concierge personal</span></li>
                            <li><span class="lightitalic14light">Centro de información para viajes</span></li>
                            <li><span class="lightitalic14light">Servicios de emergencia</span></li>
                            <li><span class="lightitalic14light">Centro de atención telefónica Visa Platinum</span></li>
                        </ul>
                    </div>
                </section>
                <section class="lastsec" tabindex="16" title="Tarjeta de Crédito Visa Signature. Beneficios: Seguro de accidentes de viaje, seguro de autos alquilados, seguro de pérdida de equipaje, seguro médico de emergencias, compra protegida, garantía extendida, concierge personal, centro de información para viajes, servicios de emergencia, centro de atención telefónica Visa Signature, Priority Pass V.I.P. Lounges.">
                    <div>
                        <header>
                            <h3>Signature</h3>
                        </header>
                        <ul class="first">
                            <li><span class="lightitalic14light">Seguro de accidentes de viaje</span></li>
                            <li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
                            <li><span class="lightitalic14light">Seguro de pérdida de equipaje</span></li>
                            <li><span class="lightitalic14light">Seguro médico de emergencias</span></li>
                            <li><span class="lightitalic14light">Compra protegida</span></li>
                            <li><span class="lightitalic14light">Garantía extendida</span></li>
                        </ul>
                        <ul class="last">
                            <li><span class="lightitalic14light">Concierge personal</span></li>
                            <li><span class="lightitalic14light">Centro de información para viajes</span></li>
                            <li><span class="lightitalic14light">Servicios de emergencia</span></li>
                            <li><span class="lightitalic14light">Centro de atención telefónica Visa Signature</span></li>
                            <li><span class="lightitalic14light">25% adicional de acumulación de KMS. LATAM Pass<sup>2</sup></span><span class="lightitalic14light"><br />
                                En los viajes que realices en LATAM durante el primer año.</span></li>
                            <li><span class="lightitalic14light">Cupones upgrade<sup>3</sup></span><span class="lightitalic14light"><br />
                                Te otorgamos dos cupones upgrade de cortesía para que viajes en Premium Business o en Premium Economy en LATAM.</span></li>
                            <li><span class="lightitalic14light">Chequeo preferente<sup>4</sup></span><span class="lightitalic14light"><br />
                                Te ofrecemos check-in preferente en los vuelos internacionales vendidos y operados por LATAM, así viajes en clase Economy.</span></li>
                            <li><span class="lightitalic14light">Priority Pass:</span><span class="lightitalic14light"><br />
                                <br />
                                Salones VIP de aeropuertos internacionales y del espigón internacional del Aeropuerto Jorge Chávez: Podrás ingresar gratis con un invitado, pero si tienes invitados adicionales,  cada uno de ellos tendrá un costo de US$ 27.00<sup>5</sup>.<br />
                                <br />
                                Salones VIP en el espigón nacional del Aeropuerto Jorge Chávez o en otros aeropuertos dentro de Perú: Tu ingreso tendrá un costo de US$ 25.00<sup>5</sup>. Además, cada invitado tendrá un costo por persona de US$ 27.00<sup>5</sup>.<br />
                                <br />
                                Para conocer el detalle completo de los beneficios que te ofrece tanto tu Priority Pass como tu tarjeta Visa Signature, te invitamos a descargar sus aplicaciones en tu Smartphone o ingresar a <a class="lightitalic14light" style="text-decoration: none" href="https://www.prioritypass.com">www.prioritypass.com</a> o  <a class="lightitalic14light" style="text-decoration: none" href="http://www.visa-signature.com/pe">www.visa-signature.com</a></span></li>
                        </ul>
                        <!--<img src="images/visacard/visacard_signedbenefitimage.png" width="185" height="25" alt="" style="margin-top: 25px;">-->
                    </div>
                </section>
                <div class="clear"></div>
                <!--<p class="lightitalic11dark benefits_foottext" title="* Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. El 25% de acumulación adicional de kilómetros LATAM Pass no será acumulable si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta y solo para el titular de la misma. ** Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. Los upgrades están sujetos a disponibilidad de espacio y no serán acumulables si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta. *** Válido solo para el titular de la tarjeta, en el counter de clase ejecutiva de LATAM del Aeropuerto de Lima para vuelos internacionales, mostrando su Tarjeta de Crédito Visa LATAM Pass Signature.">
                    * Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. El 25% de acumulación adicional de kilómetros LATAM Pass no será acumulable si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta y solo para el titular de la misma.
                    <br />** Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. Los upgrades están sujetos a disponibilidad de espacio y no serán acumulables si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta.
                    <br />*** Válido solo para el titular de la tarjeta, en el counter de clase ejecutiva de LATAM del Aeropuerto de Lima para vuelos internacionales, mostrando su Tarjeta de Crédito Visa LATAM Pass Signature.

                </p>-->
            </section>

            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_lanpassprogramicon.png" width="23" height="20" alt="" />Programa LATAM Pass</h1>
                </header>
                <hr>
                <section tabindex="17" title="Tarjetas de Crédito Visa Clásica. Programa LATAM Pass: acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
                    <header>
                        <h3>Clásica</h3>
                    </header>
                    <p><span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo<sup>6</sup> o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span></p>
                </section>
                <section tabindex="18" title="Tarjetas de Crédito Visa Oro. Programa LATAM Pass: acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
                    <div>
                        <header>
                            <h3>Oro</h3>
                        </header>
                        <p>
                            <span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo<sup>6</sup> o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span>
                        </p>
                    </div>
                </section>
                <section tabindex="19" title="Tarjetas de Crédito Visa Platinum. Programa LATAM Pass: acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
                    <div>
                        <header>
                            <h3>Platinum</h3>
                        </header>
                        <p>
                            <span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo<sup>6</sup> o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span>
                        </p>
                    </div>
                </section>
                <section class="lastsec" tabindex="20" title="Tarjetas de Crédito Visa Signature. Programa LATAM Pass: acumulas 2 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en supermercados, grifos, restaurantes, tiendas por departamento y centros de estudio y 1.5 KMS. LATAM Pass por tus demás consumos*, en el Perú y el mundo.">
                    <div>
                        <header>
                            <h3>Signature</h3>
                        </header>
                        <p>
                            <span class="lightitalic14light">Acumulas </span><span class="mediumitalic14dark">2 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo<sup>6</sup> o su equivalente en soles en supermercados, grifos, restaurantes, tiendas por departamento y centros de estudio y </span><span class="mediumitalic14dark">1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por tus demás consumos<sup>6</sup>, en el Perú y el mundo.</span>
                        </p>
                    </div>
                </section>
                <div class="clear"></div>
            </section>

            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_requirementsicon.png" width="23" height="23" alt="" />Requisitos</h1>
                </header>
                <hr>
                <section tabindex="21" title="Tarjetas de Crédito Visa Clásica. Requisito: Debes tener un ingreso mínimo mensual de S/. 1,800.">
                    <header>
                        <h3>Clásica</h3>
                    </header>
                    <p>
                        <span class="lightitalic14light">Debes tener un ingreso mínimo<br />
                            mensual de </span><span class="mediumitalic14dark">S/. 1,800</span>
                    </p>
                </section>
                <section tabindex="22" title="Tarjeta de Crédito Oro. Requisito: Debes tener un ingreso mínimo mensual de S/. 2,500.">
                    <div>
                        <header>
                            <h3>Oro</h3>
                        </header>
                        <p>
                            <span class="lightitalic14light">Debes tener un ingreso mínimo<br />
                                mensual de </span><span class="mediumitalic14dark">S/. 2,500</span>
                        </p>
                    </div>
                </section>
                <section tabindex="23" title="Tarjeta de Crédito Visa Platinum. Requisito: Debes tener un ingreso mínimo mensual de S/. 6,000.">
                    <div>
                        <header>
                            <h3>Platinum</h3>
                        </header>
                        <p>
                            <span class="lightitalic14light">Debes tener un ingreso mínimo<br />
                                mensual de </span><span class="mediumitalic14dark">S/. 6,000</span>
                        </p>
                    </div>
                </section>
                <section class="lastsec" tabindex="24" title="Requisitos Tarjeta de Crédito Visa Signature. Requisito: Debes tener un ingreso mínimo mensual de S/. 12,000.">
                    <div>
                        <header>
                            <h3>Signature</h3>
                        </header>
                        <p>
                            <span class="lightitalic14light">Debes tener un ingreso mínimo<br />
                                mensual de </span><span class="mediumitalic14dark">S/. 12,000</span>
                        </p>
                    </div>
                </section>
                <div class="clear"></div>
            </section>

            <div tabindex="25" title="Requisitos generales. Debes estar laborando al momento de solicitar la tarjeta. Documentos necesarios: Copia simple de DNI y copia de último recibo de servicios. Si eres dependiente, deberás contar con la copia de tus boletas de pago de los últimos 2 meses (últimas 3 boletas para vendedores y comisionistas). Si eres independiente, deberás contar con la copia del formulario de pago de impuestos por honorarios profesionales, ventas o rentas de los últimos 3 meses o copia de última declaración de impuesto a la renta.">
                <ul>
                    <li><span class="mediumitalic14dark">Debes estar laborando al momento de solicitar la tarjeta.</span></li>
                    <li><span class="mediumitalic14dark">Documentos necesarios:</span>
                        <ul class="bulletreqs">
                            <li><span class="lightitalic14light">- Copia simple de DNI.</span></li>
                            <li><span class="lightitalic14light">- Copia de último recibo de servicios.</span></li>
                            <li><span class="italic14light">- Dependientes: </span><span class="lightitalic14light">Copia de boletas de pago de los últimos 2 meses (últimas 3 boletas para vendedores y comisionistas).</span></li>
                            <li><span class="italic14light">- Independientes: </span><span class="lightitalic14light">Copia de formulario de pago de impuestos por honorarios profesionales, ventas o rentas de los últimos 3 meses o copia de
                                <br />
                                &nbsp;&nbsp;última declaración de impuesto a la renta.</span></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="lightitalic11light" style="margin: 30px 0px;" tabindex="26" title="Para solicitar una Tarjeta de Crédito BCP LATAM Pass solo debes llamarnos al 311 9898 o acércate a cualquiera de nuestras Agencias BCP a nivel nacional. También puedes dejarnos tus datos en el formulario virtual ubicado en esta web y en los próximos días nos estaremos comunicando contigo para tramitar tu tarjeta.">
                <span>Para solicitar una Tarjeta de Crédito BCP LATAM Pass solo debes llamarnos al 311 9898 o acércate a cualquiera de nuestras Agencias BCP a nivel nacional.<br />
                    También puedes dejarnos tus datos en el formulario virtual ubicado en esta web y en los próximos días nos estaremos comunicando contigo para tramitar tu tarjeta.</span>
            </div>



            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_featuresicon.png" width="25" height="23" alt="" />Características</h1>
                </header>
                <hr>
                <ul style="margin: 30px 0;" tabindex="27" title="Características comunes: Las tarjetas de Crédito BCP LATAM Pass Visa del BCP tienen validez nacional e internacional. Aceptada en mas de 50,000 establecimientos en el Perú y 12 millones en el mundo. Puedes comprar, retirar efectivo y comprar por internet, Puedes financiar tus consumos al contado, modalidad revolvente o en cuotas de hasta 36 meses. Te permite elegir tu fecha de facturación entre los 10 o 25 de cada mes. No tendrás costo mensual si no tienes deuda.">
                    <li><span class="lightitalic14light">Las Tarjetas de Crédito LATAM Pass Visa del BCP tienen</span><span class="mediumitalic14dark"> validez nacional e internacional.</span></li>
                    <li><span class="lightitalic14light">Aceptada en mas de</span><span> 50,000 establecimientos en el Perú </span><span class="mediumitalic14dark">y 12 millones en el mundo.</span></li>
                    <li><span class="lightitalic14light">Puedes comprar, retirar efectivo y</span><span class="mediumitalic14dark"> comprar por internet.</span></li>
                    <li><span class="lightitalic14light">Puedes financiar tus consumos al contado, modalidad revolvente o en</span><span class="mediumitalic14dark"> cuotas de hasta 36 meses.</span></li>
                    <li><span class="mediumitalic14dark">Te permite elegir </span><span class="lightitalic14light">tu fecha de facturación entre los 10 o 25 de cada mes.</span></li>
                    <li><span class="mediumitalic14dark">No tendrás costo </span><span class="lightitalic14light">mensual si no tienes deuda.</span></li>
                </ul>
            </section>

            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_commonbennefitsicon.png" width="24" height="16" alt="" />Beneficios Comunes</h1>
                </header>
                <hr>
                <ul style="margin: 30px 0;" tabindex="28" title="Beneficios comunes: 
Disposición de efectivo: Puedes solicitar hasta 95% de tu línea de crédito en efectivo.
Compra de deuda: ¡no tengas deudas separadas! las Tarjetas de Crédito LATAM Pass Visa del BCP te dan Facilidad para trasladar tus deudas de otras tarjetas de crédito a tasas preferenciales.
Cargo recurrente: ¡ordena tus pagos y ahorra tiempo! accede a cargo recurrente y hacer que tus pagos regulares (ej: agua, luz, teléfono) se cobren automáticamente de tu tarjeta de crédito.
Tarjetas adicionales: obten hasta 9 tarjetas de crédito adicionales para tus familiares.
Campañas especiales: accede a campañas de descuentos con tu tarjeta de crédito y a campañas de acumulacion de KMS. LATAM Pass.
¡Ahorra tiempo!, realiza tus consultas y pagos por Banca por Teléfono (311-9898) o viabcp.com.">
                    <li><span class="mediumitalic14dark">Disposición de efectivo: </span><span class="lightitalic14light">Puedes solicitar hasta 95% de tu línea de crédito en efectivo.</span></li>
                    <li><span class="mediumitalic14dark">Compra de deuda: </span><span class="lightitalic14light">¡No tengas deudas separadas! Las Tarjetas de Crédito LATAM Pass Visa del BCP te dan Facilidad para trasladar tus deudas de otras tarjetas de crédito a tasas preferenciales.</span></li>
                    <li><span class="mediumitalic14dark">Cargo recurrente: </span><span class="lightitalic14light">¡Ordena tus pagos y ahorra tiempo! Accede a cargo recurrente y haz que tus pagos regulares (ej: agua, luz, teléfono) se cobren automáticamente de tu tarjeta de crédito.</span></li>
                    <li><span class="mediumitalic14dark">Tarjetas Adicionales: </span><span class="lightitalic14light">Obten hasta 9 tarjetas de crédito adicionales para tus familiares.</span></li>
                    <li><span class="mediumitalic14dark">Campañas especiales: </span><span class="lightitalic14light">Accede a campañas de descuentos con tu tarjeta de crédito y a campañas de acumulacion de KMS. LATAM Pass.</span></li>
                    <li><span class="mediumitalic14dark">¡Ahorra tiempo! </span><span class="lightitalic14light">Realiza tus consultas y pagos por Banca por Teléfono (311-9898) o viabcp.com.</span></li>
                </ul>
            </section>

            <hr style="border: 1px solid #EFEFEF; margin-bottom: 20px;">
            <div style="padding-right: 8px; margin-bottom: 30px;" tabindex="29" title="Condiciones legales: 
1. Para recibir el bono de kilómetros debes usar tu tarjeta de crédito en los primeros 45 días desde su aprobación. Las tarjetas adicionales y upgrade no aplican al bono. 
2. Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. El 25% de acumulación adicional de kilómetros LATAM Pass no será acumulable si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta y solo para el titular de la misma. 
3. Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. Los upgrades están sujetos a disponibilidad de espacio y no serán acumulables si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta.
4. Válido solo para el titular de la tarjeta,  en el counter de clase ejecutiva de LATAM del Aeropuerto de Lima para vuelos internacionales, mostrando su Tarjeta de Crédito Visa Signature BCP LATAM Pass. 
5. En caso genere alguno de estos costos adicionales se cargarán en dólares a su Tarjeta de Crédito Visa Signature LATAM Pass.
6. Las disposiciones de efectivo, traslados de deuda, efectivo preferente, consumos en casinos, pagos de servicios realizados en viabcp.com y banca móvil, débitos automáticos, los pagos de impuestos y los servicios legales  no acumulan KMS. LATAM Pass. 
">
                <span class="lightitalic11dark" style="font-family: cursive;">(1) Para recibir el bono de kilómetros debes usar tu tarjeta de crédito en los primeros 45 días desde su aprobación. Las tarjetas adicionales y upgrade no aplican al bono.
                    <br />
                    <br />
                    (2) Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. El 25% de acumulación adicional de kilómetros LATAM Pass no será acumulable si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta y solo para el titular de la misma.
                    <br />
                    <br />
                    (3) Solo aplica si el cliente no forma parte de alguna de las categorías de socio elite LATAM Pass. Los upgrades están sujetos a disponibilidad de espacio y no serán acumulables si el cliente pasa a formar parte de alguna de dichas categorías. Este beneficio es otorgado por única vez y estará disponible para el socio 30 días posteriores a la fecha de corte de la primera facturación de la tarjeta. Válido solo durante el primer año de obtenida la tarjeta.
                    <br />
                    <br />
                    (4) Válido solo para el titular de la tarjeta,  en el counter de clase ejecutiva de LATAM del Aeropuerto de Lima para vuelos internacionales, mostrando su Tarjeta de Crédito Visa Signature BCP LATAM Pass.
                    <br />
                    <br />
                    (5) En caso genere alguno de estos costos adicionales se cargarán en dólares a su Tarjeta de Crédito Visa Signature LATAM Pass.
                    <br />
                    <br />
                    (6) Las disposiciones de efectivo, traslados de deuda, efectivo preferente, consumos en casinos, pagos de servicios realizados en viabcp.com y banca móvil, débitos automáticos, los pagos de impuestos y los servicios legales  no acumulan KMS. LATAM Pass.
                    <br />
                    <br />
                    La acumulación, canje, uso y demás condiciones aplicables a los KMS. LATAM Pass se rigen bajo el reglamento de LATAM Pass publicado en <a class="lightitalic11dark" href="https://www.latam.com/es_pe/latam-pass/" style="text-decoration: none;">www.latam.com/es_pe/latam-pass/</a>. Para más información, condiciones y restricciones ingresar a viabcp.com. Sujeto a los límites diarios por operaciones establecidas para cada canal. El abono de KMS. LATAM Pass por consumos con Tarjetas de Crédito BCP LATAM Pass, se realizará 48 horas después de la fecha de corte de facturación. Los kilómetros acumulados por vuelo en las Líneas Aéreas oneword®, o Líneas Aéreas Asociadas o en Empresas Asociadas, al utilizar cualquiera de los servicios de las Empresas Asociadas o bonos promocionales, tienen vigencia máxima de 3 años calendario, lo que significa que vencen el día 31 de diciembre del tercer año en el cual fueron acumulados. Ahora bien, cada vez que el pasajero vuele en LATAM y acumule 1 o más kilómetros por ese vuelo, todos los kilómetros acumulados vigentes en su cuenta hasta la fecha de dicho vuelo automáticamente se renuevan, pasando a tener una vigencia de 36 meses corridos, contados desde la fecha del mencionado vuelo y vencerán, efectivamente, el último día del trigésimo sexto mes.
                </span>
            </div>

        </div>
        <div class="clear"></div>
    </section>

    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Tarjetas").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/nuestras_tarjetas/visa']);
    </script>
</asp:Content>
