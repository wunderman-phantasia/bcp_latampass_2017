﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="nuestrastarjetas.aspx.cs" Inherits="nuestrastarjetas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.contentwrap header h1, .Content header h1', { fontFamily: 'Flexo-Bold' });
        Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.credito header h1, .debito header h1', { fontFamily: 'Flexo-Regular' });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <section class="Content contentb">
        <header tabindex="7" title="Nuestras Tarjetas" style="height: 34px;">
            <h1>Nuestras Tarjetas</h1>
            <div class="div_regresa"><a class="promobtn" href="Default.aspx" tabindex="8" title="Regresar">Regresar</a></div>
        </header>
        <div class="tarjetas" style="height: 387px;">
            <section class="credito">
                <header style="height: 65px;">
                    <h1 style="text-align: center;">Tarjetas de Crédito</h1>
                </header>
                <figure>
                    <h3>Clásica</h3>
                    <img src="images/cards/cards_clasiccard.png" width="144" height="89" alt="">
                    <figcaption>
                        <ul>
                            <li><a href="tarjetacreditovisa.aspx" tabindex="8" title="Tarjetas de Crédito Visa">Visa</a></li>
                            <li><a href="tarjetaamex.aspx" tabindex="9" title="Tarjetas de Crédito American Express">American Express</a></li>
                        </ul>
                    </figcaption>
                </figure>

                <figure>
                    <h3>Oro</h3>
                    <!-- <img src="images/cards/cards_goldcard.png" width="144" height="90" alt=""> -->
                    <img src="images/amexcard/amexcard_goldcard.png " width="144" height="90" alt="">

                    <figcaption>
                        <ul>
                            <li><a href="tarjetacreditovisa.aspx" tabindex="-1">Visa</a></li>
                            <li><a href="tarjetaamex.aspx" tabindex="-1">American Express</a></li>
                        </ul>
                    </figcaption>
                </figure>
                <figure>
                    <h3>Platinum</h3>
                    <!--<img src="images/cards/cards_platinumcard.png" width="144" height="90" alt="">-->
                    <img src="images/amexcard/amexcard_platinumcard.png" width="144" height="90" alt="">
                    <figcaption>
                        <ul>
                            <li><a href="tarjetacreditovisa.aspx" tabindex="-1">Visa</a></li>
                            <li><a href="tarjetaamex.aspx" tabindex="-1">American Express</a></li>
                            <!--<li><a href="tarjetamastercard.aspx" tabindex="10" title="Tarjetas de Crédito Mastercard">Mastercard</a></li>-->
                        </ul>
                    </figcaption>
                </figure>
                <figure>
                    <h3>Signature</h3>
                    <img src="images/cards/cards_blackcard.png" width="144" height="90" alt="">
                    <figcaption>
                        <ul>
                            <li><a href="tarjetacreditovisa.aspx" tabindex="-1">Visa</a></li>
                            <!--<li><a href="tarjetamastercard.aspx" tabindex="-1">Mastercard</a></li>-->
                        </ul>
                    </figcaption>
                </figure>
            </section>
            <section class="debito" style="margin-right: 30px;">
                <header style="height: 65px;">
                    <h1 style="text-align: center;">Tarjeta de Débito</h1>
                </header>
                <figure>
                    <h3>&nbsp;</h3>
                    <img src="images/cards/cards_debitcard.png" width="144" height="89" alt="">
                    <figcaption>
                        <ul>
                            <li><a href="tarjetacredimas.aspx" tabindex="11" title="Tarjeta de Débito Credimás Visa">Visa</a></li>
                        </ul>
                    </figcaption>
                </figure>
            </section>
        </div>
    </section>

    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Tarjetas").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/nuestras_tarjetas']);
    </script>
</asp:Content>

