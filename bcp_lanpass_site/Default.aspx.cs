﻿using System.Web.Services;

public partial class _Default : System.Web.UI.Page
{
    [WebMethod(EnableSession = true)]
    public static bool HayCuposLibres()
    {
        return new BLPromocion().HayCuposLibres();
    }
}