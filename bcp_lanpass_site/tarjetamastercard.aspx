﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="tarjetamastercard.aspx.cs" Inherits="tarjetamastercard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

	<script type="text/javascript">
	    //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
	    Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.italic30orange', { fontFamily: 'Flexo-Italic' });
	    Cufon.replace('.mediumitalic30orange', { fontFamily: 'Flexo-Bold' });
	    Cufon.replace('.visa figure h1', { fontFamily: 'Flexo-Light' });
	    Cufon.replace('.lista section header h1', { fontFamily: 'Flexo-Bold' });
	</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

	<section class="Content contentb">
		<header style="height:34px;">
			<h1 tabindex="7" title="Tarjetas de Crédito Mastercard: Disponemos de 02 tipos: Platinum, Black.">
				<span class="italic30orange">Nuestras Tarjetas</span><span class="mediumitalic30orange"> - Crédito Mastercard</span>
			</h1>
            <a href="nuestrastarjetas.aspx" class="btnRegresar" title="Regresar"></a>
		</header>
		<div class="tarjetas">
				<section class="visa">
					<figure>
						<h1>Platinum</h1>
						<img src="images/mastercard/mastercard_platinumcard.png" width="150" height="95" alt="">
						<figcaption>
							<p tabindex="8" title="Tarjeta de Crédito Mastercard Platinum: Bono de bienvenida 2,500 kilómetros LATAM Pass.">
								<span class="lightitalic14blue">Bono de bienvenida**</span>
								<br /><span class="mediumitalic14blue">2,500 KMS. LATAM Pass</span>
							</p>
						</figcaption>
					</figure>
					<figure>
						<h1>Black</h1>
						<img src="images/mastercard/mastercard_blackcard.png" width="150" height="95" alt="">
						<figcaption>
							<p tabindex="9" title="Tarjeta de Crédito Mastercard Black: Bono de bienvenida 3,500 kilómetros LATAM Pass.">
								<span class="lightitalic14blue">Bono de bienvenida**</span>
								<br /><span class="mediumitalic14blue">3,500 KMS. LATAM Pass</span>
							</p>
						</figcaption>
					</figure>
				</section>
				<hr>
				<span class="cardsasktext">¿Aún no tienes tu Tarjeta de Crédito BCP LATAM Pass?</span>
				<a class="btnbanner1" href="formulario.aspx" style="margin-top:20px;" tabindex="10" title="Pide tu Tarjeta BCP LATAM Pass online.">¡Pídela aquí!</a>	
		</div>
		<div class="lista">
			<section>
				<header style="height:34px;">						
					<h1><img src="images/visacard/visacard_bennefitsicon.png" width="22" height="21" alt=""/>Beneficios</h1>
				</header>
				<hr>
				<section tabindex="11" title="Tarjetas de Crédito Mastercard Platinum: Beneficios: Seguro de viajes, seguro de autos alquilados, seguro médico integral para viajes, asistencia personal, asistencia de viajes, servicio al cliente MasterCard.">
					<header>
						<h3>Platinum</h3>
					</header>
					<ul class="first">
						<li><span class="lightitalic14light">Seguro de viajes</span></li>
						<li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
						<li><span class="lightitalic14light">Seguro médico integral para viajes</span></li>
					</ul>
					<ul class="last">
						<li><span class="lightitalic14light">Asistencia personal</span></li>
						<li><span class="lightitalic14light">Asistencia de viajes</span></li>
						<li><span class="lightitalic14light">Servicio al cliente MasterCard</span></li>
					</ul>
				</section>
				<section class="lastsec" tabindex="12" title="Tarjetas de Crédito Mastercard Black: Beneficios: Seguro de viaje, seguro de autos alquilados, servicio de inconvenientes de viaje, cobertura por cancelación y/o demora de viaje, beneficios por demora y/o pérdida de equipaje, protección de equipaje, protección contra robo o asalto en cajero, protección de compras, garantía extendida, asistencia personal, asistencia de viajes, servicio al cliente MasterCard, Priority Pass V.I.P. Lounges.">
					<div>
						<header>
							<h3>Black</h3>
						</header>
						<ul class="first">
							<li><span class="lightitalic14light">Seguro de viaje</span></li>
							<li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
							<li><span class="lightitalic14light">Servicio de inconvenientes de viaje</span></li>
							<li><span class="lightitalic14light">Cobertura por cancelación y/o demora de viaje</span></li>
							<li><span class="lightitalic14light">Beneficios por demora y/o pérdida de equipaje</span></li>
							<li><span class="lightitalic14light">Protección de equipaje</span></li>
							<li><span class="lightitalic14light">Protección contra robo o asalto en cajero</span></li>
							<li><span class="lightitalic14light">Protección de compras</span></li>
							<li><span class="lightitalic14light">Garantía extendida</span></li>
						</ul>
						<ul class="last">
							<li><span class="lightitalic14light">Asistencia personal</span></li>
							<li><span class="lightitalic14light">Asistencia de viajes</span></li>
							<li><span class="lightitalic14light">Servicio al cliente MasterCard</span></li>
							<img src="images/visacard/visacard_signedbenefitimage.png" width="185" height="25" alt="" style="margin-top: 25px;">
						</ul>
					</div>
				</section>									
				<div class="clear"></div>
			</section>

			<section>
				<header style="height:34px;">						
					<h1><img src="images/visacard/visacard_lanpassprogramicon.png" width="23" height="20" alt=""/>Programa LATAM Pass</h1>
				</header>
				<hr>
				<section tabindex="13" title="Tarjetas de Crédito Mastercard Platinum. Programa LATAM Pass: acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
					<header>
						<h3>Platinum</h3>
					</header>
						<p><span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span></p>
				</section>
				<section class="lastsec" tabindex="14" title="Tarjetas de Crédito Mastercard Black. Programa LATAM Pass: acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
					<div>
						<header>
							<h3>Black</h3>
						</header>
							<p><span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span></p>
					</div>
				</section>					
				<div class="clear"></div>
			</section>

			<section>
				<header style="height:34px;">						
					<h1><img src="images/visacard/visacard_requirementsicon.png" width="23" height="23" alt=""/>Requisitos</h1>
				</header>
				<hr>
				<section tabindex="15" title="Tarjetas de Crédito Mastercard Platinum. Requisito: Debes tener un ingreso mínimo mensual de S/. 3,000.">
					<header>
						<h3>Platinum</h3>
					</header>
					<p><span class="lightitalic14light">Debes tener un ingreso mínimo<br />mensual de </span><span class="mediumitalic14dark">S/. 3,000</span></p>
				</section>
				<section class="lastsec" tabindex="16" title="Tarjetas de Crédito Mastercard Black. Requisito: Debes tener un ingreso mínimo mensual de S/. 8,000.">
					<div>
						<header>
							<h3>Black</h3>
						</header>
						<p><span class="lightitalic14light">Debes tener un ingreso mínimo<br />mensual de </span><span class="mediumitalic14dark">S/. 8,000</span></p>
					</div>
				</section>					
				<div class="clear"></div>
				<ul tabindex="17" title="Requisitos generales. Debes estar laborando al momento de solicitar la tarjeta. Documentos necesarios: Copia simple de DNI y copia de último recibo de servicios. Si eres dependiente, deberás contar con la copia de tus boletas de pago de los últimos 2 meses (últimas 3 boletas para vendedores y comisionistas). Si eres independiente, deberás contar con la copia del formulario de pago de impuestos por honorarios profesionales, ventas o rentas de los últimos 3 meses o copia de última declaración de impuesto a la renta.">
					<li><span class="mediumitalic14dark">Debes estar laborando al momento de solicitar la tarjeta.</span></li>
					<li><span class="mediumitalic14dark">Documentos necesarios:</span>
						<ul class="bulletreqs">
							<li><span class="lightitalic14light">- Copia simple de DNI.</span></li>
							<li><span class="lightitalic14light">- Copia de último recibo de servicios.</span></li>
							<li><span class="italic14light">- Dependientes: </span><span class="lightitalic14light"> Copia de boletas de pago de los últimos 2 meses (últimas 3 boletas para vendedores y comisionistas).</span></li>
							<li><span class="italic14light">- Independientes: </span><span class="lightitalic14light">Copia de formulario de pago de impuestos por honorarios profesionales, ventas o rentas de los últimos 3 meses o copia de <br />&nbsp;&nbsp;última declaración de impuesto a la renta.</span></li>
						</ul>
					</li>
				</ul>

				<div class="lightitalic11light" style="margin:30px 0px;"  tabindex="18" title="Para solicitar una Tarjeta de Crédito BCP LATAM Pass solo debes llamarnos al 311 9898 o acércate a cualquiera de nuestras Agencias BCP a nivel nacional. También puedes dejarnos tus datos en el formulario virtual ubicado en esta web y en los próximos días nos estaremos comunicando contigo para tramitar tu tarjeta."><span>Para solicitar una Tarjeta de Crédito BCP LATAM Pass solo debes llamarnos al 311 9898 o acércate a cualquiera de nuestras Agencias BCP a nivel nacional.<br /> 
				También puedes dejarnos tus datos en el formulario virtual ubicado en esta web y en los próximos días nos estaremos comunicando contigo para tramitar tu tarjeta.</span></div>

			</section>

			<section>
					<header style="height:34px;">						
						<h1><img src="images/visacard/visacard_featuresicon.png" width="25" height="23" alt=""/>Características</h1>
					</header>
					<hr>
					<ul style="margin: 30px 0;" tabindex="19" title="Características comunes. Las tarjetas de Crédito BCP LATAM Pass Mastercard del BCP tienen validez nacional e internacional. Aceptada en más de 32 millones de establecimientos alrededor del mundo. Puedes comprar, retirar efectivo y comprar por internet. Puedes financiar tus consumos al contado, modalidad revolvente o en cuotas de hasta 36 meses. Puedes elegir tu fecha de facturación entre los 10 o 22 de cada mes. No tendrás costo mensual si no tienes deuda.">
						<li><span class="lightitalic14light">Las Tarjetas de Crédito LATAM Pass MasterCard del BCP tienen</span><span class="mediumitalic14dark"> validez nacional e internacional.</span></li>
						<li><span class="lightitalic14light"> Aceptada en más de </span><span class="mediumitalic14dark">32 millones de establecimientos alrededor del mundo.</span></li>
						<li><span class="lightitalic14light">Puedes comprar, retirar efectivo y</span><span class="mediumitalic14dark"> comprar por internet.</span></li>
						<li><span class="lightitalic14light">Puedes financiar tus consumos al contado, modalidad revolvente o en</span><span class="mediumitalic14dark"> cuotas de hasta 36 meses.</span></li>
						<li><span class="mediumitalic14dark">Puedes elegir </span><span class="lightitalic14light">tu fecha de facturación entre los 10 o 22 de cada mes.</span></li>
						<li><span class="mediumitalic14dark">No tendrás costo </span><span class="lightitalic14light">mensual si no tienes deuda.</span></li>
					</ul>
			</section>
			
			<section>
					<header style="height:34px;">						
						<h1><img src="images/visacard/visacard_commonbennefitsicon.png" width="24" height="16" alt=""/>Beneficios Comunes</h1>
					</header>
					<hr>
					<ul style="margin: 30px 0;" tabindex="20" title="Beneficios comunes. 
Compra de deuda: ¡no tengas deudas separadas! las Tarjetas de Crédito LATAM Pass Mastercard del BCP te dan facilidad para trasladar tus deudas de otras tarjetas de crédito a tasas preferenciales. 
Tarjetas adicionales: obten hasta 9 tarjetas de crédito adicionales para tus familiares.
Campañas especiales: accede a campañas de descuentos con tu tarjeta de crédito y a campañas de acumulacion de KMS. LATAM Pass; 
¡Ahorra tiempo!, realiza tus consultas y pagos por Banca por Teléfono (311-9898) o viabcp.com.">
						<li><span class="mediumitalic14dark">Compra de deuda: </span><span class="lightitalic14light">¡No tengas deudas separadas! Las Tarjetas de crédito LATAM Pass Mastercard del BCP te dan facilidad para trasladar tus deudas de otras tarjetas de crédito a tasas preferenciales.</span></li>						
						<li><span class="mediumitalic14dark">Tarjetas Adicionales: </span><span class="lightitalic14light">Obten hasta 9 tarjetas de crédito adicionales para tus familiares.</span></li>
						<li><span class="mediumitalic14dark">Campañas especiales: </span><span class="lightitalic14light">Accede a campañas de descuentos con tu tarjeta de crédito y a campañas de acumulacion de KMS. LATAM Pass.</span></li>
						<li><span class="mediumitalic14dark">¡Ahorra tiempo! </span><span class="lightitalic14light">Realiza tus consultas y pagos por Banca por Teléfono (311-9898) o viabcp.com.</span></li>
					</ul>
			</section>

			<hr style="border: 1px solid #EFEFEF; margin-bottom: 20px;">
			<div style="padding-right: 8px;margin-bottom: 30px;" tabindex="21" title="Condiciones legales: 
1. Las disposiciones de efectivo, traslados de deuda, efectivo preferente, los consumos en casinos, los pagos de servicios realizados en viabcp.com, los débitos automáticos, los pagos de impuestos y los servicios legales no acumulan KMS. LATAM Pass. 
2. La acumulación, canje, uso y demás condiciones aplicables a los KMS. LATAM Pass se rigen bajo el reglamento de LATAM Pass publicado en www.latam.com/es_pe/latam-pass/. 
3. Para recibir el bono de kilómetros debes usar tu tarjeta de crédito en los primeros 45 días desde su aprobación. Las tarjetas adicionales y upgrade no aplican al bono. 
4. Para más información, condicionesy resticciones ingresar a viabcp.com 
5. Sujeto a los límites diarios por operaciones establecidas para cada canal. 
6. El abono de KMS. LATAM Pass por consumos con Tarjetas de Crédito BCP LATAM Pass, se realizará 48 horas después de la fecha de corte de facturación. 
7. Los kilómetros acumulados por vuelo en las Líneas Aéreas oneworld®, o Líneas Aéreas Asociadas, o en Empresas Asociadas, al utilizar cualquiera de los servicios de las Empresas Asociadas o bonos promocionales, tienen vigencia máxima de 3 años calendario, lo que significa que vencen el día 31 de diciembre del tercer año en el cual fueron acumulados. Ahora bien, cada vez que el pasajero vuele en LATAM y acumule 1 o más kilómetros por ese vuelo, todos los kilómetros acumulados vigentes en su cuenta hasta la fecha de dicho vuelo automáticamente se renuevan, pasando a tener una vigencia de 36 meses corridos, contados desde la fecha del mencionado vuelo y vencerán, efectivamente, el último día del trigésimo sexto mes.
">
				<span class="lightitalic11dark">
Las disposiciones de efectivo, traslados de deuda, efectivo preferente, los consumos en casinos, los pagos de servicios realizados en <a class="lightitalic11dark"style="text-decoration:none;" href="http://www.viabcp.com">viabcp.com</a>, los débitos automáticos, los pagos de impuestos y los servicios legales no acumulan KMS. LATAM Pass.
<br /><br />
** La acumulación, canje, uso y demás condiciones aplicables a los KMS. LATAM Pass se rigen bajo el reglamento de LATAM Pass publicado en <a class="lightitalic11dark"style="text-decoration:none;" href="https://www.latam.com/es_pe/latam-pass/">www.latam.com/es_pe/latam-pass/</a>.
<br /><br />
*** Para recibir el bono de kilómetros debes usar tu tarjeta de crédito en los primeros 45 días desde su aprobación. Las tarjetas adicionales y upgrade no aplican al bono.
<br /><br />
**** Para más información, condicionesy  resticciones ingresar a viabcp.com
<br /><br />
Sujeto a los límites diarios por operaciones establecidas para cada canal.
<br /><br />
El abono de KMS. LATAM Pass por consumos con Tarjetas de Crédito BCP LATAM Pass, se realizará 48 horas después de la fecha de corte de facturación.  
<br />Los kilómetros acumulados por vuelo en las Líneas Aéreas oneworld®, o Líneas Aéreas Asociadas, o en Empresas Asociadas, al utilizar cualquiera de los servicios de las Empresas Asociadas o bonos promocionales, tienen vigencia máxima de 3 años calendario, lo que significa que vencen el día 31 de diciembre del tercer año en el cual fueron acumulados. Ahora bien, cada vez que el pasajero vuele en LATAM y acumule 1 o más kilómetros por ese vuelo, todos los kilómetros acumulados vigentes en su cuenta hasta la fecha de dicho vuelo automáticamente se renuevan, pasando a tener una vigencia de 36 meses corridos, contados desde la fecha del mencionado vuelo y vencerán, efectivamente, el último día del trigésimo sexto mes.
				</span>	
			</div>
			
		</div>
		<div class="clear"></div>
	</section>

    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Tarjetas").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/nuestras_tarjetas/mastercard']);
    </script>
</asp:Content>

