﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="CntDefault" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        Cufon.replace('.aviBox h1', { fontFamily: 'Flexo-Light' }); // Requires a selector engine for IE 6-7, see above
        Cufon.replace('.aviBox h3', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.pull-left', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.pull-right', { fontFamily: 'Flexo-Light' });
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.slider .bannerslide[data-id="1"] h1', { fontFamily: 'Flexo-Bold' })
        //Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
        //Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <form id="frmMain" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <section id="roll" class="Content home">
            <div class="contentSlided">

                <div id="bannerSlider" class="slider">

                    <div class="bannerslide bannerultimallamada cont-banner-lanpass-ae">
                        <div class="cont-int-lanpass-ae">
                          <h2>Este es el momento que estabas esperando</h2>
                          <a href="http://www.latam.com/latampass/catalogolanpassperu/" target="_blank" class="btn-descubre"></a>
                        </div>             
                    </div>

                    <%--
                    <div class="bannerslide bannernavidad2014" data-id="17">
                        <div class="cont-int-tpd">
                            
                            <h2>Adelantar el regalo de<br>
                                quien quieres, te lleva<br>
                                a donde quieres.</h2>
                            
                            <h3 style="padding-bottom: 0">Comprando en:</h3>
                            
                            <ul class="bullet" style="margin-top: 0; padding-left: 20px;">
                                <li><span>Tiendas por departamento.</span></li>
                                <li><span>Jugueterías.</span></li>
                                <li><span>Librerías.</span></li>
                            </ul>

                            <a href="promo-navidad.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>  
                    --%>

                    <div data-id="13" class="bannerslide bannerdescrubrelanpass" style="position: relative;">
                    </div>   
                    
                    <%--
                    <div class="bannerslide bannercanjea cont-banner-lanpass-ae" data-id="18">
                        <div class="cont-int-lanpass-ae">
                          <h2>&iexcl;Volvi&oacute; Kms. que vuelan!</h2>
                          <h3>Viaja por el Perú  desde 7,000 KMS. LANPASS o por Sudamérica desde 18,000 KMS. LANPASS.  </h3>
                          <a href="http://www.latam.com/latampass/es_pe/sitio_personas/lanpass/promociones/kms-que-vuelan-bcp/vuelos-desde-lima/" target="_blank" class="btn-descubre"></a>
                        </div> 
                        <div class="text-legal">*Tarifa desde 7,000 kms. aplica a destinos nacionales, tarifa desde 18,000 kms. solo aplica a La Paz y Santa Cruz. Ver t&eacute;rminos y condiciones.</div>
                    </div>               
                    --%>
                    

                    <%--
                    <div class="bannerslide bannerusala" data-id="16">
                        <div class="cont-int-tpd">
                            <h2>En octubre,<br />
                                usa <span style="font-family: 'Flexo-Bold';">todos los días tu</span><br />
                                <span style="font-family: 'Flexo-Bold';">Tarjeta de Crédito BCP LANPASS</span><br />
                                y gana hasta <span style="font-family: 'Flexo-Bold';">50,000 kms.</span></h2>
                            <a href="usala-todos-los-dias.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>
                    --%>

                    <%--
                    <div class="bannerslide banneramex" data-id="15">
                        <div class="cont-int-lanpass-ae">
                            <h2>Tus compras te acercan<br />
                                a tu próximo viaje</h2>
                            <a href="promo-amex.aspx" class="btn-descubre"></a>
                        </div>
                    </div>
                    <div data-id="14" class="bannerslide bannersupermercadosx5">
                        <div class="cont-int-tpd">
                            <h2>Compra en supermercados con tus<br />
                                <span style="font-family: 'Flexo-Italic'; font-size: 19px;">Tarjetas de Crédito BCP LANPASS</span><br />
                                y acércate a tu próximo viaje</h2>
                            <a href="promo-supermercados.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>
                    <div data-id="14" class="bannerslide bannersupermercadosx5b">
                        <div class="cont-int-tpd">
                            <h2>Compra en supermercados con tus<br />
                                <span style="font-family: 'Flexo-Italic'; font-size: 19px;">Tarjetas de Crédito BCP LANPASS</span><br />
                                y acércate a tu próximo viaje</h2>
                            <a href="promo-supermercados.aspx" class="btn-inscribir"></a>
                        </div>
                    </div>
                    --%>

                    

                </div>
                <div id="pagSlide" class="pagSlide"></div>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#bannerSlider").carouFredSel({
                            auto: { play: true, timeoutDuration: 8000 },
                            items: 1,
                            scroll: { fx: "fade", items: 1, pauseOnHover: true, duration: 800 },
                            pagination: "#pagSlide"
                        });
                    })
                </script>


                <div style="margin-top: 18px;">
                    <div class="avion" tabindex="11" title="Información de kilómetros para los principales destinos: Iquitos 14mil kilómetros, Tarapoto 10mil kilómetros, Trujillo 10mil kilómetros, Cuzco 14mil kilómetros, Arequipa 14mil kilómetros, Santiago 26mil kilómetros, Bogotá 28mil kilómetros, Miami 48mil kilómetros.">
                        <div class="aviBox">
                            <img src="images/flightboard/flightboard_icon.jpg" width="54" height="54" alt="">
                            <h1>¡Viaja a estos destinos!</h1>
                            <h3>Comprando con tus Tarjetas BCP LATAM Pass estarás cada vez más cerca de ellos.</h3>
                            <div class="clear"></div>
                        </div>
                        <div class="aviBox pull-left">
                            Destinos Recomendados
                        </div>
                        <div class="aviBox pull-right">
                            Viaja desde:
                        </div>
                        <div class="clear"></div>

                        <div class="Mainflights">
                            <div class="flightboardline">
                                <div data-id="1" class="basicBoard fliptext"></div>
                                <div data-id="1" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="2" class="basicBoard fliptext"></div>
                                <div data-id="2" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="3" class="basicBoard fliptext"></div>
                                <div data-id="3" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="4" class="basicBoard fliptext"></div>
                                <div data-id="4" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="5" class="basicBoard fliptext"></div>
                                <div data-id="5" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="6" class="basicBoard fliptext"></div>
                                <div data-id="6" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="vIE">
                            <div class="flightboardline">
                                <div data-id="7" class="basicBoard fliptext"></div>
                                <div data-id="7" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="8" class="basicBoard fliptext"></div>
                                <div data-id="8" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="9" class="basicBoard fliptext"></div>
                                <div data-id="9" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="10" class="basicBoard fliptext"></div>
                                <div data-id="10" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="11" class="basicBoard fliptext"></div>
                                <div data-id="11" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                            <div class="flightboardline">
                                <div data-id="12" class="basicBoard fliptext"></div>
                                <div data-id="12" class="basicBoard2 fliptext"></div>
                                <div class="letterlong"><span class="letterpadding">KMS. LANPASS</span></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="Pagflights">
                            <a href="javascript:void(0);" rel="01" class="activo">1</a>
                            <a href="javascript:void(0);" rel="02">2</a>
                        </div>

                        <a href="javascript:void(0);" class="arrow-prev" style="display: none;"></a>
                        <a href="javascript:void(0);" class="arrow-next"></a>

                    </div>

                    <div class="banners">
                        <!--<div class="banner1"><a class="btnbanner1" href="formulario.aspx" tabindex="12" title="Pide tu Tarjeta BCP LANPASS online.">¡Pídela aquí!</a></div>-->
                        
                        <!--<div class="banner2"><a class="btnbanner2" href="faustino.aspx" tabindex="13" title="¡Conoce a Faustino!">¡Conoce a Faustino!</a></div>-->

                        <div class="banner4"><a href="http://www.latam.com/latampass/catalogolanpassperu/" tabindex="13" title="Catálogo LANPASS" target="_blank"></a></div>

                        <div class="banner3">
                            <div class="Doc One">
                                <ul id="selectDocType">
                                    <li class="lightitalic11dark" data-id="1"><span style="float: left;">DNI</span><div class="comboselect"></div>
                                    </li>
                                    <li class="lightitalic11dark unselected" data-id="1">DNI</li>
                                    <li class="lightitalic11dark unselected" data-id="2">Carné de extranjería</li>
                                </ul>
                                <input id="inputDoc" data-id="1" type="text" maxlength="8" placeholder="Ingresa tu DNI" class="inputDoc" tabindex="14" title="Ingresa tu número de documento y presiona ENTER">
                                <p class="lightitalic11orange inputDocerror" tabindex="15" title="Tu número de documento debe tener 8 números."></p>
                                <a class="btnbanner3" href="" tabindex="16" title="Consulta">Consulta</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>

            </div>
        </section>
    </form>
    <div class="modal2"></div>
    <div data-id="1" class="modal">
        <div message-id="1" class="message">
            <div class="popupbox">
                <span class="ModalClose bolditalic30orange">&times;</span>
                <p class="bolditalic23orange popuptitle" id="data-nombre-1">&ltNombre&gt</p>
                <p class="lightitalic23orange popupttext1" id="data-titulo-1">¡Traslada tu Deuda de otros bancos al BCP y gana un Bono de Kilómetros*!</p>
                <p class="lightitalic14dark popupttext2" id="data-descrip-1">*Conoce más de esta oferta acercándote a una Agencia BCP o llamando a nuestra Banca por Teléfono al 311-9898 y disfruta de los beneficios de las Tarjetas de Crédito BCP LANPASS!</p>
                <p class="lightitalic11dark popupttext3" id="data-subdescrip-1">Válido hasta el &lt;fecha de vencimiento&gt; sujeto a evaluación crediticia.</p>
            </div>
        </div>
    </div>

    <div data-id="5" class="modal">
        <div class="message messagesmall">
            <div class="popupbox">
                <span class="ModalClose bolditalic30orange">&times;</span>
                <p class="lightitalic23orange popupttext1" style="padding-top: 55px;">Por el momento, no tenemos una oferta activa para ti.</p>
                <p class="lightitalic14dark popupttext2">¡Te mantendremos informado!</p>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="pull-left" style="width: 100px; margin-top: 25px;">
            Compartir en: 
        </div>
        <div class="pull-left" style="width: 50px; margin-top: 20px;">
            <a class="fbbuttonclick" href="">
                <img src="images/facebook.png" width="22" height="22" alt=""></a>
        </div>
        <div class="pull-left" style="margin-top: 20px;">
            <div class="fb-like" data-href="http://www.bcplatampass.com/" data-width="450" data-layout="button_count" data-action="like" data-show-faces="false" data-send="false"></div>
        </div>
    </footer>
    <script type="text/javascript" src="js/jquery.flightboard.js"></script>
    <script src="js/jquery.history.js"></script>
    <script src="js/JScript_Inicio.js" type="text/javascript"></script>
    <script type="text/javascript">
        _gaq.push(['_trackPageview', '/WebsiteOficial/home']);
        //$(".active").removeClass("active");
        $("#Inicio").css({ "display": "none" });
        $('.avion').css('cursor', 'pointer').on('click', function () {
            window.open("http://www.latam.com/latampass/es_pe/sitio_personas/lanpass/promociones/desde-ahora-conoce-el-peru-por-menos-kms/desde-lima/");
        });
    </script>

    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
      _fbq.push(['addPixelId', '1493357820896130']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1493357820896130&amp;ev=PixelInitialized" /></noscript>

    <!-- Facebook Conversion Code for BCP Lan Pass - AON - Noviembre 2014 -->
    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6017646669790', {'value':'0.00','currency':'USD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6017646669790&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

</asp:Content>

