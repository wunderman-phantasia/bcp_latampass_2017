﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Web.Services;
using DA;

/* @001 LAG 07/02/2014 Se agregó tipo de documento en la inserción de datos del cliente */
public partial class formulario : System.Web.UI.Page
{

    public String UtmSource;
    public String UtmMedium;
    public String UtmCampaign;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["utm_source"] != null || Request.QueryString["utm_medium"] != null || Request.QueryString["utm_campaign"] != null)
        {
            UtmSource = Request.QueryString["utm_source"];
            UtmMedium = Request.QueryString["utm_medium"] ?? "none";
            UtmCampaign = Request.QueryString["utm_campaign"];
        }
        else if (Request.QueryString["gclid"] != null)
        {
            UtmSource = "google";
            UtmMedium = "cpc";
            UtmCampaign = "";
        }
        else
        {
            UtmSource = "none";
            UtmMedium = "none";
            UtmCampaign = "none";
        }

        if (!IsPostBack)
        {
            Convert.ToString(Request.QueryString["x"]);
            this.hdfIdDiseno.Value = HttpContext.Current.Request.QueryString["idDiseno"];
            ListarTarjetaDiseno();

        }
        
        Grabar_Cliente();

    }

    private void Grabar_Cliente()
    {
        if (Request.HttpMethod.ToString() == "POST")
        {
            BLCliente bl = new BLCliente();

            string nro_doc = Request["dni"];
            /* I@001*/
            string tipo_doc = (nro_doc == null ? "CE" : "DNI");
            /* F@001*/

            if (nro_doc == null)
            {
                nro_doc = Request["ce"];
            }

            try
            {
                bool? registrado = bl.Registrar_Cliente(
                    Request["pickcard"],
                    Request["firstname"],
                    Request["lastnamefather"],
                    Request["lastnamemother"],
                    Request["phone"],
                    /* I@001*/
                    tipo_doc,
                    /* F@001*/
                    nro_doc,
                    Request["email"],
                    (Request["hasaccount"] == "yes" ? true : false),
                    int.Parse(Request["income"]),
                    "soles",//Request["incomecurrency"] // se cambio por default solo soles 17/02/2017
                    bl.Obtener_IP(Request),
                    Request["UtmSource"],
                    Request["UtmMedium"],
                    Request["UtmCampaign"],
                    Convert.ToInt32(typecard.SelectedValue)
                    );

                if (Convert.ToBoolean(registrado))
                    Response.Redirect("confirmacion.aspx");
                else
                    Response.Redirect("error-registro.aspx");
            }
            catch { }
        }
        //else Response.Redirect("formulario.aspx");
    }

    private void ListarTarjetaDiseno()
    {
        using (DataClsLanPass db = new DataClsLanPass())
        {
            List<USP_DISENO_CONSULTAR_DISENOTARJETAResult> lista = db.USP_DISENO_CONSULTAR_DISENOTARJETA().ToList();
            typecard.DataSource = lista;
            typecard.DataTextField = "Titulo";
            typecard.DataValueField = "DisenoTarjetaId";
            typecard.DataBind();
            typecard.SelectedValue = "";
            typecard.Items.Insert(0, new ListItem("SELECCIONAR", ""));

        }

    }
}