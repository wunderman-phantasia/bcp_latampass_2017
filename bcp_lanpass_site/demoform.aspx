﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="demoform.aspx.cs" Inherits="demoform" %>

<!DOCTYPE html>
	<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->

<head runat="server">
    <meta charset="utf-8">
    <title></title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.placeholder.js"></script>
    <script type="text/javascript">
        $(function () {
            var input = document.createElement("input");
            if (('placeholder' in input) == false) {
                $('[placeholder]').focus(function () {
                    var i = $(this);
                    if (i.val() == i.attr('placeholder')) {
                        i.val('').removeClass('placeholder');
                        if (i.hasClass('password')) {
                            i.removeClass('password');
                            this.type = 'password';
                        }
                    }
                }).blur(function () {
                    var i = $(this);
                    if (i.val() == '' || i.val() == i.attr('placeholder')) {
                        if (this.type == 'password') {
                            i.addClass('password');
                            this.type = 'text';
                        }
                        i.addClass('placeholder').val(i.attr('placeholder'));
                    }
                }).blur().parents('form').submit(function () {
                    $(this).find('[placeholder]').each(function () {
                        var i = $(this);
                        if (i.val() == i.attr('placeholder'))
                            i.val('');
                    })
                });
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    pruebita pe varom
    
    <input type="text" id="firstname" name="firstname" maxlength="30" placeholder="Nombre" />
    
    </div>
    </form>
</body>
</html>
