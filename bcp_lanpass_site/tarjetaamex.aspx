﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="tarjetaamex.aspx.cs" Inherits="tarjetaamex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        //Cufon.replace('.content ul li a', { fontFamily: 'Flexo-Regular' });
        Cufon.replace('.contentwrap header h1', { fontFamily: 'Flexo-Bold' });
        Cufon.replace('.contentwrap p', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.italic30orange', { fontFamily: 'Flexo-Italic' });
        Cufon.replace('.mediumitalic30orange', { fontFamily: 'Flexo-Bold' });
        Cufon.replace('.visa figure h1', { fontFamily: 'Flexo-Light' });
        Cufon.replace('.lista section header h1', { fontFamily: 'Flexo-Bold' });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <section class="Content contentb">
        <header style="height: 34px;">
            <h1 tabindex="7" title="Tarjetas de Crédito American Express: Disponemos de 03 tipos: Clásica, Oro, Platinum.">
                <span class="italic30orange">Nuestras Tarjetas</span><span class="mediumitalic30orange"> - Crédito American Express</span>
            </h1>
            <div class="div_regresa2"><a class="promobtn" href="masinformacion.aspx" tabindex="8" title="Regresar">Regresar</a></div>
        </header>
        <div class="tarjetas">
            <section class="visa">
                <figure>
                    <h1>Clásica</h1>
                    <img src="images/amexcard/amexcard_clasiccard.png" width="152" height="94" alt="" />
                    <figcaption>
                        <p tabindex="8" title="Tarjeta de Crédito American Express Clásica: Bono de bienvenida 1,500 kilómetros LATAM Pass.">
                            <span class="lightitalic14blue">Bono de bienvenida<sup>1</sup></span>
                            <br />
                            <span class="mediumitalic14blue">2,000 KMS. LATAM Pass</span>
                        </p>
                    </figcaption>
                </figure>

                <figure>
                    <h1>Oro</h1>
                    <img src="images/amexcard/amexcard_goldcard.png" width="152" height="92" alt="">
                    <figcaption>
                        <p tabindex="9" title="Tarjeta de Crédito American Express Oro: Bono de bienvenida 2,500 kilómetros LATAM Pass.">
                            <span class="lightitalic14blue">Bono de bienvenida<sup>1</sup></span>
                            <br />
                            <span class="mediumitalic14blue">3,000 KMS. LATAM Pass</span>
                        </p>
                    </figcaption>
                </figure>

                <figure>
                    <h1>Platinum</h1>
                    <img src="images/amexcard/amexcard_platinumcard.png" width="148" height="92" alt="">
                    <figcaption>
                        <p tabindex="10" title="Tarjeta de Crédito American Express Platinum: Bono de bienvenida 3,500 kilómetros LATAM Pass.">
                            <span class="lightitalic14blue">Bono de bienvenida<sup>1</sup></span>
                            <br />
                            <span class="mediumitalic14blue">4,000 KMS. LATAM Pass</span>
                        </p>
                    </figcaption>
                </figure>
            </section>
             <hr>
            <span class="cardsasktext">¿Aún no tienes tu Tarjeta de Crédito BCP LATAM Pass?</span>
            <a class="btnbanner1" href="formulario.aspx?pickcard=amex" style="margin-top: 20px;" tabindex="12" title="Pide tu Tarjeta BCP LATAM Pass online.">¡Pídela aquí!</a>
        </div>
        <div class="lista">
            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_bennefitsicon.png" width="22" height="21" alt="" />Beneficios</h1>
                </header>
                <hr>
                <section tabindex="12" title="Tarjetas de Crédito American Express Clásica: Beneficios: Seguro de accidentes de viaje, seguro de autos alquilados, servicio de inconvenientes de viaje, servicios de viaje, asistencia de emergencia, servicio al cliente American Express.">
                    <header>
                        <h3>Clásica</h3>
                    </header>
                    <ul class="first">
                        <li><span class="lightitalic14light">Seguro de accidentes de viaje</span></li>
                        <li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
                        <li><span class="lightitalic14light">Servicio de inconvenientes de viaje</span></li>
                    </ul>
                    <ul class="last">
                        <li><span class="lightitalic14light">Servicios de viaje</span></li>
                        <li><span class="lightitalic14light">Asistencia de emergencia</span></li>
                        <li><span class="lightitalic14light">Servicio al cliente American Express</span></li>
                    </ul>
                </section>
                <section tabindex="13" title="Tarjetas de Crédito American Express Oro: Beneficios: Seguro de accidentes de viaje, seguro de autos alquilados, servicio de inconvenientes de viaje, servicios de viaje, asistencia de emergencia, servicio al cliente American Express.">
                    <div>
                        <header>
                            <h3>Oro</h3>
                        </header>
                        <ul class="first">
                            <li><span class="lightitalic14light">Seguro de accidentes de viaje</span></li>
                            <li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
                            <li><span class="lightitalic14light">Servicio de inconvenientes de viaje</span></li>
                        </ul>
                        <ul class="last">
                            <li><span class="lightitalic14light">Servicios de viaje</span></li>
                            <li><span class="lightitalic14light">Asistencia de emergencia</span></li>
                            <li><span class="lightitalic14light">Servicio al cliente American Express</span></li>
                        </ul>
                    </div>
                </section>
                <section class="lastsec" tabindex="14" title="Tarjetas de Crédito American Express Platinum: Beneficios: Seguro de accidentes de viaje, seguro de autos alquilados, servicio de inconvenientes de viaje, seguro de protección de compras, garantía extendida, servicios de viaje, asistencia de emergencia, servicio al cliente American Express, Priority Pass V.I.P. Lounges.">
                    <div>
                        <header>
                            <h3>Platinum</h3>
                        </header>
                        <ul class="first">
                            <li><span class="lightitalic14light">Seguro de accidentes de viaje</span></li>
                            <li><span class="lightitalic14light">Seguro de autos alquilados</span></li>
                            <li><span class="lightitalic14light">Servicio de inconvenientes de viaje</span></li>
                            <li><span class="lightitalic14light">Seguro de protección de compras</span></li>
                            <li><span class="lightitalic14light">Garantía extendida</span></li>
                        </ul>
                        <ul class="last">
                            <li><span class="lightitalic14light">Servicios de viaje</span></li>
                            <li><span class="lightitalic14light">Asistencia de emergencia</span></li>
                            <li><span class="lightitalic14light">Servicio al cliente American Express</span></li>
                            <!--<img src="images/visacard/visacard_signedbenefitimage.png" width="185" height="25" alt="" style="margin-top: 25px;">-->
                        </ul>
                    </div>
                </section>
                <div class="clear"></div>
            </section>

            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_lanpassprogramicon.png" width="23" height="20" alt="" />Programa LATAM Pass</h1>
                </header>
                <hr>
                <section tabindex="15" title="Tarjetas de Crédito American Express Clásica. Programa LATAM Pass: Acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
                    <header>
                        <h3>Clásica</h3>
                    </header>
                    <p><span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo<sup>2</sup> o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span></p>
                </section>
                <section tabindex="16" title="Tarjetas de Crédito American Express Oro. Programa LATAM Pass: Acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
                    <div>
                        <header>
                            <h3>Oro</h3>
                        </header>
                        <p><span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo<sup>2</sup> o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span></p>
                    </div>
                </section>
                <section class="lastsec" tabindex="17" title="Tarjetas de Crédito American Express Platinum. Programa LATAM Pass: Acumulas 1.5 KMS. LATAM Pass por cada dólar de consumo* o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.">
                    <div>
                        <header>
                            <h3>Platinum</h3>
                        </header>
                        <p><span class="lightitalic14light">Acumulas</span><span class="mediumitalic14dark"> 1.5 KMS. LATAM Pass</span><span class="lightitalic14light"> por cada dólar de consumo<sup>2</sup> o su equivalente en soles en cualquier establecimiento, en el Perú y el mundo.</span></p>
                    </div>
                </section>
                <div class="clear"></div>
            </section>

            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_requirementsicon.png" width="23" height="23" alt="" />Requisitos</h1>
                </header>
                <hr>
                <section tabindex="18" title="Tarjetas de Crédito American Express Clásica. Requisito: Debes tener un ingreso mínimo mensual de S/. 1,800.">
                    <header>
                        <h3>Clásica</h3>
                    </header>
                    <p><span class="lightitalic14light">Debes tener un ingreso mínimo<br />
                        mensual de </span><span class="mediumitalic14dark">S/. 1,800</span></p>
                </section>
                <section tabindex="19" title="Tarjetas de Crédito American Express Oro. Requisito: Debes tener un ingreso mínimo mensual de S/. 2,500.">
                    <div>
                        <header>
                            <h3>Oro</h3>
                        </header>
                        <p><span class="lightitalic14light">Debes tener un ingreso mínimo<br />
                            mensual de </span><span class="mediumitalic14dark">S/. 2,500</span></p>
                    </div>
                </section>
                <section class="lastsec" tabindex="20" title="Tarjetas de Crédito American Express Platinum. Requisito: Debes tener un ingreso mínimo mensual de S/. 6,000.">
                    <div>
                        <header>
                            <h3>Platinum</h3>
                        </header>
                        <p><span class="lightitalic14light">Debes tener un ingreso mínimo<br />
                            mensual de </span><span class="mediumitalic14dark">S/. 6,000</span></p>
                    </div>
                </section>
                <div class="clear"></div>
                <ul tabindex="21" title="Requisitos generales. Debes estar laborando al momento de solicitar la tarjeta. Documentos necesarios: Copia simple de DNI y copia de último recibo de servicios. Si eres dependiente, deberás contar con la copia de tus boletas de pago de los últimos 2 meses (últimas 3 boletas para vendedores y comisionistas).">
                    <li><span class="mediumitalic14dark">Debes estar laborando al momento de solicitar la tarjeta.</span></li>
                    <li><span class="mediumitalic14dark">Documentos necesarios:</span>
                        <ul class="bulletreqs">
                            <li><span class="lightitalic14light">- Copia simple de DNI.</span></li>
                            <li><span class="lightitalic14light">- Copia de último recibo de servicios.</span></li>
                            <li><span class="italic14light">- Dependientes: </span><span class="lightitalic14light">Copia de boletas de pago de los últimos 2 meses (últimas 3 boletas para vendedores y comisionistas).</span></li>
                        </ul>
                    </li>
                </ul>

                <div class="lightitalic11light" style="margin: 30px 0px;" tabindex="22" title="Para solicitar una Tarjeta de Crédito BCP LATAM Pass solo debes llamarnos al 311 9898 o acércate a cualquiera de nuestras Agencias BCP a nivel nacional. También puedes dejarnos tus datos en el formulario virtual ubicado en esta web y en los próximos días nos estaremos comunicando contigo para tramitar tu tarjeta.">
                    <span>Para solicitar una Tarjeta de Crédito BCP LATAM Pass solo debes llamarnos al 311 9898 o acércate a cualquiera de nuestras Agencias BCP a nivel nacional.<br />
                        También puedes dejarnos tus datos en el formulario virtual ubicado en esta web y en los próximos días nos estaremos comunicando contigo para tramitar tu tarjeta.</span>
                </div>
            </section>

            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_featuresicon.png" width="25" height="23" alt="" />Características</h1>
                </header>
                <hr>
                <ul style="margin: 30px 0;" tabindex="23" title="Características comunes. Las tarjetas de Crédito BCP LATAM Pass American Express del BCP tienen validez nacional e internacional. Aceptada en miles de establecimientos en Perú en el mundo. Puedes comprar, retirar efectivo y comprar por internet. Puedes financiar tus consumos al contado, modalidad revolvente o en cuotas de hasta 36 meses. No tendrás costo mensual si no tienes deuda.">
                    <li><span class="lightitalic14light">Las Tarjetas de Crédito LATAM Pass American Express del BCP tienen</span><span class="mediumitalic14dark"> validez nacional e internacional.</span></li>
                    <li><span class="lightitalic14light">Aceptada en </span><span class="mediumitalic14dark">miles de establecimientos en Perú </span><span class="lightitalic14light">y el mundo.</span></li>
                    <li><span class="lightitalic14light">Puedes comprar, retirar efectivo y</span><span class="mediumitalic14dark"> comprar por internet.</span></li>
                    <li><span class="lightitalic14light">Puedes financiar tus consumos al contado, modalidad revolvente o en</span><span class="mediumitalic14dark"> cuotas de hasta 36 meses.</span></li>
                    <li><span class="mediumitalic14dark">No tendrás costo </span><span class="lightitalic14light">mensual si no tienes deuda.</span></li>
                </ul>
            </section>

            <section>
                <header style="height: 34px;">
                    <h1>
                        <img src="images/visacard/visacard_commonbennefitsicon.png" width="24" height="16" alt="" />Beneficios Comunes</h1>
                </header>
                <hr>
                <ul style="margin: 30px 0;" tabindex="24" title="Beneficios comunes: 
Disposición de efectivo: Puedes solicitar hasta 95% de tu línea de crédito en efectivo.
Compra de deuda: ¡no tengas deudas separadas! las Tarjetas de Crédito LATAM Pass American Express del BCP te dan facilidad para trasladar tus deudas de otras tarjetas de crédito a tasas preferenciales.
Tarjetas adicionales: obten hasta 9 tarjetas de crédito adicionales para tus familiares.
Campañas especiales: accede a campañas de descuentos con tu tarjeta de crédito y a campañas de acumulacion de KMS. LATAM Pass.
¡Ahorra tiempo!, realiza tus consultas y pagos por Banca por Teléfono (311-9898) o viabcp.com.">
                    <li><span class="mediumitalic14dark">Disposición de efectivo: </span><span class="lightitalic14light">Puedes solicitar hasta 95% de tu línea de crédito en efectivo.</span></li>
                    <li><span class="mediumitalic14dark">Compra de deuda: </span><span class="lightitalic14light">¡No tengas deudas separadas! Las Tarjetas de crédito LATAM Pass American Express del BCP te dan facilidad para trasladar tus deudas de otras tarjetas de crédito a tasas preferenciales.</span></li>
                    <li><span class="mediumitalic14dark">Tarjetas Adicionales: </span><span class="lightitalic14light">Obten hasta 9 tarjetas de crédito adicionales para tus familiares.</span></li>
                    <li><span class="mediumitalic14dark">Campañas especiales: </span><span class="lightitalic14light">Accede a campañas de descuentos con tu tarjeta de crédito y a campañas de acumulacion de KMS. LATAM Pass.</span></li>
                    <li><span class="mediumitalic14dark">¡Ahorra tiempo! </span><span class="lightitalic14light">Realiza tus consultas y pagos por Banca por Teléfono (311-9898) o viabcp.com.</span></li>
                </ul>
            </section>

            <hr style="border: 1px solid #EFEFEF; margin-bottom: 20px;">
            <div style="padding-right: 8px; margin-bottom: 30px;" tabindex="25" title="Condiciones legales: 
1. Las disposiciones de efectivo, traslados de deuda, efectivo preferente, los consumos en casinos, los pagos de servicios realizados en viabcp.com, los débitos automáticos, los pagos de impuestos y los servicios legales  no acumulan KMS. LATAM Pass.
">
                <span class="lightitalic11dark" style="font-family:cursive;">
                    (1) Para recibir el bono de kilómetros debes usar tu tarjeta de crédito en los primeros 45 días desde su aprobación. Las tarjetas adicionales y upgrade no aplican al bono.
                    <br />
                    <br />
                    (2) Las disposiciones de efectivo, traslados de deuda, efectivo preferente, los consumos en casinos, los pagos de servicios realizados en viabcp.com, los débitos automáticos, los pagos de impuestos y los servicios legales  no acumulan KMS. LATAM Pass.
                    <br />
                    <br />
                    La acumulación, canje, uso y demás condiciones aplicables a los KMS. LATAM Pass se rigen bajo el reglamento de LATAM Pass publicado en <a style="text-decoration:none;" class="lightitalic11dark" href="https://www.latam.com/es_pe/latam-pass/">www.latam.com/es_pe/latam-pass/</a>. Para más información, condiciones y restricciones ingresar a viabcp.com. Sujeto a los límites diarios por operaciones establecidas para cada canal. El abono de KMS. LATAM Pass por consumos con Tarjetas de Crédito BCP LATAM Pass, se realizará 48 horas después de la fecha de corte de facturación. Los kilómetros acumulados por vuelo en las Líneas Aéreas oneword®, o Líneas Aéreas Asociadas o en Empresas Asociadas, al utilizar cualquiera de los servicios de las Empresas Asociadas o bonos promocionales, tienen vigencia máxima de 3 años calendario, lo que significa que vencen el día 31 de diciembre del tercer año en el cual fueron acumulados. Ahora bien, cada vez que el pasajero vuele en LATAM y acumule 1 o más kilómetros por ese vuelo, todos los kilómetros acumulados vigentes en su cuenta hasta la fecha de dicho vuelo automáticamente se renuevan, pasando a tener una vigencia de 36 meses corridos, contados desde la fecha del mencionado vuelo y vencerán, efectivamente, el último día del trigésimo sexto mes.
                </span>
            </div>

        </div>
        <div class="clear"></div>
    </section>


    <script type="text/javascript">
        $(".active").removeClass("active");
        $("#Tarjetas").addClass("active");
        _gaq.push(['_trackPageview', '/WebsiteOficial/nuestras_tarjetas/amex']);
    </script>

</asp:Content>
